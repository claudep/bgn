# Copyright (c) 2009 Claude Paroz <claude@2xlibre.net>
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render, get_object_or_404
from django.core.serializers import serialize
from django.core.paginator import Paginator, InvalidPage, EmptyPage
from django.urls import reverse
from django.utils.translation import gettext as _

from taxo.models import Rank, Taxon
from invent.models import BaseObject


def taxo_browse_by_mode(request, taxon_id, mode):
    if mode in ('select', 'selectb'):
        return taxo_tree(request, taxon_id, mode)
    elif mode == 'obj':
        return taxo_browse(request, taxon_id)


def taxo_tree(request, taxon_id, mode):
    """ bare = 1 if the template should not include standard html content """
    taxon = get_object_or_404(Taxon, pk=int(taxon_id))
    context = {
        'displayed_taxons': batch_col(taxon.get_children()),
        'main_taxon': taxon,
        'mode'      : mode,
    }
    template = {
        'select': 'taxo/taxo_tree.html', 'selectb': 'taxo/taxo_tree_bare.html'
    }.get(mode)
    return render(request, template, context)


def taxo_tree_by_rank(request, rank, mode):
    """ rank is a rank included in models.RANK_CHOICES """
    taxon_list = Taxon.objects.filter(rank=rank)
    context = {
        'displayed_taxons': batch_col(taxon_list),
        'mode'      : mode,
    }
    template = {
        'select': 'taxo/taxo_tree.html', 'selectb': 'taxo/taxo_tree_bare.html',
        'obj'   : 'taxo/taxo_browse.html',
    }.get(mode)
    return render(request, template, context)


def taxo_browse(request, taxon_id):
    taxon = get_object_or_404(Taxon, pk=int(taxon_id))
    paginator_obj = Paginator(BaseObject.objects.filter(taxon__hierarchy__startswith=taxon.get_hierarchy_string()).order_by('title'), 10)
    context = {
        'main_taxon'  : taxon,
        'num_children': paginator_obj.count,
        'displayed_taxons': batch_col(taxon.get_children(), num=5),
        'mode'        : 'obj',
    }
    try:
        context['page_obj'] = paginator_obj.page(1)
    except (EmptyPage, InvalidPage):
        context['page_obj'] = paginator_obj.page(paginator_obj.num_pages)
    return render(request, "taxo/taxo_browse.html", context)


# The two following functions are used to get only a paginated list of objects from taxo_browse main page
def taxo_browse_subobjects(request, taxon_id):
    taxon = get_object_or_404(Taxon, pk=int(taxon_id))
    no_page = int(request.GET.get('p', '1'))
    paginator_obj = Paginator(BaseObject.objects.filter(taxon__hierarchy__startswith=taxon.get_hierarchy_string()).order_by('title'), 10)
    context = {
        'main_taxon'  : taxon,
        'num_children': paginator_obj.count,
        'more_url'    : reverse('taxo_browse_subobjects', args=[taxon.id]),
        'style'       : 'short',
    }
    try:
        context['page'] = paginator_obj.page(no_page)
    except (EmptyPage, InvalidPage):
        context['page'] = paginator_obj.page(paginator_obj.num_pages)
    return render(request, "invent/object_list_block.html", context)


def taxo_search(request, mode):
    """ Search taxonomy with 'text' search term """
    if not 'text' in request.GET:
        return HttpResponseForbidden(_("Search requests must contain a text parameter"))
    taxons = Taxon.objects.filter(name__icontains=request.GET['text'])
    context = {
        'displayed_taxons': batch_col(taxons),
        'mode'            : mode,
    }
    return render(request, "taxo/taxo_widget.html", context)


# Utility functions
def batch_col(lst, num=15):
    """ This function cut lst in chunks of num objects, and create a list of dicts suitable for display:
        [{'index':, 'col':<objects>, 'class':<visible|hidden>},...]
    """
    batch = []
    if not lst:
        return batch
    start = 0
    while start < len(lst):
        batch.append(lst[start:start+num])
        start += num
    # batch = [[0,1.. 15], [16, 17.. 30], [..]..]
    col_list = [{'index':i, 'col':col, 'class':i<3 and 'visible' or 'hidden'} for i, col in enumerate(batch)]
    for i in range(len(col_list), 3):
        col_list.append({'index':i, 'class':'visible'})
    return col_list
