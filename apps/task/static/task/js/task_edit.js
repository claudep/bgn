function delete_file_task(file_id) {
    if (confirm(gettext("Do you really want to delete this file?"))) {
        $.ajax({
            type: "POST",
            url: "/task/"+task_id+"/file/del/",
            data: "id_to_delete="+file_id,
            success: function(data){
                $("div#file-"+file_id).hide();
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}

function delete_task_child(task_id_child) {
    if (confirm(gettext("Delete this task is definitive. Do you really want to delete this task and all its content?"))) {
        $.ajax({
            type: "POST",
            url: "/task/"+task_id+"/task/del/",
            data: "id_to_delete="+task_id_child,
            success: function(data){
                $("div#task-"+task_id_child).hide();
                window.location.reload(true);
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}

function up_level_task(task_id_child){
    if(confirm(gettext("Do you really want to up level this task?"))) {
        $.ajax({
            type:"POST",
            url:"/task/"+task_id+"/task/up/",
            data:"id_to_up_level="+task_id_child,
            success: function(data){
                $("div#task-"+task_id_child).hide();
                window.location.reload(true);
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}
