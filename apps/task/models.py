from datetime import date

from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericRelation
from django.contrib.staticfiles import finders
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.templatetags.static import static
from django.urls import reverse
from django.utils.translation import gettext as _

from invent.models import BaseObject, Attribute, ObjectValue, date_validator, value_render


class TaskAuto(models.Model):
    text = models.TextField()
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    value_text = models.TextField(blank=True)
    
    class Meta:
        db_table = "task_auto"

    def __str__(self):
        return self.text


class Task(models.Model):
    text = models.TextField("Tâche", null = False, blank = False)
    done = models.BooleanField("Terminé", null = False)
    obj = models.ForeignKey(BaseObject, null=True, on_delete=models.CASCADE)
    auto = models.ForeignKey(TaskAuto, null=True, on_delete=models.CASCADE)
    deadline = models.DateField("Echéance", null=True, blank=True)
    owner = models.ForeignKey(User, on_delete=models.CASCADE)
    files = GenericRelation('invent.FileAttachment')
    
    class Meta:
        db_table = "task"

    def status(self):
        if self.done:
            return "done" # green
        elif self.deadline == None:
            return "unknown" # gray
        elif self.deadline <= date.today():
            return "late" # red
        else:
            return "early" # yellow

    def __str__(self):
        return self.text or str(self.pk)
    
    def get_text(self):
        return str(self)
        
    def get_absolute_url(self):
        return reverse('task_edit', args=[str(self.id)])
            
    def breadcrumb(self, link_self=False):
        """ Produce a tuple list structure to give to template to build the breadcrumb """
        bc_list = [(_("Home"), "/")]
        bc_list.append((self.get_text(), link_self and self.get_absolute_url() or ""))
        return bc_list


@receiver(post_save)
def ObjectValue_post_save(sender, instance, created, **kwargs):
    if not isinstance(instance, ObjectValue):
        return
    for auto in TaskAuto.objects.filter(attribute = instance.attr):
        try:
            task = Task.objects.get(obj = instance.obj, auto = auto)
        except Task.DoesNotExist:
            task = Task(obj = instance.obj, auto = auto)
            task.text = auto.text
            task.done = False
            task.owner = instance.obj.owner
        if instance.attr.datatype == 'date':
            try:
                task.deadline = date_validator(instance.value_text)
            except ValidationError:
                pass
        task.save()

@receiver(pre_delete, sender = ObjectValue)
def ObjectValue_pre_delete(sender, instance, **kwargs):
    Task.objects.filter(obj = instance.obj, auto__attribute = instance.attr).delete()

@receiver(value_render)
def ObjectValue_render(sender, result, **kwargs):
    for task in Task.objects.filter(obj = sender.obj, auto__attribute = sender.attr, done = False):
        result.append('<div><img src="%s"> %s</div>' % (static('img/%s.png' % task.status()), task.text))
