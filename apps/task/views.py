from django import forms
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.dispatch import receiver, Signal
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import render, get_object_or_404
from django.utils.translation import gettext as _
from django.views.decorators.http import require_POST

from invent.views import value_edit_post_init, value_edit_post_save
from invent.models import BaseObject, FileAttachment
from invent.forms import FileForm

from .models import TaskAuto, Task
from .forms import TaskForm

from datetime import date

HOME_TUPLE = (_("Home"), "/")

@login_required
def task_create(request, obj_id = None):
    task = Task()
    task.owner = request.user
    
    if obj_id:
        task.obj = BaseObject.objects.get(id = obj_id)
        
    return task_edit_form(request, task)

@login_required
def task_create_task(request, task_id):
    task = Task()
    task.owner = request.user
    task.parent = Task.objects.get(id = task_id)
    
    return task_edit_form(request, task)
    
@login_required
def task_edit(request, task_id):
    task = get_object_or_404(Task, id = task_id)
    
    return task_edit_form(request, task)

def task_edit_form(request, task):
    editable = True
    if request.method == 'POST':
        form = TaskForm(request.POST, instance = task)
        
        if form.is_valid():
            obj = task.obj
            
            if not form.instance.owner_id:
                form.instance.owner = request.user
            form.save()
            return HttpResponseRedirect(obj and obj.get_absolute_url() or '/')
    else:
        form = TaskForm(instance = task)

    context = {
        'task': task,
        'form': form,
        'editable': editable,
        'breadcrumb':  task and task.breadcrumb() or [HOME_TUPLE, (_("Create task"), "")],
    }
    return render(request, 'task/task_edit.html', context)


@login_required
def task_delete(request, task_id):
    if request.method == 'POST':
        task = get_object_or_404(Task, pk=task_id)
        obj = task.obj
        task.delete()
        messages.success(request, _("The task was successfully deleted."))
        return HttpResponseRedirect(obj.get_absolute_url())


@login_required
def tasks_view(request):
    tasks = Task.objects.all()
        
    def sort_key(task):
        return (task.deadline or date.today(), task.text)
        
    tasks = sorted(tasks, key = sort_key)
    
    context = {
        'tasks': tasks,
         'breadcrumb': [HOME_TUPLE, (_("Task Edition"), "")],
    }
        
    return render(request, 'task/tasks_view.html', context)

@receiver(value_edit_post_init)
def post_init(sender, form, obj, attr, value, **kwargs):
    for auto in TaskAuto.objects.filter(attribute = attr):
        try:
            done = Task.objects.get(obj = obj, auto = auto).done
        except Task.DoesNotExist:
            done = False
        form.fields['auto_%i' % auto.id] = forms.BooleanField(label = auto.text, initial = not done, required = False)

@receiver(value_edit_post_save)
def post_save(sender, form, obj, attr, value, **kwargs):
    for auto in TaskAuto.objects.filter(attribute = attr):
        task = Task.objects.get(obj = obj, auto = auto)
        task.done = not form.cleaned_data['auto_%i' % auto.id]
        task.save()
        
def edit_file_task(request, task_id, file_id):
    task = get_object_or_404(Task, pk=task_id)
    file_id = int(file_id)
    file_obj = None
    
    if file_id > 0:
        file_obj = FileAttachment.objects.get(pk=file_id)
        
    if request.method == 'POST':
        file_form = FileForm(request.POST, request.FILES, instance=file_obj)
        
        if file_form.is_valid():                
            file_form.save(task)
            return HttpResponseRedirect(task.get_absolute_url())
    else:
        file_form = FileForm(instance=file_obj)
    context = {
        'task': task,
        'photo': file_obj,
        'form': file_form,
        'breadcrumb': task and task.breadcrumb(),
    }
    return render(request, 'task/task_file_edit.html', context)

def del_file_task(request, task_id):
    return del_element(request, task_id, FileAttachment)

def del_task_child(request, task_id):
    return del_element(request, task_id, Task)
    
@require_POST
def del_element(request, task_id, DelModel):
    """ Delete an attribute value from a task """
    task = get_object_or_404(Task, pk=task_id)
        
    try:
        pk = int(request.POST['id_to_delete'])
        value = get_object_or_404(DelModel, pk=pk)
    except ValueError:
        attr = get_object_or_404(Attribute, name=request.POST['id_to_delete'])
    value.delete()
    return HttpResponse("")

@require_POST
def up_level_task_child(request, parent_id):
    """ Up level for task child """
    task_parent = get_object_or_404(Task, pk=parent_id)
    
    try:
        pk_child = int(request.POST['id_to_up_level'])
        task_child = get_object_or_404(Task, pk=pk_child)
        
        if task_parent.parent:
            task_child.parent = task_parent.parent
        else:
            task_child.parent = None
        task_child.save()
        
    except ValueError:
        value_error = request.POST['id_to_up_level']
        
    return HttpResponse("")
