from django.urls import path

from task import views

urlpatterns = [
    path('', views.tasks_view, name='tasks_view'),
    path('create/', views.task_create, name='task_create'),
    path('create/<int:obj_id>/', views.task_create, name='task_create'),
    path('<int:task_id>/edit/', views.task_edit, name='task_edit'),
    path('<int:task_id>/delete/', views.task_delete, name='task_delete'),
    path('<int:task_id>/file/<int:file_id>/edit_file/', views.edit_file_task, name='edit_file_task'),
    path('<int:task_id>/file/del/', views.del_file_task, name='del_file_task'), # file id in POST
    #url(r'^list_task$', views.tasks_view, name='tasks_view'),
    path('create_task/<int:task_id>/', views.task_create_task, name='task_create_task'),
    path('<int:task_id>/task/del/', views.del_task_child, name='del_task_child'),
    path('<int:parent_id>/task/up/', views.up_level_task_child, name='up_level_task_child'),
]
