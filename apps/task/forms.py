# -*- coding: utf-8 -*-
from django import forms
from django.utils.translation import gettext as _
from .models import Task

class TaskForm(forms.ModelForm):
    deadline = forms.DateField(label = "Echéance", input_formats = ['%d.%m.%Y', '%Y-%m-%d', '%d/%m/%Y'], required = False)
    class Meta:
        model = Task
        fields = ('deadline', 'text', 'done')
