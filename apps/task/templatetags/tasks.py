from datetime import date
from django import template
from django.conf import settings
from django.db.models import Q
from task.models import Task

register = template.Library()

@register.inclusion_tag('task/tasks_block.html', takes_context = True)
def tasks_block(context):
    user = context['request'].user
    def sort_key(task):
        return (task.deadline or date.today(), task.text)
    tasks = Task.objects.filter(done = False).select_related('obj')
    if (not user.is_superuser):
        private = Q(obj__isnull = True, owner = user)
        if (user.is_staff):
            obj = Q(obj__isnull = False)
        else:
            obj = Q(obj__isnull = False, obj__owner = user)
        tasks = tasks.filter(private | obj)
    tasks = sorted(tasks, key = sort_key)
    return {
        'tasks': tasks[:1000],
    }

