from django.contrib import admin

from .models import Task, TaskAuto


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ['text', 'obj', 'deadline']
    raw_id_fields = ('obj',)
    list_filter = ('done',)


@admin.register(TaskAuto)
class TaskAutoAdmin(admin.ModelAdmin):
    list_display = ['text', 'attribute', 'value_text']
