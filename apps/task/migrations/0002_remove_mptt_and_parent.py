from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('task', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='task',
            name='level',
        ),
        migrations.RemoveField(
            model_name='task',
            name='lft',
        ),
        migrations.RemoveField(
            model_name='task',
            name='parent',
        ),
        migrations.RemoveField(
            model_name='task',
            name='rght',
        ),
        migrations.RemoveField(
            model_name='task',
            name='tree_id',
        ),
    ]
