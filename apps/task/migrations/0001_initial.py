from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('invent', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField(verbose_name='Tâche')),
                ('done', models.BooleanField(verbose_name='Terminé')),
                ('deadline', models.DateField(blank=True, null=True, verbose_name='Echéance')),
                ('lft', models.PositiveIntegerField(db_index=True, editable=False)),
                ('rght', models.PositiveIntegerField(db_index=True, editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(db_index=True, editable=False)),
            ],
            options={
                'db_table': 'task',
            },
        ),
        migrations.CreateModel(
            name='TaskAuto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('text', models.TextField()),
                ('value_text', models.TextField(blank=True, null=True)),
                ('attribute', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.Attribute')),
            ],
            options={
                'db_table': 'task_auto',
            },
        ),
        migrations.AddField(
            model_name='task',
            name='auto',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='task.TaskAuto'),
        ),
        migrations.AddField(
            model_name='task',
            name='obj',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='invent.BaseObject'),
        ),
        migrations.AddField(
            model_name='task',
            name='owner',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='task',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='task.Task'),
        ),
    ]
