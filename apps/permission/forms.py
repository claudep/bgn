from django import forms
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.utils.translation import gettext_lazy as _

from invent.models import STATUS_CHOICES
from permission.models import Permission, PERM_LIST

class GroupModelChoiceField(forms.ModelChoiceField):
    def exclude_choice(self, grp):
        self.cache_choices=True
        for ch in self.choices:
            if ch[0] == grp.pk:
                self.choice_cache.remove(ch)
                break

class PermissionEditForm(forms.Form):
    owner  = forms.ModelChoiceField(queryset=User.objects.all())
    status = forms.ChoiceField(choices=STATUS_CHOICES)
    new_perm_group = GroupModelChoiceField(
        queryset=Group.objects.all().order_by('name'),
        widget=forms.Select(attrs={'style': 'max-width:20em;'}),
        required=False)
    new_perm_perm  = forms.ChoiceField(choices=(PERM_LIST), widget=forms.RadioSelect, initial='read')

    apply_to_children = forms.BooleanField(required=False,
        label=_("Apply these permissions to all contained objects"))

    def __init__(self, obj, *args, **kwargs):
        self.instance = obj
        super(PermissionEditForm, self).__init__(*args, **kwargs)
        self.fields['owner'].initial = obj.owner
        self.fields['status'].initial = obj.status
        if not obj .is_containable():
            del self.fields['apply_to_children']
        for perm in obj.permissions.select_related('group'):
            self.fields['perm_del_%d' % perm.id] = forms.BooleanField(initial=False, required=False)

    def save(self, *args, **kwargs):
        if 'owner' in self.changed_data:
            self.instance.owner = self.cleaned_data['owner']
        self.instance.status = self.cleaned_data['status']
        self.instance.save()

        if 'new_perm_group' in self.changed_data:
            self.instance.add_permission(self.cleaned_data['new_perm_group'], self.cleaned_data['new_perm_perm'])
        perms_id_to_delete = [d[9:] for d in self.changed_data if d.startswith('perm_del')]
        if perms_id_to_delete:
            Permission.objects.filter(pk__in=perms_id_to_delete).delete()

        if self.cleaned_data.get('apply_to_children', False):
            perms = self.instance.permissions.all()
            child_ids = list(self.instance.get_children().values_list('id', flat=True))
            Permission.objects.filter(content_type=ContentType.objects.get_for_model(self.instance),
                object_id__in=child_ids).delete()
            for child in self.instance.get_children():
                if child.status != self.instance.status or child.owner != self.instance.owner:
                    child.status = self.instance.status
                    child.owner = self.instance.owner
                    child.save()
                for perm in perms:
                    perm.pk = None
                    perm.object_id = child.pk
                    perm.save()

