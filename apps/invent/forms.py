# Copyright (c) 2009-2011 Claude Paroz <claude@2xlibre.net>
import os
import re
from collections import OrderedDict

from django import forms
from django.conf import settings
from django.contrib import admin
from django.contrib.admin.widgets import AutocompleteSelect
from django.contrib.auth.models import AnonymousUser
from django.core.files.base import File
from django.db.models import Q
from django.forms.widgets import HiddenInput
from django.http import Http404
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.translation import ngettext, gettext_lazy as _
from django.utils.safestring import mark_safe

from haystack.forms import SearchForm as HaystackSearchForm
from tinymce.widgets import TinyMCE

from .models import (
    Attribute, AttributeForObject, BaseObject, Command, DatatypeChoices,
    FileAttachment, GermTest, ImageSubobject, Lot, ObjectType, ObjectValue
)
from .templatetags.objects import resume


def int_or_404(value):
    try:
        return int(value)
    except ValueError:
        raise Http404('An integer was expected')


class DateInput(forms.DateInput):
    input_type = "date"

    def format_value(self, value):
        return str(value) if value is not None else None


class ParentAutocompleteWidget(AutocompleteSelect):
    def __init__(self, **kwargs):
        super().__init__(
            BaseObject._meta.get_field('parent').remote_field, admin.site,
            attrs={'data-minimum-input-length': 3}, **kwargs
        )

    def get_url(self):
        return reverse('baseobject_autocomplete')


class ObjectEditForm(forms.ModelForm):
    class Meta:
        model = BaseObject
        fields = ('parent', 'title', 'description', 'obj_type', 'status')
        widgets = {
            'parent': ParentAutocompleteWidget,
            'obj_type': forms.HiddenInput(),
            'description': TinyMCE,
        }

    def __init__(self, user=None, obj_type=None, **kwargs):
        class FieldGroup(OrderedDict):
            """ FieldGroup allows to get a field in a list with form.__getitem__ to obtain a BoundField in template """
            def __iter__(self2):
                for attr_name in self2.keys():
                    yield self[attr_name]

        def create_fields_for_attrs(attr_list, grp):
            for attr in attr_list:
                if not attr.is_editable():
                    continue
                initial_val = existing_values.pop(attr.name, None)
                ff = attr.get_form_field(initial=initial_val and initial_val[0] or None)
                if ff is None:
                    continue
                ff.comments = initial_val and initial_val[1] or None
                self.fields[attr.name] = ff
                self.fields[attr.name].is_attr = True
                self.gr_fields[grp.gname][attr.name] = ff

        super().__init__(**kwargs)
        if not self.instance.pk:
            # Parent/obj_type forced by the view on creation.
            del self.fields['parent']
            del self.fields['obj_type']
        existing_values = dict([val.attr.name, (val.get_value(raw=True), val.comments)] for val in self.instance.get_values())
        if self.instance.pk:
            # For existing objects, status is edited in PermissionEditForm
            del self.fields['status']

        self.gr_fields = OrderedDict()
        for group in obj_type.get_groups(categ_exclude=['taxo'], with_not_grouped=True):
            self.gr_fields[group.gname] = FieldGroup()
            create_fields_for_attrs(group.get_attributes(), group)
            last_group = group
        remaining_attrs = Attribute.objects.filter(name__in=[existing_values.keys()])
        create_fields_for_attrs(remaining_attrs, last_group)
        if not self.gr_fields[last_group.gname]:
            # Delete last empty group (Other attributes)
            del self.gr_fields[last_group.gname]

    def clean_parent(self):
        parent = self.cleaned_data.get('parent')
        if parent:
            if not parent.obj_type.can_contain.filter(pk=self.instance.obj_type_id).exists():
                raise forms.ValidationError(f"{parent} ne peut pas contenir d’objet {self.instance.obj_type}")
        return parent

    def save(self, *args, **kwargs):
        is_new = self.instance.pk is None
        super().save(*args, **kwargs)
        # Save attribute values
        if is_new:
            changed_keys = [k for k in self.cleaned_data if self.cleaned_data[k] != '']
        else:
            changed_keys = self.changed_data
        for key in changed_keys:
            if hasattr(self.fields[key], "is_attr"):
                new_value = self.cleaned_data.get(key)
                if new_value:
                    self.instance.set_value_for(key, new_value)

                else:
                    # Existing value to delete
                    value_obj = self.instance.get_value_for(key, as_object=True)

                    if value_obj:
                        value_obj.delete()


# http://www.b-list.org/weblog/2008/nov/09/dynamic-forms/
def get_value_form(obj, attribute, value_obj=None, data=None, with_label=True):
    """ Obtain a form class dynamically depending on the attribute datatype """
    fields = {}
    obj_data = value_obj and value_obj.get_value(raw=True) or None
    fields['value_%s' % attribute.name] = attribute.get_form_field(
        initial=obj_data, with_label=with_label, required=True)
    if attribute.get_datatype() != "text":
        obj_data = value_obj and value_obj.comments or None
        fields['comments'] = forms.CharField(initial=obj_data, required=False, label=_("Comments"))
    for f in fields:
        fields[f].widget.attrs.update({'class':'form_value'})
    fields['descriptor'] = forms.IntegerField(widget=forms.HiddenInput, initial=attribute.id, required=False)
    fields['object']     = forms.IntegerField(widget=forms.HiddenInput, initial=obj.id, required=False)
    new_class = type('ValueForm', (forms.BaseForm,), { 'base_fields': fields })
    return new_class(data)


class AddPhotoForm(forms.Form):
    image  = forms.FileField(label=_("Image"))
    title  = forms.CharField(label=_("Title"), required=False,
        max_length=BaseObject._meta.get_field('title').max_length)

    authorized_exts = ('.gif', '.png', '.jpg', '.jpeg', '.zip')
    class Media:
        css = {
            'all': ('css/forms.css',)
        }

    def clean_image(self):
        data = self.cleaned_data['image']
        if data:
            ext = os.path.splitext(data.name)[1].lower()
            if ext not in self.authorized_exts:
                raise forms.ValidationError(_("Only files with following extensions are admitted: %s") % ", ".join(self.authorized_exts))
        return data

    def save(self, parent=None, *args, **kwargs):
        up_file = self.cleaned_data['image']
        if up_file.name.endswith(".zip"):
            import zipfile, tempfile
            files = []
            zfobj = zipfile.ZipFile(up_file, 'r')
            for name in zfobj.namelist():
                ext = ".%s" % name.rsplit('.', 1)[-1].lower()
                if ext in self.authorized_exts and not name.startswith("/") and not ".." in name:
                    tmp = tempfile.NamedTemporaryFile(suffix=ext)
                    tmp.write(zfobj.read(name))
                    files.append((File(tmp, name=name), name.rsplit('.', 1)[0]))
        else:
            files = [(up_file, self.cleaned_data.get('title', up_file.name.rsplit('.', 1)[0]))]
        for f, title in files:
            # Create base_object of type 'image'
            bo = BaseObject.objects.create(
                obj_type=ObjectType.objects.get(code='image'),
                parent=parent, status=parent.status, owner=parent.owner, title=title)
            # Create image_object
            io = ImageSubobject.objects.create(obj=bo, image=f)
        return len(files)


class FileForm(forms.ModelForm):
    class Meta:
        model = FileAttachment
        fields = ('fil', 'title')
        
    def save(self, obj, *args, **kwargs):
                    
        if not self.cleaned_data['title']:
            # If no title entered, use file name as fallback
            #~ self.cleaned_data['title'] = self.files['fil'].name
            self.instance.title = self.files['fil'].name
        
        if (self.instance.mimetype == ""):
            self.instance.mimetype = self.files['fil'].content_type
        
        self.instance.content_object = obj
        if 'fil' in self.files:
            self.instance.mimetype = self.files['fil'].content_type
        super(FileForm, self).save(*args, **kwargs)


class LotEditForm(forms.ModelForm):
    class Meta:
        model = Lot
        exclude = ['accession']


class GermTestEditForm(forms.ModelForm):
    class Meta:
        model = GermTest
        exclude = ['lot']


class CommandEditForm(forms.ModelForm):
    class Meta:
        model = Command
        exclude = ["lot"]
        widgets = {
            "date_cmd": DateInput
        }
        help_texts = {
            "weight": _("This weight will be automatically removed from the parent lot weight."),
        }

    def save(self, **kwargs):
        if "weight" in self.changed_data:
            # Subtract command weight from the lot weight
            weight_diff = self.cleaned_data["weight"] - (self["weight"].initial or 0)
            if self.instance.lot.weight:
                self.instance.lot.weight -= weight_diff
                self.instance.lot.save()
        return super().save(**kwargs)


class ImportForm(forms.Form):
    xlsfile = forms.FileField(label = _("File to import (.ods/.xls/.csv)"))
    imp_type = forms.ChoiceField(label="Importation type", choices=(
        ('create_acc', 'Create accessions'),
        ('update_acc', 'Update accessions'),
        #('create_lot', 'Create lots'),
        ('update_lot', 'Update lots'),
        #('create_test', 'Create tests'),
        #('update_test', 'Update tests'),
    ), widget=forms.RadioSelect)

    def clean_xlsfile(self):
        data = self.cleaned_data['xlsfile']
        supported = any(snip in data.content_type for snip in ('excel', 'spreadsheetml', 'opendocument.spreadsheet', 'csv'))
        if not supported:
            raise forms.ValidationError(
                _("Sorry, this does not seem to be a valid ods, xlsx or csv file.") + " (content_type=%s)" % data.content_type
            )
        return self.cleaned_data


class SearchForm(forms.Form):
    fulltext = forms.CharField(required=False)
    class Media:
        js = ('js/search.js',)

    oper_type_map = {
        'T': (('icontains', _('contains')), ('exact', _('is exactly')),
              ('istartswith', _('begins with')), ('iendswith', _('ends with'))),  # Text
        'N': (('exact', '='), ('gt', '>'), ('gte', '>='), ('lte', '<='), ('lt', '<')),  #Number
        'D': (('lt', _('before')), ('exact', _('the')), ('gt', _('after'))),  # Date
        'B': (('exact', _('is')),),  # Boolean
        'V': (('exact', _('is')),),  # Vocabulary
    }

    @classmethod
    def decode_attr_name(cls, value):
        """ attribute name may include metadata : ATTR__SUBATTR or ..ATTR """
        attr_name = value
        subattr_name = prefix = ""
        if attr_name and '__' in attr_name:
            attr_name, subattr_name = value.split('__', 1)
        if attr_name and attr_name.startswith(".."):
            prefix = "parent__"
            attr_name = attr_name[2:]
        return attr_name, subattr_name, prefix

    def __init__(self, data=None, user=None, base_num=None, *args, **kwargs):
        super().__init__(data, *args, **kwargs)
        if user is None:
            user = AnonymousUser()
        self.user = user
        attr_list = self._collect_attributes()
        attr_list.sort(key=lambda x: x[1])
        attr_list.insert(0,('', _('Choose an attribute')))
        self.attr_list = attr_list

        self.field_nums = sorted(
            [int_or_404(k.split('_',1)[1]) for k in self.data.keys() if k.startswith('choice')])
        if not self.field_nums:
            if base_num is None:
                base_num = 0
            self.field_nums = [base_num]
        for num in self.field_nums:
            self.fields[f'choice_{num}'] = forms.ChoiceField(
                choices=attr_list,
                widget=forms.Select(attrs={'class':'attr_select'}),
                required=False
            )
            # CharField because content is dynamic on the client forms.ChoiceField(choices=())
            self.fields['oper_%d' % num]   = forms.CharField(required=False, widget=forms.Select)
            self.fields['value_%d' % num]  = forms.CharField(required=False)
            self.fields['vvalue_%d' % num] = forms.CharField(required=False,
                widget=forms.Select(attrs={'class':'hidden'}))
        if data is not None:
            for fname in data.keys():
                if fname[:7] == 'choice_' and data[fname]:
                    num = int(fname.split('_',1)[1])
                    dtype = data[fname][0] # First char of T_ACCENAME
                    if dtype not in self.oper_type_map:
                        continue
                    # Populate opers
                    self.fields['oper_%d' % num].widget.choices = self.oper_type_map.get(dtype)
                    if dtype in ('B', 'V'):
                        self.fields['value_%d' % num] = forms.IntegerField(required=False)
                        self.fields['value_%d' % num].widget.attrs['class'] = 'hidden'
                        self.fields['vvalue_%d' % num].widget.attrs['class'] = ''
                        # Populate vvalue
                        if dtype == 'B':
                            choices = [('1', 'Vrai'), ('0', 'Faux')]
                        else:
                            attr_name, subattr_name, prefix = self.decode_attr_name(data[fname][2:])
                            if subattr_name:
                                attr_name = subattr_name
                            try:
                                attr = Attribute.objects.get(name=attr_name)
                            except Attribute.DoesNotExist:
                                continue
                            choices = [(v['id'],v['name']) for v in attr.get_vocab()]
                        self.fields['vvalue_%d' % num].widget.choices = choices
                    else:
                        self.fields['vvalue_%d' % num].widget.attrs['class'] = 'hidden'
                        if dtype == 'N':
                            self.fields['value_%d' % num] = forms.FloatField(required=False)
        return

    def _collect_attributes(self):
        """ Find all searchable attributes, and return a list of tuples in the form:
            [(V_ATTRNAME, "Attr title), (N_ATTR__SUBATTR, "Attr > Subattr"), ...]
        """
        datatype_map = DatatypeChoices.as_searchtp_dict()
        attr_query = Attribute.objects.used_attributes()
        attr_query = Attribute.objects.used_attributes().exclude(datatype='meta').exclude(searchable=False)
        if self.user.is_anonymous:
            attr_query = attr_query.exclude(private=True)
        attr_list = list(attr_query)
        # Transform Attribute instances in tuples like (PREF_ATTRIBUTE, verbose_name)
        attr_list2 = []
        for attr in attr_list:
            if isinstance(attr, Attribute):
                attr_list2.append(
                    ("%s_%s" % (datatype_map.get(attr.get_datatype()), attr.name), attr.atitle))
            else:
                attr_list2.append(attr) # already formatted
        return attr_list2

    def clean(self):
        if self.errors:
            return self.cleaned_data
        # At least one choice_ should be set up
        cleaned_data = self.cleaned_data
        fulltext = cleaned_data.get('fulltext')
        choices = filter(lambda x: x[0].startswith('choice_') and x[1], cleaned_data.items())
        values  = filter(lambda x: (x[0].startswith('value_') or x[0].startswith('vvalue_')) and x[1] != '', cleaned_data.items())
        if (not fulltext) and (not choices or not values):
            raise forms.ValidationError(_("You have to choose at least one attribute with a value for searching"))
        for key, val in choices:
            # Check all opers are present
            if not cleaned_data['oper_%s' % key.split('_')[-1]]:
                raise forms.ValidationError(_("One search operator is missing"))
        return cleaned_data

    def search(self):
        # public or (private and user=owner) or (permission group in (list of user groups))
        ugroups = [g.id for g in self.user.groups.all()]
        # TODO: add possibility to limit search to an inventory (with mptt get_descendants() query)
        query = BaseObject.objects.filter(
            Q(permissions__group__pk__in=ugroups) |
            Q(status="public") |
            Q(status="private", owner__pk=self.user.id)
        )
 
        for num in self.field_nums:
            choice = self.cleaned_data['choice_%d' % num]
            if not choice:
                continue
            dtype, attr_name = choice.split('_', 1)
            value = self.cleaned_data['vvalue_%d' % num] if dtype in ('B', 'V') else self.cleaned_data['value_%d' % num]
            if value == '':
                continue
            
            operator = str(self.cleaned_data['oper_%d' % num])
            attr_name, subattr_name, prefix = self.decode_attr_name(attr_name)
            attr = Attribute.objects.get(name=attr_name)           
            if subattr_name:
                # Search one level deeper (object -> value -> object -> value) with a subquery
                subattr = Attribute.objects.get(name=subattr_name)
                subquery = eval(attr.queryset)
                subquery_dict = {'%sobjectvalue__attr__pk' % prefix: subattr.id,
                                 '%sobjectvalue__%s__%s' % (prefix, subattr.get_value_field(), operator): value}
                subquery = subquery.filter(**subquery_dict)
                query_dict = {'%sobjectvalue__attr__pk' % prefix: attr.id,
                              '%sobjectvalue__value_int__in' % prefix: subquery}
            else:
                query_dict = {'%sobjectvalue__attr__pk' % prefix: attr.id,
                              '%sobjectvalue__%s__%s' % (prefix, attr.get_value_field(), operator): value}
            query = query.filter(**query_dict)

        fulltext = self.cleaned_data.get('fulltext')
        if fulltext:
            filter = Q(title__icontains = fulltext) | Q(description__icontains = fulltext)
            filter = filter | Q(objectvalue__value_text__icontains = fulltext)
            filter = filter | Q(objectvalue__value_voc__value__icontains = fulltext)
            try:
                filter = filter | Q(objectvalue__value_int = int(fulltext))
            except ValueError:
                pass
            try:
                filter = filter | Q(objectvalue__value_float = float(fulltext))
            except ValueError:
                pass
            query = query.filter(filter)
        
        return query.distinct().values('id')

    def search_old(self):
        # public or (private and user=owner) or (permission group in (list of user groups))
        ugroups = [g.id for g in self.user.groups.all()]
        # TODO: add possibility to limit search to an inventory (with mptt get_descendants() query)
        query = BaseObject.objects.filter(Q(permissions__group__pk__in=ugroups) | Q(status="public") | Q(status="private", owner__pk=self.user.id))
 
        object_type_id = self.cleaned_data['object_type']
            
        if object_type_id != 0:
            query = query.filter(obj_type__id=object_type_id)
        exacts = {}
        
        for num in self.field_nums:
            choice = self.cleaned_data['choice_%d' % num]
            
            if not choice:
                continue
                
            dtype, attr_name = choice.split('_', 1)
            value = dtype == 'V' and self.cleaned_data['vvalue_%d' % num] or self.cleaned_data['value_%d' % num]

            if value == '':
                continue
            operator = str(self.cleaned_data['oper_%d' % num])

            attr_name, subattr_name, prefix = self.decode_attr_name(attr_name)
            attr = Attribute.objects.get(name=attr_name)
            type_attr = attr.code[0:3]
            if subattr_name:
                # Search one level deeper (object -> value -> object -> value) with a subquery. It isn't use in Search.
                subattr = Attribute.objects.get(name=attr_name)
                subquery = eval(attr.queryset)
                subquery_dict = {'%sobjectvalue__attr__pk' % prefix: subattr.id,
                                 '%sobjectvalue__%s__%s' % (prefix, subattr.get_value_field(), operator): value}
                subquery = subquery.filter(**subquery_dict)

                query_filter = Q(**{
                    '%sobjectvalue__attr__pk' % prefix: attr.id,
                    '%sobjectvalue__value_int__in' % prefix: subquery}
                )
            else:
                query_filter = Q(**{
                    '%sobjectvalue__attr__pk' % prefix: attr.id,
                    '%sobjectvalue__%s__%s' % (prefix, attr.get_value_field(), operator): value}
                )

            if operator == 'exact':
                prev_filter = exacts.get(choice)
                prev_filter = prev_filter and prev_filter | query_filter or query_filter
                exacts[choice] = prev_filter
            else:
                query = query.filter(query_filter)
                
        for query_filter in exacts.itervalues():
            query = query.filter(query_filter)
            
        fulltext = self.cleaned_data.get('fulltext')
        if fulltext:
            filter = Q(title__icontains = fulltext) | Q(description__icontains = fulltext)
            filter = filter | Q(objectvalue__value_text__icontains = fulltext)
            filter = filter | Q(objectvalue__value_voc__value__icontains = fulltext)
            try:
                filter = filter | Q(objectvalue__value_int = int(fulltext))
            except ValueError:
                pass
            try:
                filter = filter | Q(objectvalue__value_float = float(fulltext))
            except ValueError:
                pass
            query = query.filter(filter)
                        
        results = query.distinct().values('id')
  
        return results, object_type_id

    def next_num(self):
        return self.field_nums[-1] + 1

    def criterion(self):
        """ Used in template to render criterion lines """
        for num in self.field_nums:
            yield (self['choice_%d' % num], self['oper_%d' % num], self['value_%d' % num], self['vvalue_%d' % num])


class ImageSearchForm(HaystackSearchForm):
    def no_query_found(self):
        if self.is_valid():
            # Return all elements when no search term is entered
            return self.searchqueryset.all()
        return super(ImageSearchForm, self).no_query_found()
