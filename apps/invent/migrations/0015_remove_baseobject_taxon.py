from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0014_remove_attribute_subattributes'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='baseobject',
            name='taxon',
        ),
    ]
