from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0012_command_model'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attribute',
            name='form_template',
        ),
    ]
