from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0011_remove_old_bo_fields'),
    ]

    operations = [
        migrations.CreateModel(
            name='Command',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designation', models.CharField(blank=True, max_length=200, verbose_name='Designation')),
                ('date_cmd', models.DateField(blank=True, null=True, verbose_name='Command date')),
                ('weight', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Weight [g]')),
                ('comment', models.TextField(blank=True)),
                ('lot', models.ForeignKey(on_delete=models.deletion.PROTECT, to='invent.lot')),
            ],
        ),
    ]
