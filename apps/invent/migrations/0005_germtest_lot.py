from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0004_populate_institute'),
    ]

    operations = [
        migrations.CreateModel(
            name='Lot',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('designation', models.CharField(max_length=60, blank=True, verbose_name='Designation')),
                ('creation', models.DateField(blank=True, null=True, verbose_name='Creation date')),
                ('annee_recolte', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Année de récolte')),
                ('transfert', models.DateField(blank=True, null=True, verbose_name='Date de transfert')),
                ('retire', models.BooleanField(default=False, verbose_name='Lot retiré')),
                ('germ_rate_initial', models.SmallIntegerField(blank=True, null=True, verbose_name='Initial germination rate')),
                ('pmg', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Poids mille grains [g]')),
                ('weight', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Weight [g]')),
                ('total_weight', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True, verbose_name='Total weight [g]')),
                ('nb_sachet', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Nb sachets')),
                ('phys_location', models.CharField(blank=True, choices=[('col_active', 'collection active (frigo)'), ('col_base', 'collection de base (congélateur)'), ('col_cons', 'collection de conservation (externe)'), ('commande', 'commande (externe)'), ('multiplic', 'en multiplication')], max_length=12, verbose_name='Location (physical genebank)')),
                ('parcelle', models.CharField(blank=True, max_length=30, verbose_name='Parcelle')),
                ('box_id', models.CharField(blank=True, max_length=50, verbose_name='Box ID')),
                ('loc_x', models.CharField(blank=True, max_length=20, verbose_name='Bloc (coordonnée X)')),
                ('loc_y', models.CharField(blank=True, max_length=20, verbose_name='Rangée (coordonnée Y)')),
                ('loc_z', models.CharField(blank=True, max_length=20, verbose_name='Étagère (coordonnée Z)')),
                ('affectation', models.CharField(blank=True, choices=[('commande', 'Commande'), ('conserv', 'Conservation'), ('multiplic', 'Multiplication'), ('retrait', 'Retrait'), ('temoin', 'Lot témoin'), ('test', 'Test de germination'), ('transfert', 'Transfert')], max_length=10, verbose_name='Affectation')),
                ('comments', models.TextField(blank=True, verbose_name='Comments')),
                ('lot_code1', models.CharField(blank=True, max_length=60, verbose_name='Code lot 1')),
                ('lot_code2', models.CharField(blank=True, max_length=60, verbose_name='Code lot 2')),
                ('lot_code3', models.CharField(blank=True, max_length=60, verbose_name='Code lot 3')),
                ('accession', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.BaseObject')),
                ('instcode', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='invent.Institute')),
                ('lot_parent', models.CharField(blank=True, max_length=50, verbose_name='Lot parent')),
            ],
        ),
        migrations.CreateModel(
            name='GermTest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_test', models.DateField(blank=True, null=True, verbose_name='Date du test')),
                ('numero', models.CharField(blank=True, max_length=30, verbose_name='Numéro du test')),
                ('termine', models.BooleanField(default=False, verbose_name='Test terminé')),
                ('acc_num', models.CharField(blank=True, max_length=50)),
                ('nombre', models.PositiveSmallIntegerField(blank=True, choices=[(1, '25'), (2, '50'), (3, '100'), (4, '150'), (5, '200')], null=True, verbose_name='Nombre de semences testées')),
                ('taux1', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux 1er comptage des plantules normales')),
                ('taux2', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux 2ème comptage des plantules normales')),
                ('taux3', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux 3ème comptage des plantules normales')),
                ('taux_norm', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Résultat final plantules normales en %')),
                ('taux_anorm', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux de plantules anormales')),
                ('taux_durs', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux dures/dormantes')),
                ('taux_morts', models.PositiveSmallIntegerField(blank=True, null=True, verbose_name='Taux de plantules mortes')),
                ('comments', models.TextField(blank=True, verbose_name='Comments')),
                ('lot', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.Lot')),
            ],
        ),
    ]
