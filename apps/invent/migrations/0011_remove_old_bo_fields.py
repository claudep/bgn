from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0010_delete_objectvaluestats'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='germtest',
            name='old_bo',
        ),
        migrations.RemoveField(
            model_name='lot',
            name='old_bo',
        ),
    ]
