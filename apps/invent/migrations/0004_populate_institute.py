from django.db import migrations


def pop_institutes(apps, schema_editor):
    VocabValue = apps.get_model('invent', 'VocabValue')
    Institute = apps.get_model('invent', 'Institute')
    for val in VocabValue.objects.filter(vocab__name='Institute codes'):
        Institute.objects.create(code=val.value_code, name=val.value)
    

def migrate_vocabs(apps, schema_editor):
    Attribute = apps.get_model('invent', 'Attribute')
    Institute = apps.get_model('invent', 'Institute')

    for attr in Attribute.objects.filter(vocab__name='Institute codes'):
        attr.datatype = 'qset'
        attr.queryset = 'Institute.objects.all()'
        attr.vocab = None
        attr.save()
        for value in attr.objectvalue_set.all():
            value.value_int = Institute.objects.get(code=value.value_voc.value_code).pk
            value.value_voc = None
            value.save()
    

class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0003_institute'),
    ]

    operations = [
        migrations.RunPython(pop_institutes, migrations.RunPython.noop),
        migrations.RunPython(migrate_vocabs, migrations.RunPython.noop),
    ]
