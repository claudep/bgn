from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0006_attribute_inheritable'),
    ]

    operations = [
        migrations.AddField(
            model_name='germtest',
            name='old_bo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='invent.baseobject'),
        ),
        migrations.AddField(
            model_name='lot',
            name='old_bo',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='+', to='invent.baseobject'),
        ),
    ]
