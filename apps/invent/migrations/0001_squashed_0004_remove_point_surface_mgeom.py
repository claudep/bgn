from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import invent.fields
import invent.models
import permission.models
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('contenttypes', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('taxo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vocabulary',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=35, unique=True)),
                ('ordering', models.CharField(choices=[('code', 'By value code'), ('name', 'By translated name')], default='code', max_length=10)),
            ],
            options={
                'db_table': 'vocabulary',
            },
        ),
        migrations.CreateModel(
            name='VocabValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value_code', models.CharField(max_length=10)),
                ('value', models.CharField(max_length=120)),
                ('vocab', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.Vocabulary')),
            ],
            options={
                'ordering': ('value_code',),
                'db_table': 'vocab_value',
            },
        ),
        migrations.CreateModel(
            name='AttributeGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('gname', models.CharField(max_length=50, unique=True)),
                ('category', models.CharField(choices=[('general', 'General'), ('a0', 'Specimens - A0'), ('list', 'Lists/Inventories'), ('taxo', 'Taxonomy'), ('statistics', 'Statistics')], default='general', max_length=15)),
                ('position', models.IntegerField(default=0)),
            ],
            options={
                'db_table': 'attribute_group',
            },
        ),
        migrations.CreateModel(
            name='Attribute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.SlugField(max_length=35, unique=True)),
                ('active', models.BooleanField(db_index=True, default=True)),
                ('private', models.BooleanField(default=False, help_text='Only visible by users having write access to the target object')),
                ('atitle', models.CharField(max_length=50)),
                ('code', models.CharField(blank=True, max_length=35, null=True)),
                ('ahelp', models.TextField(blank=True)),
                ('datatype', models.CharField(choices=[('text', 'Text'), ('integer', 'Integer'), ('float', 'Float'), ('boolean', 'Boolean'), ('date', 'Date'), ('meta', 'Metaattribute'), ('speclist', 'List of species'), ('qset', 'Queryset')], max_length=35)),
                ('searchable', models.BooleanField(default=True)),
                ('multivalued', models.BooleanField(default=False)),
                ('calculator', models.CharField(blank=True, max_length=35, null=True)),
                ('queryset', models.CharField(blank=True, max_length=100, null=True)),
                ('position_in_group', models.IntegerField(default=0)),
                ('form_template', models.TextField(blank=True, null=True)),
                ('render_template', models.TextField(blank=True, null=True)),
                ('vocab', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.PROTECT, to='invent.Vocabulary')),
                ('group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='invent.AttributeGroup')),
            ],
            options={
                'ordering': ('name',),
                'db_table': 'attribute',
            },
        ),
        migrations.CreateModel(
            name='ObjectType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('code', models.SlugField(max_length=20)),
                ('tname', models.CharField(max_length=50, unique=True)),
                ('icon', models.ImageField(blank=True, upload_to='icons')),
                ('searchable', models.BooleanField(default=False, help_text="Available in 'Search in' select on search page")),
                ('can_contain', models.ManyToManyField(blank=True, to='invent.ObjectType')),
            ],
            options={
                'db_table': 'object_type',
            },
        ),
        migrations.CreateModel(
            name='BaseObject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('uuid', invent.fields.UuidField(blank=True, editable=False, max_length=36, unique=True)),
                ('title', models.CharField(max_length=100, verbose_name='Title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='Description')),
                ('status', models.CharField(choices=[('private', 'Private'), ('public', 'Public')], default='private', max_length=10)),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, to=settings.AUTH_USER_MODEL)),
                ('obj_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.ObjectType')),
                ('taxon', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to='taxo.Taxon')),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('modified', models.DateTimeField(auto_now=True)),
                ('weight', models.SmallIntegerField(default=0, help_text='Higher numbers will make the item appear first in some lists')),
                ('lft', models.PositiveIntegerField(editable=False)),
                ('rght', models.PositiveIntegerField(editable=False)),
                ('tree_id', models.PositiveIntegerField(db_index=True, editable=False)),
                ('level', models.PositiveIntegerField(editable=False)),
            ],
            options={
                'db_table': 'base_object',
                'get_latest_by': 'modified',
            },
            bases=(models.Model, permission.models.PermissionMixin),
        ),
        migrations.AddField(
            model_name='baseobject',
            name='parent',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='children', to='invent.BaseObject'),
        ),
        migrations.CreateModel(
            name='FileAttachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('fil', models.FileField(upload_to='attachments', verbose_name='File')),
                ('title', models.TextField(blank=True, verbose_name='Title')),
                ('mimetype', models.CharField(blank=True, editable=False, max_length=100)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'db_table': 'fileattachment',
            },
        ),
        migrations.CreateModel(
            name='ImageSubobject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('image', sorl.thumbnail.fields.ImageField(upload_to='photos')),
                ('obj', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='invent.BaseObject')),
            ],
            options={
                'db_table': 'image_subobject',
            },
        ),
        migrations.CreateModel(
            name='ObjectValueStats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value_int', models.IntegerField(blank=True, null=True, validators=[invent.models.int_validator])),
                ('value_float', models.FloatField(blank=True, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('attr', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='stat_val', to='invent.Attribute')),
                ('attr_subj', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='target_val', to='invent.Attribute')),
                ('obj', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.BaseObject')),
            ],
            options={
                'db_table': 'object_value_stats',
            },
        ),
        migrations.CreateModel(
            name='PhotoAttachment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('photo', sorl.thumbnail.fields.ImageField(upload_to='photos')),
                ('title', models.TextField(blank=True, verbose_name='Title')),
                ('order', models.IntegerField(default=0)),
                ('object_id', models.PositiveIntegerField()),
                ('content_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='contenttypes.ContentType')),
            ],
            options={
                'db_table': 'photoattachment',
            },
        ),
        migrations.CreateModel(
            name='ObjectValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('value_text', models.TextField(blank=True, null=True)),
                ('value_int', models.IntegerField(blank=True, null=True, validators=[invent.models.int_validator])),
                ('value_float', models.FloatField(blank=True, null=True)),
                ('comments', models.TextField(blank=True, null=True)),
                ('attr', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.Attribute')),
                ('obj', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.BaseObject')),
                ('value_voc', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='invent.VocabValue')),
            ],
            options={
                'db_table': 'object_value',
            },
        ),
        migrations.CreateModel(
            name='AttributeInGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.IntegerField(default=0)),
                ('attribute', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.Attribute')),
                ('group', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.AttributeGroup')),
            ],
            options={
                'db_table': 'attribute_in_group',
                'unique_together': {('attribute', 'group')},
            },
        ),
        migrations.AddField(
            model_name='attributegroup',
            name='attributes',
            field=models.ManyToManyField(through='invent.AttributeInGroup', to='invent.Attribute'),
        ),
        migrations.CreateModel(
            name='AttributeForObject',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('status', models.CharField(blank=True, max_length=15, null=True)),
                ('position', models.IntegerField(default=0)),
                ('searchable_in_child', models.BooleanField(default=False)),
                ('attr', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='invent.Attribute')),
                ('attr_group', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='invent.AttributeGroup')),
                ('obj_type', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='invent.ObjectType')),
            ],
            options={
                'ordering': ['obj_type', 'position'],
                'db_table': 'attribute_for_object',
            },
        ),
        migrations.AddField(
            model_name='attribute',
            name='subattributes',
            field=models.ManyToManyField(blank=True, related_name='subattributes_rel_+', to='invent.Attribute'),
        ),
        migrations.CreateModel(
            name='ImageObject',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.baseobject',),
        ),
        migrations.CreateModel(
            name='ObjectValueBoolean',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueDate',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueQueryset',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
        migrations.CreateModel(
            name='ObjectValueVocab',
            fields=[
            ],
            options={
                'proxy': True,
            },
            bases=('invent.objectvalue',),
        ),
    ]
