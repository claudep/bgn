from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0009_lot_add_nb_plants'),
    ]

    operations = [
        migrations.DeleteModel(
            name='ObjectValueStats',
        ),
    ]
