from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0001_squashed_0004_remove_point_surface_mgeom'),
    ]

    operations = [
        migrations.DeleteModel(
            name='PhotoAttachment',
        ),
    ]
