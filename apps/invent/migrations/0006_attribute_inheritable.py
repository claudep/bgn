from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0005_germtest_lot'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attribute',
            name='group',
        ),
        migrations.RemoveField(
            model_name='attribute',
            name='position_in_group',
        ),
        migrations.AddField(
            model_name='attribute',
            name='inheritable',
            field=models.BooleanField(default=True),
        ),
    ]
