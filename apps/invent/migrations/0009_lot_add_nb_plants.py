from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0008_migrate_lot_and_test'),
    ]

    operations = [
        migrations.AddField(
            model_name='lot',
            name='nb_plants',
            field=models.IntegerField(blank=True, null=True, verbose_name='Nb de plants'),
        ),
    ]
