from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0002_delete_photoattachment'),
    ]

    operations = [
        migrations.CreateModel(
            name='Institute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('code', models.CharField(max_length=10, unique=True)),
            ],
        ),
    ]
