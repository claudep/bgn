from decimal import Decimal
from django.core.exceptions import ValidationError
from django.db import migrations


def format_date(dt):
    if dt is None:
        return None
    if dt.count('-') == 2:
        return dt
    if dt.count('.') == 2:
        day, mon, year = dt.split('.')
        return '-'.join([year, mon, day])
    if dt.count('.') == 1:
        mon, year = dt.split('.')
        return '-'.join([year, mon, '1'])
    try:
        dt = int(dt)
        if 2022 > dt > 1980:
            return str(dt) + '-1-1'
    except Exception:
        pass
    print(f"Unable to convert {dt} to a date")
    return None


def to_decimal(val):
    if val is None:
        return None
    if '.' in str(val) and len(str(val).split('.')[1]) > 2:
        return round(Decimal(str(val)), 2)
    return Decimal(str(val))


def migrate_lots(apps, schema_editor):
    # Getting "real" BaseObject as we need its methods
    from invent.models import BaseObject
    #BaseObject = apps.get_model('invent', 'BaseObject')
    Lot = apps.get_model('invent', 'Lot')
    GermTest = apps.get_model('invent', 'GermTest')
    Institute = apps.get_model('invent', 'Institute')
    # ~28200 lots
    counter = 0
    for lot_obj in BaseObject.objects.filter(obj_type__code='lot'):
        counter += 1
        if counter % 1000 == 0:
            print(f'{counter} lots converted')
        lot_obj._fill_cache()
        new_lot = Lot(accession_id=lot_obj.parent_id, old_bo_id=lot_obj.pk, designation=lot_obj.title)
        lot_num = lot_obj.get_value_for('LOT_NUMERO')
        if lot_num and lot_num != lot_obj.title:
            new_lot.designation += f' / {lot_num}'
        new_lot.creation = format_date(lot_obj.get_value_for('LOT_DATE_CREATION'))
        new_lot.annee_recolte = lot_obj.get_value_for('LOT_ANNEE_RECOLTE')
        new_lot.transfert = format_date(lot_obj.get_value_for('LOT_DATE_TRANSFERT'))
        new_lot.lot_parent = lot_obj.get_value_for('LOT_PARENT') or ''
        new_lot.retire = lot_obj.get_value_for('LOT_RETIRE') == 'oui'
        new_lot.germ_rate_initial = lot_obj.get_value_for('LOT_TAUX_GERM_INITIAL')
        new_lot.pmg = to_decimal(lot_obj.get_value_for('LOT_PMG'))
        new_lot.weight = to_decimal(lot_obj.get_value_for('LOT_POIDS'))
        new_lot.total_weight = lot_obj.get_value_for('LOT_POIDS_TOT')
        new_lot.nb_sachet = lot_obj.get_value_for('LOT_NB_SACHETS')
        if lot_obj.get_value_for('LOT_EMPLACEMENT'):
            new_lot.phys_location = lot_obj.get_value_for('LOT_EMPLACEMENT', as_object=True).value_voc.value_code
        instcode = lot_obj.get_value_for('LOT_INSTCODE', as_object=True)
        if instcode:
            new_lot.instcode = Institute.objects.get(pk=instcode.value_int)
        new_lot.parcelle = lot_obj.get_value_for('LOT_PARCELLE') or ''
        new_lot.box_id = lot_obj.get_value_for('LOT_CAISSE') or ''
        new_lot.loc_x = lot_obj.get_value_for('LOT_BLOC') or ''
        new_lot.loc_y = lot_obj.get_value_for('LOT_RANGEE') or ''
        new_lot.loc_z = lot_obj.get_value_for('LOT_ETAGERE') or ''
        if lot_obj.get_value_for('LOT_AFFECTATION'):
            new_lot.affectation = lot_obj.get_value_for('LOT_AFFECTATION', as_object=True).value_voc.value_code
        new_lot.comments = lot_obj.get_value_for('LOT_COMMENTAIRE') or ''
        new_lot.lot_code1 = lot_obj.get_value_for('LOT_CODE_1') or ''
        new_lot.lot_code2 = lot_obj.get_value_for('LOT_CODE_2') or ''
        new_lot.lot_code3 = lot_obj.get_value_for('LOT_CODE_3') or ''
        try:
            new_lot.full_clean()
        except Exception as err:
            print(err)
            print(f'Unable to convert BaseObject Lot with pk {lot_obj.pk}')
            continue
        new_lot.save()
        for test_obj in lot_obj.get_children():
            test_obj._fill_cache()
            new_test = GermTest(lot=new_lot, old_bo_id=test_obj.pk)
            date_raw = test_obj.get_value_for('TEST_DATE')
            if not date_raw and test_obj.get_value_for('LOT_DATE_TEST_DERNIER'):
                date_raw = test_obj.get_value_for('LOT_DATE_TEST_DERNIER')
            if date_raw:
                if date_raw == '2013':
                    new_test.date_test = '2013-1-1'
                else:
                    new_test.date_test = format_date(date_raw)
            new_test.numero = test_obj.get_value_for('TEST_NUMERO') or ''
            new_test.termine = test_obj.get_value_for('TEST_TERMINE') == 'oui'
            new_test.acc_num = test_obj.get_value_for('TEST_ACC_NUMERO') or ''
            nombre = test_obj.get_value_for('TEST_NOMBRE', as_object=True)
            new_test.nombre = nombre.value_voc.value_code if nombre else None
            new_test.taux1 = test_obj.get_value_for('TEST_TAUX_1')
            new_test.taux2 = test_obj.get_value_for('TEST_TAUX_2')
            new_test.taux3 = test_obj.get_value_for('TEST_TAUX_3')
            new_test.taux_norm = test_obj.get_value_for('TEST_NORMALES')
            new_test.taux_anorm = test_obj.get_value_for('TEST_ANORMALES')
            new_test.taux_durs = test_obj.get_value_for('TEST_DURS')
            new_test.taux_morts = test_obj.get_value_for('TEST_MORTES')
            new_test.comments = test_obj.get_value_for('TEST_COMMENTAIRE') or ''
            try:
                new_test.full_clean()
            except ValidationError:
                import pdb; pdb.set_trace()
                continue
            new_test.save()


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0007_add_old_bo_to_lot_and_tests'),
    ]

    operations = [
        migrations.RunPython(migrate_lots, migrations.RunPython.noop)
    ]
