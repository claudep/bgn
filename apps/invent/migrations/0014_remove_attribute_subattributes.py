from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('invent', '0013_remove_attribute_form_template'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='attribute',
            name='subattributes',
        ),
        migrations.AlterField(
            model_name='attribute',
            name='datatype',
            field=models.CharField(choices=[('text', 'Text'), ('integer', 'Integer'), ('float', 'Float'), ('boolean', 'Boolean'), ('date', 'Date'), ('year', 'Year'), ('speclist', 'List of species'), ('qset', 'Queryset')], max_length=35),
        ),
    ]
