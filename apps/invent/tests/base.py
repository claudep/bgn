import os

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase

from invent.models import (
    Attribute, AttributeForObject, BaseObject, Institute, ObjectType, Vocabulary,
    VocabValue
)


def test_in_english(fun):
    """ Decorator to force using English as current language for a test """
    def _decorator(self):
        old_LANGUAGES = settings.LANGUAGES
        old_LANGUAGE_CODE = settings.LANGUAGE_CODE
        settings.LANGUAGES = (('en', 'English'),)
        settings.LANGUAGE_CODE = 'en'
        fun(self)
        settings.LANGUAGES = old_LANGUAGES
        settings.LANGUAGE_CODE = old_LANGUAGE_CODE
        return

    return _decorator


class BaseTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()
        #data.load_vocs()
        #data.load_attrs()
        #data.load_object_types()

        cls.user = User.objects.create_user('john', 'doe@example.org', 'johnpw')

        cls.img_type, _ = ObjectType.objects.get_or_create(code="image", tname="Image")
        cls.spec_type, _ = ObjectType.objects.get_or_create(code="accession", tname="Accession", searchable=True)
        cls.spec_type.can_contain.set([cls.img_type])
        cls.list_type, _ = ObjectType.objects.get_or_create(code="inventory", tname="Inventory", searchable=True)
        cls.list_type.can_contain.set([cls.img_type, cls.spec_type])
        ObjectType.objects.get_or_create(code="lot", tname="Lot")

        Institute.objects.create(code='CHE067', name="Jardin botanique de l'Université de Fribourg")
        Institute.objects.create(code='AUS002', name="Western Australian Department of Agriculture")
        txt_attr = Attribute.objects.create(name="ATTR_TEXT", datatype="text", atitle="Test attribute")
        int_attr = Attribute.objects.create(name="ATTR_INT", datatype="integer", atitle="Test integer attribute")
        Attribute.objects.get_or_create(name="ACC_NUMERO", defaults={'atitle': "Numéro d’accession", 'datatype': 'text'})
        Attribute.objects.get_or_create(name="ACC_NOM", defaults={'atitle': "Nom d’accession", 'datatype': 'text'})
        Attribute.objects.get_or_create(name="LOT_POIDS", defaults={'atitle': "Poids du lot", 'datatype': 'float'})

        cls.voc_yesno, created = Vocabulary.objects.get_or_create(name="YesNo", ordering='code')
        if created:
            VocabValue.objects.create(vocab=cls.voc_yesno, value='oui', value_code='yes')
            VocabValue.objects.create(vocab=cls.voc_yesno, value='non', value_code='no')
        Attribute.objects.create(name="ATTR_VOCAB", datatype="text", atitle="Test vocab attribute",
            vocab=cls.voc_yesno)
        Attribute.objects.create(name="ATTR_QUERYSET", datatype="qset", atitle="Queryset attribute",
            queryset="BaseObject.objects.filter(obj_type__code='accession')")
        date_attr = Attribute.objects.create(name="ATTR_DATE", datatype="date", atitle="Date attribute")

        AttributeForObject.objects.create(obj_type=cls.spec_type, attr=txt_attr)
        AttributeForObject.objects.create(obj_type=cls.spec_type, attr=int_attr)
        AttributeForObject.objects.create(obj_type=cls.spec_type, attr=date_attr)

        cls.inventory = BaseObject.objects.create(
            title="Test inventory",
            obj_type = cls.list_type,
            description="Description text.",
            owner= cls.user,
            status="public",
        )

    def tearDown(self):
        # Delete uploaded media photo().png/.jpg
        for f in os.listdir("./media/photos"):
            if f.startswith("photo"):
                os.remove("./media/photos/%s" % f)

    def _create_object(self, o_type, values=None, **data):
        """ Create a BasicObject """
        base_data = {
            'title': "Test obj",
            'obj_type': o_type,
            'description': "Description text.",
            'owner': self.user,
            'status': 'public',
        }
        base_data.update(data)
        obj = BaseObject.objects.create(**base_data)
        if values:
            for key, val in values.items():
                obj.set_value_for(key, val)
        return obj
