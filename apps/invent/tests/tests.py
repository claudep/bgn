import json
import os

from django.conf import settings
from django.core import mail
from django.core.exceptions import ValidationError
from django.core.files.base import File
from django.contrib.auth.models import User, Group
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext as _
   
from invent import models
from invent import forms
from invent.data import data
from invent.utils import default_language

from invent.tests.base import BaseTestCase, test_in_english


class InventTestCase(BaseTestCase):
    def loadInitialData(self):
        """ Test loading initial data (currently deactivated) """
        from django.core.management import call_command
        # Empty the database
        call_command('flush', verbosity=0, interactive=False)
        call_command("load-data")

    def test_home_page(self):
        """ Test home page access, anonymous, logged user, superuser """
        # anonymous
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, _("Login"))
        # 'simple' logged user
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "john")
        self.assertNotContains(response, ">Administration<")
        # staff logged user
        self.user.is_staff = True
        self.user.save()
        response = self.client.get(reverse('home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "john")
        self.assertContains(response, ">Administration<")

    def test_load_attributes(self):
        test_attributes = (
            {'name':"TEST1", 'title':{'en':"Test attr #1", 'de':"Test attr 1", 'fr':u"Test attr. n°1"},
            'code':None, 'help':"Help text of the first test attribute", 'datatype':"text", 'vocab':None},
            {'name':"TEST2", 'title':{'en':"Test attr #2", 'de':"Test attr 2", 'fr':u"Test attr. n°2"},
            'code':None, 'help':"Help text of the second test attribute", 'datatype':"integer", 'vocab':None},
        )
        data.load_attrs(test_attributes)
        attr = models.Attribute.objects.get(name='TEST1')
        self.assertEqual(attr.atitle, test_attributes[0]['title'].get(default_language(), None) or test_attributes[0]['title']['en'])
        if hasattr(attr, 'atitle_fr'):
            self.assertEqual(attr.atitle_fr, u"Test attr. n°1")

    def test_list_attributes(self):
        response = self.client.get(reverse('attributes'))
        self.assertContains(response, 'ATTR_QUERYSET')

    def test_add_attribute_value(self):
        attr_name = "ATTR_INT"
        attr = models.Attribute.objects.get(name=attr_name)
        url = reverse('add_value', args=[self.inventory.id, attr.id])
        # Test to add a value anonymously
        response = self.client.post(url,
                          {'attribute': attr.id, 'value_%s' % attr_name: '658'})
        self.assertRedirects(response, reverse('login') + "?next=" + url)

        self.assertTrue(self.client.login(username='john', password='johnpw'))
        # Test to add a bad value
        response = self.client.post(url,
                          {'attribute': attr.id, 'value_%s' % attr_name: 'abcd'})
        self.assertEqual(response.status_code, 403)
        # Test with a good value
        response = self.client.post(url,
                          {'attribute': attr.id, 'value_%s' % attr_name: '658'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(self.inventory.get_value_for(attr_name), 658)
        # Try to set a vocabulary value
        attr_name = "ATTR_VOCAB"
        attr = models.Attribute.objects.get(name=attr_name)
        response = self.client.post(reverse('add_value', args=[self.inventory.id, attr.id]),
                          {'attribute': attr.id, 'value_%s' % attr_name: 'yes'})
        self.assertEqual(self.inventory.get_value_for(attr_name), "oui")

    def testModifyAttributeVocab(self):
        """
        Test when the vocab of an attribute is modified, related values are
        migrated to the new vocab, if possible.
        """
        data.load_attrs(({'name':"MODVOC",'title':"Test attr", 'code':None, 'datatype':"text", 'vocab': "YesNo"},))
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        new_obj.set_value_for('MODVOC', 'yes')

        yesnouncertain_voc = models.Vocabulary.objects.create(name="YesNoUncertain")
        models.VocabValue.objects.create(vocab=yesnouncertain_voc, value='oui', value_code='yes')
        models.VocabValue.objects.create(vocab=yesnouncertain_voc, value='non', value_code='no')
        models.VocabValue.objects.create(vocab=yesnouncertain_voc, value='incertain', value_code='uncertain')

        # Change the vocab of the attribute (compatible value), value should be migrated
        attr = models.Attribute.objects.get(name='MODVOC')
        attr.vocab = yesnouncertain_voc
        attr.save()
        val = models.ObjectValue.objects.get(obj=new_obj)
        self.assertEqual(val.value_voc.vocab, yesnouncertain_voc)
        self.assertTrue(val.get_value() in ('yes', 'ja', 'oui'))
        # Change the vocab of the attribute (unexisting value in new vocab), change should generate error
        new_obj.set_value_for('MODVOC', 'uncertain')
        attr.vocab = self.voc_yesno
        self.assertRaises(ValueError, attr.save)

    def testAddDateValue(self):
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        # Unvalid dates
        self.assertRaises(ValidationError, new_obj.set_value_for, 'ATTR_DATE', '3009')
        self.assertRaises(ValidationError, new_obj.set_value_for, 'ATTR_DATE', '20a2----')
        self.assertRaises(ValidationError, new_obj.set_value_for, 'ATTR_DATE', '20/20/20')
        self.assertRaises(ValidationError, new_obj.set_value_for, 'ATTR_DATE', '20074400')
        # Valid dates
        new_obj.set_value_for('ATTR_DATE', '1945')
        new_obj.set_value_for('ATTR_DATE', 1945)
        new_obj.set_value_for('ATTR_DATE', '2004----')
        new_obj.set_value_for('ATTR_DATE', '202006--')
        new_obj.set_value_for('ATTR_DATE', '1931-12-05')
        new_obj.set_value_for('ATTR_DATE', '21/08/1974')
        new_obj.set_value_for('ATTR_DATE', '04.11.2005')
        new_obj.set_value_for('ATTR_DATE', '12/1988')
        new_obj.set_value_for('ATTR_DATE', '5.2005')
        # Try to set bad value through form
        attr_name = "ATTR_DATE"
        attr = models.Attribute.objects.get(name=attr_name)
        self.client.login(username='john', password='johnpw')
        response = self.client.post(
            reverse('add_value', args=[self.inventory.id, attr.id]),
            {'attribute': attr.id, 'value_%s' % attr_name: '2011.05.04'})
        self.assertContains(response, "The value '2011.05.04' is not valid for data type Date", status_code=403)

    def testAddVocabValue(self):
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        new_obj.set_value_for('ATTR_VOCAB', 'YeS')
        # Test setting vocab value is case insensitive (and then normalized)
        self.assertEqual(new_obj.get_value_for('ATTR_VOCAB', as_object=True).value_voc.value_code, 'yes')

    def testValidateIntegerValue(self):
        # Too high integer value
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        self.assertRaises(ValidationError, new_obj.set_value_for, 'ATTR_INT', 90000000000)

    def testAddQuerysetValue(self):
        attr = models.Attribute.objects.create(name="ATTR_QSET", datatype="qset",
            atitle= "Queryset attribute", queryset= "User.objects.all()")
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        new_obj.set_value_for('ATTR_QSET', self.user)
        self.assertEqual(new_obj.get_value_for('ATTR_QSET', as_object=True).get_value(), u'<a href="/users/john/">john</a>')

    def testAddBooleanValue(self):
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        new_attr = models.Attribute.objects.create(name="TESTBOOL", atitle="Test boolean", datatype='boolean')
        afo = models.AttributeForObject.objects.create(obj_type=self.spec_type, attr=new_attr, position=1)
        new_obj.set_value_for("TESTBOOL", 1)
        self.assertEqual(new_obj.get_value_for("TESTBOOL", as_object=True).get_value(), True)
        new_obj.set_value_for("TESTBOOL", 'on')
        self.assertEqual(new_obj.get_value_for("TESTBOOL", as_object=True).get_value(), True)

    def testNoDuplicatedValues(self):
        """ Test that an object cannot have two values for the same attribute (if attr.multivalued is False) """
        new_obj = self._create_object(self.spec_type, title="An object", status='private')
        new_obj.set_value_for('ATTR_DATE', '1945')
        attr = models.Attribute.objects.get(name="ATTR_DATE")
        newval = models.ObjectValueFactory(attr=attr, obj=new_obj)
        self.assertRaises(ValidationError, newval.set_value, {'value_ATTR_DATE': '1950'})

    def testMultivaluedValue(self):
        liste = self._create_object(self.list_type, title="My List")
        self._create_object(self.spec_type, title="An object1", parent=liste)
        self._create_object(self.spec_type, title="An object2", parent=liste)
        attr = models.Attribute.objects.create(name="ATTR_QSET", datatype="qset",
            atitle= "Queryset attribute", multivalued=True,
            queryset= "BaseObject.objects.filter(parent__pk=%d)" % liste.pk)

        self.assertEqual(attr.get_form_field().__class__.__name__, 'MultipleChoiceField')

    def testAddImage(self):
        self.client.login(username='john', password='johnpw')
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), "photo.png"), 'rb') as fh:
            response = self.client.post(
                reverse('add_photo', args=[self.inventory.id]),
                {'image': File(fh), 'title': 'abcd'}
            )
        # Save image redirect to object view url
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('object_detail', args=[self.inventory.id]))
        self.assertEqual(response.status_code, 200)

    def testAddEditFile(self):
        path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "photo.png")
        self.client.login(username='john', password='johnpw')
        with open(path, 'rb') as f:
            response = self.client.post('/%d/file/0/edit/' %  self.inventory.id,
                              {'fil': File(f), 'title': 'abcd'})
        # Save image redirect to object view url
        self.assertEqual(response.status_code, 302)
        response = self.client.get(reverse('object_detail', args=[self.inventory.id]))
        self.assertEqual(response.status_code, 200)
        # Edit title
        file_obj = self.inventory.files.first()
        response = self.client.post(
            '/%d/file/%d/edit/' % (self.inventory.pk, file_obj.pk),
            data={'title': 'dbca'}
        )
        self.assertRedirects(response, self.inventory.get_absolute_url())

    def test_delete_attribute(self):
        # Add the value
        attr = models.Attribute.objects.create(name="DONORNUMB", datatype="text", atitle="Num accession donateur")
        value = models.ObjectValueFactory(obj=self.inventory, attr=attr, value_text="Sample")
        value.save()
        value_id = value.id
        # Test to delete a value anonymously
        url = reverse('del_value', args=[self.inventory.id])
        response = self.client.post(url, {'id_to_delete': value_id})
        self.assertRedirects(response, reverse('login') + "?next=" + url)

        self.client.login(username='john', password='johnpw')
        response = self.client.post(url, {'id_to_delete': value_id})
        self.assertEqual(response.status_code, 200)
        self.assertRaises(models.ObjectValue.DoesNotExist, models.ObjectValue.objects.get, pk=value_id)

    def test_get_attribute_widget(self):
        attr = models.Attribute.objects.create(name="ORIGCTY", datatype="text", atitle="Pays d'origine")
        url = reverse('add_value', args=[self.inventory.id, attr.id])
        self.client.force_login(self.user)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, '<form id="descr_edit_form-0"')

    def test_show_inventories(self):
        # Create a private invent (a public one is created in setup)
        obj = self._create_object(self.list_type, title="A private list", status='private')
        # Test anonymous see only the public one
        response = self.client.get(reverse('inventories', args=[]))
        self.assertContains(response, self.inventory.title)
        self.assertNotContains(response, "A private list")
        # ...even if accessing directly the object page
        url = reverse('object_detail', args=[obj.id])
        response = self.client.get(url)
        self.assertContains(response, "vous n'êtes pas autorisé à voir cette page.", status_code=403)
        # Test owner see both
        self.client.force_login(self.user)
        response = self.client.get(reverse('inventories', args=[]))
        self.assertContains(response, self.inventory.title)
        self.assertContains(response, "A private list")

    def testShowPrivateAttributeValue(self):
        """ Test that private attribute values are only shown for users having write access """
        # Create object, set private on some attribute, add attribute value
        new_obj = self._create_object(self.spec_type, title="Object 1", parent=self.inventory)
        attr = models.Attribute.objects.get(name="ATTR_TEXT")
        attr.private = True
        attr.save()
        value = models.ObjectValueFactory(obj=new_obj, attr=attr, value_text="Sample name")
        value.save()
        # Test anonymous cannot see attribute value
        response = self.client.get(reverse('object_detail', args=[new_obj.id]))
        self.assertNotContains(response, "Sample name")
        # Test that owner can see it
        self.client.login(username='john', password='johnpw')
        response = self.client.get(reverse('object_detail', args=[new_obj.id]))
        self.assertContains(response, "Sample name")

    def test_insert_invent_accessions(self):
        """Test that accessions are inserted according to their accession number"""
        self._create_object(
            self.spec_type, title="Object 1", parent=self.inventory, status='private', values={'ACC_NUMERO': '645'}
        )
        self._create_object(
            self.spec_type, title="Object 2", parent=self.inventory, status='private', values={'ACC_NUMERO': '99'}
        )
        self._create_object(
            self.spec_type, title="Object 3", parent=self.inventory, status='private', values={'ACC_NUMERO': '900'}
        )
        self._create_object(
            self.spec_type, title="Object 4", parent=self.inventory, status='private', values={'ACC_NUMERO': '712'}
        )
        self.assertEqual(
            [obj.title for obj in self.inventory.get_children()],
            ['Object 2', 'Object 1', 'Object 4', 'Object 3']
        )

    def test_create_object(self):
        self.client.force_login(self.user)
        url = reverse('object_create', args=[self.inventory.pk, 'accession'])
        response = self.client.get(url)
        self.assertContains(
            response,
            '<input type="text" name="title" maxlength="100" required="" id="id_title">',
            html=True
        )
        response = self.client.post(
            reverse('object_create', args=[self.inventory.pk, 'accession']),
            data={
                'title': "Test 1",
                'status': 'public',
            }
        )
        new_obj = models.BaseObject.objects.get(title='Test 1')
        self.assertRedirects(response, reverse('object_detail', args=[new_obj.pk]))
        self.assertEqual(new_obj.parent, self.inventory)
        self.assertEqual(new_obj.obj_type.code, 'accession')

    @test_in_english
    def test_edit_object(self):
        """ Test object_edit form """
        new_obj = self._create_object(self.spec_type, title="Object 1", parent=self.inventory, status='private')
        attr = models.Attribute.objects.get(name="ATTR_VOCAB")
        # Add link between attribute and object, and check if attribute widget is in object_edit
        afo = models.AttributeForObject.objects.create(obj_type=self.spec_type, attr=attr, position=1)
        edit_url = reverse('object_edit', args=[new_obj.id])

        # Anonymous cannot access edit form
        response = self.client.get(edit_url)
        self.assertEqual(response.status_code, 302)
        self.client.login(username='john', password='johnpw')
        response = self.client.get(edit_url)
        self.assertContains(response, attr.atitle)

        # Test posting wrong value
        response = self.client.post(edit_url, {
            'obj_type': str(new_obj.obj_type.pk),
            'title': "Object modified",
            'description': "Test object",
            'ATTR_VOCAB': u"7654",
        })
        self.assertContains(response, "Select a valid choice. 7654 is not one of the available choices.")

        afo = models.AttributeForObject.objects.create(obj_type=self.spec_type, attr=models.Attribute.objects.get(name="ATTR_DATE"), position=1)
        response = self.client.post(edit_url, {
            'obj_type': str(new_obj.obj_type.pk),
            'title': "Object modified",
            'description': "Test object",
            'ATTR_DATE': "2011.05.04"
        })
        self.assertContains(response, "The value &#x27;2011.05.04&#x27; is not valid for data type Date")

        # Test modify values
        response = self.client.post(edit_url, {
            'obj_type': str(new_obj.obj_type.pk),
            'title': "Object modified",
            'description': "Test object",
            'ATTR_VOCAB': "no",
            'REMARKS_TAXO': "ssp. ooxymos",
        })
        self.assertRedirects(response, reverse('object_detail', args=[new_obj.id]))
        new_obj = models.BaseObject.objects.get(pk=new_obj.id) # Refresh from database
        self.assertEqual(new_obj.title, "Object modified")
        self.assertEqual(new_obj.get_value_for('ATTR_VOCAB'), "non")

    def test_edit_object_status(self):
        new_obj1 = self._create_object(self.spec_type, title="Object 2", parent=self.inventory, status='private')
        new_obj2 = self._create_object(self.spec_type, title="Object 1", parent=self.inventory, status='private')
        # Test anonymous denied
        post_data = {
            'status': 'public', 'owner': self.inventory.owner.id,
            'new_perm_group':'', 'new_perm_perm':'read', 'apply_to_children': True
        }
        response = self.client.post(
            reverse('object_edit_perms', args=[self.inventory.id]), post_data)
        self.assertRedirects(response, reverse('login') + "?next=" + reverse('object_edit_perms', args=[self.inventory.id]))
        self.assertEqual(models.BaseObject.objects.get(pk=new_obj1.id).status, 'private')
        # Test changing status
        self.client.login(username='john', password='johnpw')
        response = self.client.post(
            reverse('object_edit_perms', args=[self.inventory.id]), post_data)
        self.assertRedirects(response, self.inventory.get_absolute_url())
        self.assertEqual(models.BaseObject.objects.get(pk=new_obj1.id).status, 'public')
        self.assertEqual(models.BaseObject.objects.get(pk=new_obj2.id).status, 'public')

    def test_edit_object_permissions(self):
        from permission.models import Permission
        # Test changing owner and permissions
        new_obj1 = self._create_object(self.spec_type, title="Object 2", parent=self.inventory, status='private')
        new_obj2 = self._create_object(self.spec_type, title="Object 1", parent=self.inventory, status='private')
        user2 = User.objects.create_user('john2', 'doe2@example.org', 'john2pw')
        grp1  = Group.objects.create(name='grp1')
        grp2  = Group.objects.create(name='grp2')
        Permission.objects.create(content_object=self.inventory, group=grp1, perm='rw')
        Permission.objects.create(content_object=new_obj2, group=grp2, perm='rw')

        self.client.login(username='john', password='johnpw')
        response = self.client.post(
            reverse('object_edit_perms', args=[self.inventory.id]),
            {'status': 'public', 'owner': user2.id,
             'new_perm_group':grp2.id, 'new_perm_perm':'read', 'apply_to_children': True})
        new_obj1 = models.BaseObject.objects.get(pk=new_obj1.id)
        new_obj2 = models.BaseObject.objects.get(pk=new_obj2.id)
        self.assertEqual(new_obj1.owner, user2)
        self.assertEqual(new_obj1.permissions.all().count(), 2)
        self.assertEqual(new_obj2.permissions.all().count(), 2)
        self.assertEqual(new_obj1.permissions.all()[0].group, grp1)

    def test_delete_object(self):
        """ Test object_edit form """
        new_obj = self._create_object(self.spec_type, title="Object 1", parent=self.inventory, status='private')
        self.client.force_login(self.user)
        response = self.client.post(reverse('object_delete', args=[new_obj.pk]))
        self.assertRedirects(response, reverse('object_detail', args=[self.inventory.pk]))
        self.assertEqual(models.BaseObject.objects.filter(pk=new_obj.pk).count(), 0)

    def testGetHTMLTableValues(self):
        """ Test 'export' a list as an HTML table, (external data access) """
        new_list = self._create_object(self.list_type, title="A list")
        grp = models.AttributeGroup.objects.create(gname="Origin", category="a0")
        models.AttributeInGroup.objects.create(
            attribute=models.Attribute.objects.get(name='ATTR_TEXT'),
            group=grp, position=0)
        models.AttributeInGroup.objects.create(
            attribute=models.Attribute.objects.get(name='ATTR_DATE'),
            group=grp, position=1)
        new_obj1 = self._create_object(self.spec_type, title="Contained 1", parent=new_list)
        new_obj1.set_value_for('ATTR_DATE', '2009-07-23')
        new_obj2 = self._create_object(self.spec_type, title="Contained 2", parent=new_list)
        new_obj2.set_value_for('ATTR_TEXT', 'kg')
        # Set taxonomy link
        response = self.client.get(reverse('object_by_uuid', args=[new_list.uuid, 'html']))
        # Table caption
        self.assertContains(response, '<caption>%s</caption' % new_list.title)
        # Table headers
        self.assertContains(response, '<th>ATTR_DATE</th>')
        self.assertContains(response, '<th>ATTR_TEXT</th>')
        # Table content
        self.assertContains(response, '<td>2009-07-23</td>')
        self.assertContains(response, '<td>kg</td>')

    def test_add_lot_command(self):
        acc = self._create_object(self.spec_type, title="Acc 1", parent=self.inventory)
        lot = models.Lot.objects.create(accession=acc, weight=234.5)
        self.client.force_login(self.user)
        response = self.client.post(reverse("command_create", args=[lot.pk]), data={
            "designation": "Command by client",
            "date_cmd": "2025-01-01",
            "weight": "44.5",
        })
        self.assertRedirects(response, acc.get_absolute_url())
        lot.refresh_from_db()
        self.assertEqual(lot.weight, 190)
        cmd = lot.command_set.first()
        response = self.client.post(reverse("command_edit", args=[cmd.pk]), data={
            "designation": "Command by client",
            "date_cmd": "2025-01-01",
            "weight": "42.5",
        })
        lot.refresh_from_db()
        self.assertEqual(lot.weight, 192)


class ValueValidationTestCase(BaseTestCase):
    def testDateValidation(self):
        val = models.ObjectValueDate.objects.create(
            obj=models.BaseObject.objects.all()[0],
            attr=models.Attribute.objects.get(name='ATTR_DATE'),
            value_text='')
        self.assertTrue(val.validate("20.3.2001"))
