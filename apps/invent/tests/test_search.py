from django.contrib.auth.models import User, Group
from django.urls import reverse

from invent import models
from invent import forms
from invent.tests.base import BaseTestCase, test_in_english
from invent.views import openxml_contenttype


class SearchTests(BaseTestCase):

    @test_in_english
    def test_display_search(self):
        attr1 = models.Attribute.objects.create(name="ACC_ESPECE_LAT", datatype="text", atitle="Latin name")
        # At least one value must exist for an attribute to be displayed in search.
        self._create_object(
            self.spec_type,
            title="An object", values={'ACC_ESPECE_LAT': 'Phyto'}
        )
        response = self.client.get(reverse('search'))
        self.assertContains(
            response,
            '<select class="attr_select" id="id_choice_0" name="choice_0">\n<option value="" selected="selected">Choose an attribute</option>\n<option value="T_ACC_ESPECE_LAT">Latin name</option>\n</select>',
            html=True
        )

    def test_simple_search(self):
        self._create_object(
            self.spec_type,
            title="Blé d'automne", values={'ATTR_TEXT': 'Phyto'}
        )
        response = self.client.get(reverse('search') + '?fulltext=blé')
        self.assertContains(response, "Blé d'automne")

    def test_add_criterion(self):
        """ Test ajax call to dynamically add criterion to search form """
        self._create_object(
            self.spec_type,
            title="An object", values={'ATTR_INT': 5}
        )
        url = reverse('add_criterion', args=[1])
        response = self.client.get(url)
        self.assertContains(response, '<option value="N_ATTR_INT">Test integer attribute</option>', html=True)
        self.assertContains(response, '<select name="oper_1" id="id_oper_1"></select>', html=True)
        self.assertContains(response, '<input type="text" name="value_1" id="id_value_1" />', html=True)

    def test_getting_vocab(self):
        """ Test ajax call to retrieve vocab for an attribute """
        response = self.client.get(reverse('vocab_for_attribute'), data={'attr_name': 'ATTR_VOCAB'})
        self.assertContains(response, "oui")

    def test_criteria_search(self):
        new_obj1 = self._create_object(
            self.spec_type,
            title="An object",
            values={'ATTR_TEXT': 'An accession', 'ATTR_VOCAB': 'yes', 'ATTR_INT': '2005'}
        )
        new_obj2 = self._create_object(
            self.spec_type,
            title="Another object",
            values={'ATTR_TEXT': 'Second accession', 'ATTR_VOCAB': 'no'}
        )
        # Search a numeric value
        f = forms.SearchForm(
            data={'choice_0':'N_ATTR_INT', 'oper_0':'gte', 'value_0': '2005'}
        )
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['id'], new_obj1.id)
        # Search with two criteria (text/vocabulary)
        vocval = models.VocabValue.objects.get(vocab__name='YesNo', value_code='no')
        f = forms.SearchForm(data={
            'object_type': '0', 'choice_0':'T_ATTR_TEXT', 'oper_0':'icontains', 'value_0': 'Accession',
            'choice_1':'V_ATTR_VOCAB', 'oper_1':'exact', 'vvalue_1': str(vocval.id)})
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['id'], new_obj2.id)
        # It may happen that the 'oper' field is not populated, must raise ValidationError
        f = forms.SearchForm(
            data={'object_type': '0', 'choice_0':'T_ACCENAME', 'value_0': '2005'})
        self.assertFalse(f.is_valid())
        # Search with non-consecutive field numbers
        f = forms.SearchForm(data={
            'object_type': '0', 'choice_2':'T_ATTR_TEXT', 'oper_2':'icontains', 'value_2': 'Accession',
            'choice_4':'V_ATTR_VOCAB', 'oper_4':'exact', 'vvalue_4': str(vocval.id)})
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)

    def test_qset_attribute_search(self):
        """ Test search of subattributes (attribute that is a 'qset' attribute of another object) """
        models.AttributeForObject.objects.create(
            obj_type=self.list_type,
            attr=models.Attribute.objects.get(datatype='qset')
        )
        new_obj1 = self._create_object(
            self.spec_type,
            title="A specimen", values={'ATTR_TEXT': 'A specimen', 'ATTR_INT': 245}
        )
        new_obj2 = self._create_object(
            self.list_type,
            title="An inventory", values={'ATTR_TEXT': 'My inventory', 'ATTR_QUERYSET': new_obj1.pk}
        )

        f = forms.SearchForm(data={
            'choice_0': 'V_ATTR_QUERYSET', 'oper_0': 'lt', 'value_0': 400,
        })
        self.assertTrue(f.is_valid(), f.errors)
        res = f.search()[0]
        self.assertEqual(len(res), 1)

    def test_search_visibility(self):
        """ Test that private/protected objects don't appear in search results """
        # Create public/private/protected objects
        new_obj1 = self._create_object(self.spec_type, title="Object public",
            status='public', values={'ATTR_TEXT': 'Object public'})
        new_obj2 = self._create_object(self.spec_type, title="Object private",
            status='private', values={'ATTR_TEXT': 'Object private'})
        new_obj3 = self._create_object(self.spec_type, title="Object protected",
            status='private', values={'ATTR_TEXT': 'Object protected'})
        user2 = User.objects.create_user('john2', 'doe2@example.org', 'john2pw')
        group = Group(name='mygroup')
        group.save()
        user2.groups.add(group)
        new_obj3.add_read_permission(group)
        search_data = {'object_type': '0', 'choice_0':'T_ATTR_TEXT', 'oper_0':'icontains', 'value_0': 'Object'}
        # Public search should show 1 (public)
        f = forms.SearchForm(data=search_data)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 1)
        self.assertEqual(res[0]['id'], new_obj1.id)
        # owner search should show 3 (public/private/protected)
        f = forms.SearchForm(data=search_data, user=self.user)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 3)
        # other user search should show 2 (public/protected)
        f = forms.SearchForm(data=search_data, user=user2)
        self.assertTrue(f.is_valid())
        res = f.search()
        self.assertEqual(len(res), 2)
        self.assertTrue(res[0]['id'] in (new_obj1.id, new_obj3.id))
        self.assertTrue(res[1]['id'] in (new_obj1.id, new_obj3.id))

    def test_search_export(self):
        attr_int = models.Attribute.objects.get(name='ATTR_INT')
        attr_int.multivalued = True
        attr_int.save()
        obj1 = self._create_object(
            self.spec_type,
            title="An object",
            values={'ATTR_TEXT': 'An accession', 'ATTR_VOCAB': 'yes', 'ATTR_INT': 2005}
        )
        newval = models.ObjectValueFactory(obj=obj1, attr=attr_int)
        newval.set_value({'value_int': 2010})
        self._create_object(
            self.spec_type,
            title="Another object",
            values={'ATTR_TEXT': 'Second accession', 'ATTR_VOCAB': 'no'}
        )
        response = self.client.post(reverse('search-export'), data={
            'fulltext': 'accession',
            'all_items': 'on',
            'group_attr': 'on',
            'accession': ['ATTR_TEST', 'ATTR_VOCAB', 'ATTR_INT'],
            'selectall-lot': 'on',
            'selectall-test': 'on',
        })
        self.assertEqual(response['Content-Type'], openxml_contenttype)
