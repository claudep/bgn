import json
import re
import time
from collections import OrderedDict
from datetime import date
from operator import attrgetter
from openpyxl import Workbook

from django.conf import settings

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from django.core.exceptions import MultipleObjectsReturned, PermissionDenied, ValidationError
from django.core.paginator import Paginator, InvalidPage, EmptyPage

from django.db.models import Count, Model, Q
from django.dispatch import Signal
from django.http import (
    HttpResponse, HttpResponseRedirect, HttpResponseForbidden, Http404, JsonResponse,
)
from django.shortcuts import render, get_object_or_404
from django.template import Template, Context, RequestContext, loader
from django.urls import reverse
from django.utils.translation import gettext as _, ngettext

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_POST
from django.views.generic import DeleteView, DetailView, TemplateView, UpdateView, View

from haystack.views import SearchView as HaystackSearchView
from haystack.query import SearchQuerySet

from invent.models import (
    Attribute, AttributeForObject, AttributeGroup, AttributeInGroup,
    BaseObject, Command, FileAttachment, GermTest, Lot, MetaValue, ObjectType,
    ObjectValue, ObjectValueFactory, Vocabulary, VocabValue
)
from invent import forms
from imports.models import Importer

from permission.forms import PermissionEditForm

from invent.paginator import BGNPaginator

HOME_TUPLE = (_("Home"), "/")

openxml_contenttype = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'


class IndexView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        top_levels = BaseObject.objects.filter(obj_type__code='inventory', level=0, status='public')
        return {
            'top_levels': top_levels.order_by('title'),
            'latest': top_levels.order_by('-weight', '-modified')[:6],
        }

def prepare_object_page(type_code, page):
    if type_code == 'accession':
            # Prefetch has images
            obj__pks_with_imgs = BaseObject.objects.filter(
                obj_type__code='image', parent__pk__in=[obj.pk for obj in page.object_list]
            ).order_by().values_list('parent__pk', flat=True).distinct()
            for obj in page.object_list:
                obj._has_images = obj.pk in obj__pks_with_imgs


class ObjectDetailView(DetailView):
    model = BaseObject
    template_name = 'invent/object_detail.html'

    def get_object(self):
        obj = get_object_or_404(BaseObject, pk=self.kwargs['obj_id'])
        if not obj.can_view(self.request.user):
            raise PermissionDenied()
        return obj

    def get_context_data(self, **kwargs):
        obj = self.object
        editable = obj.can_edit(self.request.user)
        batch_size = self.request.COOKIES.get('BGNPageSize') or 30

        num_acc = obj.get_value_for('ACC_NUMERO')
        if (num_acc == None):
            num_acc = ""

        context = {
            **super().get_context_data(**kwargs),
            'breadcrumb': obj.breadcrumb(),
            'obj': obj,
            'grouped_values': obj.get_grouped_values(self.request.user, with_taxo=False),
            'editable': editable,
            'attr_groups': editable and obj.obj_type.get_groups(categ_exclude=['taxo'], with_not_grouped=True),
            'containable_types': obj.obj_type.can_contain.exclude(code='image'),
            'images': obj.get_images(),
            'lots': obj.lot_set.all().prefetch_related('germtest_set') if (
                obj.obj_type.code == 'accession' and self.request.user.is_authenticated
                ) else [],
            'grouped_children': {},
            'display_options': ('30', '100', 'All'),
            'list_inventory': get_list_inventory(),
            'num_acc': num_acc,
            'name_acc': obj.get_value_for('ACC_NOM'),
        }

        if obj.is_containable():
            children_by_type = obj.get_children_by_type()
            if children_by_type and editable:
                context['uuid_url'] = obj.uuid_url(self.request)
            for obj_type, content in children_by_type.items():
                if obj_type.code == 'image':
                    continue # Images as Objects are special-cased
                elif obj_type.code == 'lot':  # Old version of lots, shortly removed
                    continue

                paginator = BGNPaginator(content['children'], batch_size, orphans=5)
                no_page = int(self.request.GET.get('p%d' % obj_type.pk, '1'))

                try:
                    page = paginator.page(no_page)
                except (EmptyPage, InvalidPage):
                    page = paginator.page(paginator.num_pages)
                context.update(paginator.get_context(self.request))

                prepare_object_page(obj_type.code, page)
                context['grouped_children'][obj_type] = {
                    'count': content['count'], 'top_level': content['top_level'], 'object_list': page,
                }
                context['allow_tabular'] = tabular_view_allowed(self.request.user)
                context['rendered_objects'] = paginator.current_page.object_list
            context["accession_total_count"] = obj.get_descendants().filter(obj_type__code="accession").count()

        return context


def object_image(request, obj_id):
    """ View of an image object, limited to image and attributes (without other context)
        generally AJAX-called to fill main content area (div id='content_blocks') """
    image = get_object_or_404(BaseObject, pk=obj_id)
    if not image.can_view(request.user):
        return permission_denied(request)
    editable = image.can_edit(request.user)
    context = {
        'image': image,
        'grouped_values': image.get_grouped_values(request.user),
        'editable': editable,
        'img_attrs': editable and image.obj_type.get_attributes(),
    }
    return render(request, 'invent/object_detail_image.html', context)

def permission_denied(request):
    context = {}
    return render(request, 'invent/denied.html', context)

'''def object_list(request, obj_id):
    """ List the content of an object """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    context = {
        'obj': obj,
    }
    return render(request, 'invent/object_list.html', context)
'''

def object_list_table(request, obj_id, otype, format='html'):
    # Not optimal to call by_uuid, TODO: factorize code elsewhere...
    list_obj = get_object_or_404(BaseObject, pk=obj_id)

    return object_list_by_uuid(request, list_obj.uuid, format=(format=='table' and 'html' or format), otype=otype)

def object_list_by_uuid(request, uuid, format='html', otype='all'):
    """ With format == 'html', returns a basic HTML-formatted table suitable for import in spreadsheets """
    list_obj = get_object_or_404(BaseObject, uuid=uuid)
    all_attrs = list_obj.get_children_existing_attrs()
    children = list_obj.get_children().exclude(obj_type__code='image')
    if otype == "all":
        obj_type_num = children.values_list('obj_type').distinct().count()
        if obj_type_num > 1:
            return http_403(request, "Unable to choose which children list to render")
        otype_obj = children[0].obj_type
    else:
        # Sort attributes depending on objecttype attributes
        otype_obj = get_object_or_404(ObjectType, code=otype)

    sorted_attrs = otype_obj.get_attributes()

    def get_index(attr):
        try:
            return sorted_attrs.index(attr)
        except ValueError:
            return 1000

    all_attrs = sorted(all_attrs, key=get_index, reverse=True)
    all_values = [] # List of value dicts
    empty_value = ''
    for obj in children.order_by('title'):
        val_dict, remaining_dict = obj.get_attr_dict(all_attrs, empty_value)
        # OQE-specific, export mesures numbers
        if hasattr(obj, 'mesureparcelle_set'):
            val_dict['MESURES'] = ",".join(
                [mp.mesure.numero for mp in obj.mesureparcelle_set.all().select_related('mesure')]
            )
        if remaining_dict:
            # There are attributes not in all_attrs: complete all_attrs and all_values already constructed
            # Due to their special treatment, taxo values may be in remaining_dict, try to
            # order them properly
            taxo_attrs = OrderedDict(AttributeInGroup.objects.filter(group__category='taxo').values_list('attribute__name', 'position').order_by('position'))
            remaining_dict = OrderedDict([(key[1], key[2]) for key in sorted([(taxo_attrs.get(k,0), k, val) for k, val in remaining_dict.items()], reverse=True)])
            remaining_keys = remaining_dict.keys()
            # Extend all_attrs, then re-parse all_values to add missing keys
            all_attrs.extend([Attribute.objects.get(name=name) for name in remaining_keys])
            for val in all_values:
                for k in remaining_keys:
                    val[k] = empty_value
            val_dict.update(remaining_dict)
        all_values.append(val_dict)

    # Reverse all_attrs and dict in all_values, so 'automatic' attrs are first
    all_attrs.reverse()
    all_values = [
        OrderedDict([(key, val[key]) for key in reversed(val.keys())])
        for val in all_values
    ]

    context = {
        'obj'        : list_obj,
        'headers'    : [a.name for a in all_attrs],
        'sdict_list' : all_values,
        'num_children': len(children),  # query was already resolved.
    }
    if is_ajax(request):
        template = 'invent/object_list_table.html'
    else:
        template = 'invent/object_list_table_container.html'
        context['caption'] = list_obj.get_title()
    return render(request, template, context)


class PaginatedObjectsView(TemplateView):
    """ Called by pagination arrows in object list block """
    template_name = 'invent/object_list_block.html'
    named_url     = 'object_list'
    display_style = 'short'
    
    def get_queryset(self):
        self.list_obj = get_object_or_404(BaseObject, pk=int(self.kwargs['obj_id']))
        if self.kwargs.get('otype') in ('all', None):
            children = self.list_obj.get_children().all().order_by('title')
        else:
            try:
                children = list(self.list_obj.get_children_by_type(type_code=self.kwargs['otype']).values())[0]['children']
            except IndexError:
                children = self.list_obj.get_children().none()
        return children

    def get_context_data(self, **kwargs):
        batch_size = self.request.COOKIES.get('BGNPageSize') or 30
        paginator = BGNPaginator(self.object_list, batch_size, orphans=5)
        no_page = int(self.request.GET.get('p', '1'))
        
        context = {
            'obj': self.list_obj,
            'num_children': paginator.count,
            'type_code': self.kwargs.get('otype'),
            'more_url': reverse(self.named_url, kwargs=self.kwargs),
            'style': self.display_style,
            'display_options': ( '30', '100', 'All'),
        }
        try:
            context['page'] = paginator.page(no_page)
            #~ context.update(paginator.get_context(self.request))
            #~ context['rendered_objects'] = paginator.current_page.object_list
            
        except (EmptyPage, InvalidPage):
            context['page'] = paginator.page(paginator.num_pages)
        prepare_object_page(self.kwargs.get('otype'), context['page'])
            
        return context

    def get(self, request, *args, **kwargs):
        self.object_list = self.get_queryset()
        context = self.get_context_data(**kwargs)
        return self.render_to_response(context)


def invent_list(request):
    """ List all viewable inventories of first level on the site """
    if request.user.is_authenticated:
        if request.user.is_superuser:
            invs = BaseObject.objects.filter(obj_type__code='inventory', level=0)
        else:
            invs = BaseObject.objects.filter(obj_type__code='inventory', level=0).filter(Q(status='public') | (Q(owner=request.user)))
    else:
        invs = BaseObject.objects.filter(obj_type__code='inventory', level=0, status='public')
    invs = invs.order_by('title')
     
    try:
        page = int(request.GET.get('page', '1'))
    except ValueError:
        page = 1
        
    paginator = Paginator(invs, 12)
        
    context = {
        'invents': paginator.page(page), 
        'breadcrumb': [HOME_TUPLE, (_("Inventory list"), "")],
        }
        
    return render(request, 'invent/invent_list.html', context)


class ObjectEditView(LoginRequiredMixin, UpdateView):
    model = BaseObject
    template_name = 'invent/object_edit.html'
    form_class = forms.ObjectEditForm
    pk_url_kwarg = 'obj_id'

    def get_object(self):
        obj = super().get_object()
        self.obj_type = obj.obj_type
        return obj

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs.update({'user': self.request.user, 'obj_type': self.obj_type})
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'obj': self.object,
            'breadcrumb': self.object and self.object.breadcrumb() or '',
        })
        return context

    def form_valid(self, form):
        if not form.instance.owner_id:
            form.instance.owner = self.request.user
        if 'parent_id' in self.kwargs and int(self.kwargs['parent_id']) != 0:
            # In case of creation
            parent = BaseObject.objects.get(pk=self.kwargs['parent_id'])
            form.instance.parent = parent
        form.save()

        if form.instance.obj_type.code == 'image':
            redirect_url = "%s#%d" % (form.instance.parent.get_absolute_url(), form.instance.id)
        else:
            redirect_url = form.instance.get_absolute_url()
        return HttpResponseRedirect(redirect_url)


class ObjectCreateView(ObjectEditView):
    def dispatch(self, *args, **kwargs):
        self.obj_type = get_object_or_404(ObjectType, code=self.kwargs['obj_code'])
        return super().dispatch(*args, **kwargs)

    def get_object(self):
        return None

    def get_initial(self):
        initial = super().get_initial()
        # if getting the form for a new object of type 'lot' with a parent of type 'lot' => sous-lot
        if self.obj_type.code == 'lot' and int(self.kwargs['parent_id']) != 0:
            parent = BaseObject.objects.get(pk=self.kwargs['parent_id'])
            if parent.obj_type.code == 'lot':
                initial.update({
                    'LOT_DATE_CREATION': date.today().isoformat(),
                    'LOT_TAUX_GERM_INITIAL': parent.get_value_for('LOT_TAUX_GERM_DERNIER') or parent.get_value_for('LOT_TAUX_GERM_INITIAL'),
                    'LOT_TAUX_GERM_DERNIER': parent.get_value_for('LOT_TAUX_GERM_DERNIER'),
                    'LOT_DATE_TEST_DERNIER': parent.get_value_for('LOT_DATE_TEST_DERNIER'),
                    'LOT_DATE_LIMITE_MULTIPLICATION': parent.get_value_for('LOT_DATE_LIMITE_MULTIPLICATION'),
                    'LOT_DATE_LIMITE_TEST_GERMINATION': parent.get_value_for('LOT_DATE_LIMITE_TEST_GERMINATION'),
                    'LOT_EMPLACEMENT': parent.get_value_for('LOT_EMPLACEMENT'),
                    'LOT_INSTCODE': parent.get_value_for('LOT_INSTCODE'),
                    'LOT_BLOC': parent.get_value_for('LOT_BLOC'),
                    'LOT_RANGEE': parent.get_value_for('LOT_RANGEE'),
                    'LOT_ETAGERE': parent.get_value_for('LOT_ETAGERE'),
                    'LOT_PARENT': parent.get_title(),
                    'LOT_CODE_1': parent.get_value_for('LOT_CODE_1'),
                    'LOT_CODE_2': parent.get_value_for('LOT_CODE_2'),
                    'LOT_CODE_3': parent.get_value_for('LOT_CODE_3'),
                })
        initial['obj_type'] = self.obj_type.pk
        return initial

    def form_valid(self, form):
        form.instance.obj_type = self.obj_type
        response = super().form_valid(form)
        if self.obj_type.code == 'lot' and int(self.kwargs['parent_id']) != 0:
            parent = BaseObject.objects.get(pk=self.kwargs['parent_id'])
            if parent.obj_type.code == 'lot':
                parent.set_value_for(
                    'LOT_POIDS',
                    (parent.get_value_for('LOT_POIDS') or 0) - (form.instance.get_value_for('LOT_POIDS') or 0)
                )
                while parent:
                    if parent.obj_type.code == 'accession':
                        form.instance.move_to(parent, position='last-child')
                        form.instance.save()
                        break
                    parent = parent.parent
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'breadcrumb': [HOME_TUPLE, (_("Create object"), "")],
        })
        return context


@login_required
def object_edit_perms(request, obj_id):
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    edit_form = PermissionEditForm(obj, request.POST or None)
    if request.method == 'POST':
        if edit_form.is_valid():
            edit_form.save()
            return HttpResponseRedirect(edit_form.instance.get_absolute_url())
    context = {
        'obj': obj,
        'breadcrumb': obj.breadcrumb(),
        'form'  : edit_form,
    }
    return render(request, 'invent/object_edit_perms.html', context)


@login_required
@require_POST
def object_delete(request, obj_id):
    """ Delete an object """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to delete this object"))
    parent = obj.parent
    title = obj.title
    obj.delete()
    messages.success(request, _("The object '%s' has been deleted.") % title)
    if parent:
        return HttpResponseRedirect(reverse('object_detail', args=[parent.pk]))
    else:
        return HttpResponseRedirect(reverse('home'))


@login_required
def object_autocomplete(request):
    term = request.GET.get('term', '')
    object_list = BaseObject.objects.filter(obj_type__code__in=['inventory', 'accession'], title__icontains=term).order_by('-obj_type__code')
    return JsonResponse({
        'results': [
            {'id': str(obj.pk), 'text': str(obj)}
            for obj in object_list[:100]
        ],
        'pagination': {'more': False},
    })


class ObjectExportLots(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        inventory = get_object_or_404(BaseObject, pk=kwargs['obj_id'])
        book = Workbook()
        sheet = book.active
        fields = [f.name for f in Lot._meta.get_fields() if not f.one_to_many]
        fields.insert(2, "ACC_NUMERO")
        fields.insert(3, "ACC_NOM")
        sheet.append([f.upper() for f in fields])
        lots = Lot.objects.filter(
            accession__in=inventory.get_descendants().filter(obj_type__code="accession")
        ).select_related("accession", "instcode")
        for lot in lots:
            values = [self.serialize(lot, field_name) for field_name in fields]
            sheet.append(values)

        response = HttpResponse(content_type=openxml_contenttype)
        dateToday = date.today().strftime('%Y_%m_%d')
        response['Content-Disposition'] = f'attachment; filename="invent_{inventory.pk}_lots_{dateToday}.xlsx"'

        book.save(response)
        return response

    def serialize(self, lot, field_name):
        if field_name == 'instcode':
            return lot.instcode.code if lot.instcode_id else ''
        if field_name == 'accession':
            return lot.accession_id
        if field_name.startswith("ACC_"):
            return lot.accession.get_value_for(field_name)
        if hasattr(lot, f'get_{field_name}_display'):
            return getattr(lot, f'get_{field_name}_display')()
        value = getattr(lot, field_name)
        if isinstance(value, Model):
            return value.pk
        return value


@login_required
def object_import(request, obj_id):
    """ Import an xls file containing data for sub-objects creation """
    obj = get_object_or_404(BaseObject, pk=obj_id)
    if not obj.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    import_form = forms.ImportForm(request.POST or None, request.FILES or None)
    
    msg = ""
    if request.method == 'POST':
        if import_form.is_valid():
            try:
                msg = Importer().import_file(request.FILES['xlsfile'],
                                  request.user, obj, imp_type=import_form.cleaned_data['imp_type'])
            except Exception as err:
                if settings.DEBUG:
                    raise
                msg = err
            import_form = None
    context = {
        'parent': obj,
        'form'  : import_form,
        'message' : msg
    }
    return render(request, 'invent/object_import.html', context)

# providing_args=["form", "obj", "attr", "value"]
value_edit_post_init = Signal()
value_edit_post_save = Signal()


# Views for attribute editing

@login_required
def add_value(request, object_id, attr_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    attr = get_object_or_404(Attribute, pk=attr_id)
    form = forms.get_value_form(objet, attr, None, request.POST or None)
    value_edit_post_init.send(sender = request, form = form, obj = objet, attr = attr, value = None)
    if request.method == 'POST':
        if not objet.can_edit(request.user):
            return HttpResponseForbidden(_("You are not allowed to edit this object"))
        if form.is_valid():
            newval = MetaValue(attr, objet)
            try:
                newval.set_value(form.cleaned_data)
                value_edit_post_save.send(sender = request, form = form, obj = objet, attr = attr, value = None)
            except ValidationError as e:
                return HttpResponseForbidden(_(u"Error: %s") % u", ".join(e.messages))
            return HttpResponse("<div>%s</div>" % newval.render(context=Context({})))
        else:
            return HttpResponseForbidden(form_errors_to_string(form.errors))
    # Get value widget form
    context = {'value_form': form.as_p(), 'value_id': 0 }
    return render(request, 'invent/attribute_edit.html', context)


@login_required
def edit_value(request, object_id, value_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    try:
        value_id = int(value_id)
        values = [get_object_or_404(ObjectValue, pk=value_id).get_proxied()]
        attr = values[0].attr
    except ValueError:
        # value_id is an attribute name
        attr = get_object_or_404(Attribute, name=value_id)
        values =list(ObjectValue.objects.filter(obj=objet, attr=attr))
    if attr.multivalued:
        value_obj = MetaValue(attr, objet, values)
    else:
        value_obj = values[0]
    form = forms.get_value_form(objet, value_obj.attr, value_obj, request.POST or None, with_label=False)
    value_edit_post_init.send(sender = request, form = form, obj = objet, attr = attr, value = value_obj)
    if request.method == 'POST':
        if not objet.can_edit(request.user):
            return HttpResponseForbidden(_("You are not allowed to edit this object"))
        if form.is_valid():
            value_obj.set_value(form.cleaned_data)
            value_edit_post_save.send(sender = request, form = form, obj = objet, attr = attr, value = value_obj)
            return HttpResponse("%s" % value_obj.render(context=Context({})))
        else:
            return HttpResponseForbidden(form_errors_to_string(form.errors))
    context = {'value_form': form.as_p(), 'value_id': value_obj.id}
    return render(request, 'invent/attribute_edit.html', context)


@login_required
@require_POST
def del_element(request, object_id, DelModel):
    """ Delete an attribute value from an object """
    objet = get_object_or_404(BaseObject, pk=object_id)

    if not objet.can_edit(request.user):
        return HttpResponseForbidden(_("You are not allowed to edit this object"))
    try:
        pk = int(request.POST['id_to_delete'])
        value = get_object_or_404(DelModel, pk=pk)
    except ValueError:
        attr = get_object_or_404(Attribute, name=request.POST['id_to_delete'])
        value = MetaValue(attr, objet, list(ObjectValue.objects.filter(obj=objet, attr=attr)))
    value.delete()
    return HttpResponse("")


@login_required
def del_value(request, object_id):
    return del_element(request, object_id, ObjectValue)


@login_required
def add_photo(request, object_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    if request.method == 'POST':
        photo_form = forms.AddPhotoForm(request.POST, request.FILES)
        if photo_form.is_valid():
            num_images = photo_form.save(parent=objet)
            messages.success(
                request,
                ngettext("%d new image has been added.", "%d new images have been added.", num_images) % num_images
            )
            return HttpResponseRedirect(objet.get_absolute_url())
    else:
        photo_form = forms.AddPhotoForm()
    context = {
        'object': objet,
        'form': photo_form,
    }
    return render(request, 'invent/photo_edit.html', context)


@login_required
def edit_file(request, object_id, file_id):
    objet = get_object_or_404(BaseObject, pk=object_id)
    file_id = int(file_id)
    file_obj = None
    
    if file_id > 0:
        file_obj = FileAttachment.objects.get(pk=file_id)
        
    if request.method == 'POST':
        file_form = forms.FileForm(request.POST, request.FILES, instance=file_obj)
        
        if file_form.is_valid():                
            file_form.save(objet)
            return HttpResponseRedirect(objet.get_absolute_url())
    else:
        file_form = forms.FileForm(instance=file_obj)
    context = {
        'object': objet,
        'photo': file_obj,
        'form': file_form,
    }
    return render(request, 'invent/file_edit.html', context)


@login_required
def del_file(request, object_id):
    return del_element(request, object_id, FileAttachment)


class LotEditView(LoginRequiredMixin, UpdateView):
    model = Lot
    form_class = forms.LotEditForm
    template_name = 'invent/generic_edit.html'
    create = False

    def get_object(self):
        return None if self.create else super().get_object()

    def form_valid(self, form):
        if self.create:
            form.instance.accession = get_object_or_404(BaseObject, pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.accession.get_absolute_url()


class LotDeleteView(LoginRequiredMixin, DeleteView):
    model = Lot

    def get_success_url(self):
        return self.object.accession.get_absolute_url()


class GermTestEditView(LoginRequiredMixin, UpdateView):
    model = GermTest
    form_class = forms.GermTestEditForm
    template_name = 'invent/generic_edit.html'
    create = False

    def get_object(self):
        return None if self.create else super().get_object()

    def form_valid(self, form):
        if self.create:
            form.instance.lot = get_object_or_404(Lot, pk=self.kwargs['pk'])
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.lot.accession.get_absolute_url()


class GermTestDeleteView(LoginRequiredMixin, DeleteView):
    model = GermTest

    def get_success_url(self):
        return self.object.lot.accession.get_absolute_url()


class CommandEditView(LoginRequiredMixin, UpdateView):
    model = Command
    form_class = forms.CommandEditForm
    template_name = 'invent/generic_edit.html'
    create = False

    def get_object(self):
        return None if self.create else super().get_object()

    def form_valid(self, form):
        if self.create:
            form.instance.lot = get_object_or_404(Lot, pk=self.kwargs["lot_pk"])
        return super().form_valid(form)

    def get_success_url(self):
        return self.object.lot.accession.get_absolute_url()


class CommandDeleteView(LoginRequiredMixin, DeleteView):
    model = Command

    def get_success_url(self):
        return self.object.lot.accession.get_absolute_url()


def attributes_in_group(request, group_id):
    """ Negative group_id indicates that this is other attributes for an objet type """
    group_id = int(group_id)
    if group_id < 0:
        obj_type = ObjectType.objects.get(pk = group_id * -1)
        attrs = AttributeForObject.get_others_grouped(obj_type).get_attributes()
    else:
        attrs = Attribute.objects.filter(attributeingroup__group__pk=group_id, active=True)
    attributes = [{'id':a.id, 'name':a.name, 'title':a.atitle, 'dyn':bool(a.calculator)} for a in attrs]
    return HttpResponse(json.dumps(attributes), content_type='application/json')


def attributes(request):
    """ Show all attributes grouped on a page """
    context = {
        'breadcrumb': [HOME_TUPLE, (_("Attribute List"), "")],
        'attr_groups': AttributeGroup.objects.all().order_by('position'),
        'attrs_no_group': Attribute.objects.annotate(grp=Count('attributegroup')).filter(active=True, grp=0).order_by('name'),
    }
    return render(request, 'invent/attributes.html', context)


def vocab_for_attribute(request):
    """ Returns all vocabulary values for an attribute vocabulary, in JSON format for Ajax use """
    attr_name, subattr_name, prefix = forms.SearchForm.decode_attr_name(request.GET.get('attr_name'))
    if subattr_name:
        attr_name = subattr_name
    if not re.fullmatch(r"[-a-zA-Z0-9_]+", attr_name):
        raise Http404("Bad attribute name")
    try:
        attr = Attribute.objects.select_related('vocab').get(name=attr_name)
    except Attribute.DoesNotExist:
        raise Http404
    voc_values = [{'id':v['id'],'name':v['name']} for v in attr.get_vocab()]
    return HttpResponse(json.dumps(voc_values), content_type='application/json')


def search_export(request):
    selected_all = request.POST.get('all_items')
    if selected_all:
        search_form = forms.SearchForm(request.POST, request.user)
        if search_form.is_valid():
            results = search_form.search()
        listItems = [key['id'] for key in results]
    else:
        listItems = request.POST.getlist('item')
    
    attrs_base = ['OBJ_TYPE', 'ACC_NUMERO', 'ACC_NOM' ]
    attrs_opt = [name for name in request.POST.getlist('accession') if name not in attrs_base]
    export_lots = request.POST.get('selectall-lot') == 'on'
    lot_fields = Lot.attribute_fields() if export_lots else []
    export_tests = request.POST.get('selectall-test') == 'on'
    test_fields = GermTest.attribute_fields() if export_tests else []

    if not listItems:
        messages.error(request, _("You did not select any items or descriptions to export."))
        return HttpResponseRedirect(request.headers['Referer'])

    # create
    book = Workbook()
    sheet = book.active
        
    headers = (
        attrs_base +
        attrs_opt +
        [f.header_key for f in lot_fields if f.header_key != 'LOT_NUMERO'] +
        [f.header_key for f in test_fields]
    )
    sheet.append(headers)
    acc_numero_idx = headers.index('ACC_NUMERO')

    for accession in BaseObject.objects.filter(
        pk__in=listItems, obj_type__code='accession'
    ).prefetch_related('lot_set', 'lot_set__germtest_set'):
        values = ['accession']
        accession._fill_cache()
        for attr_name in attrs_base[1:] + attrs_opt:
            value = accession.get_value_for(attr_name)
            if isinstance(value, list):
                value = " | ".join([str(v) for v in value])
            values.append(value)
        sheet.append(values)

        if not request.user.is_authenticated:
            continue

        for lot in accession.lot_set.all():
            if lot_fields:
                values = ['lot'] + [''] * (len(attrs_base) - 1 + len(attrs_opt))
                for field, value in lot.attributes(empty=True):
                    if field.header_key == 'LOT_NUMERO':
                        values[acc_numero_idx] = value
                    else:
                        values.append(value.code if value and field.name == 'instcode' else value)
                sheet.append(values)

            if test_fields:
                for test in lot.germtest_set.all():
                    values = ['test'] + [''] * (len(attrs_base) - 1 + len(attrs_opt) + len(lot_fields))
                    for name, vname, value in test.attributes(empty=True):
                        values.append(value)
                    sheet.append(values)

    response = HttpResponse(content_type=openxml_contenttype)
    dateToday = time.strftime('%Y_%m_%d', time.localtime())
    response['Content-Disposition'] = 'attachment; filename="exp_data_bgn_%s.xlsx"' % dateToday

    book.save(response)
    return response
        

def replaceItemsAcc(listeValues):
    # Replace result search items by accession ids
    newItems = []
    deleteItemsList = []
    
    #~ data.get_value_for('ACC_NUMERO')
    for value in listeValues:
        data = BaseObject.objects.get(pk=value)
        objTypeId = data.obj_type.id
        
        while (objTypeId > 3):
            parentData = data.parent
            objTypeId = parentData.obj_type.id
            
            if (objTypeId == 3):
                # Replace item id  by parent id
                idParent = str(parentData.get_id())
                deleteItemsList.append(value)
                newItems.append(idParent)
    
    for item in deleteItemsList:
        listeValues.remove(item)

    listeValues.extend(newItems)
    return listeValues
    

@login_required
def accession_duplicate(request, object_id):
    obj_parent = get_object_or_404(BaseObject, pk=object_id)

    if not obj_parent.can_view(request.user):
        return permission_denied(request)

    # Collect the data for new accession.
    newaccnum = request.POST['num_new_acc']
    newaccnom = request.POST['nom_new_acc']
    # Verify if the accession number already exists.
    existings = ObjectValue.objects.filter(attr__name='ACC_NUMERO', value_text=newaccnum)
    if len(existings) > 0:
        return HttpResponseForbidden("The accession number %s exists in database." % newaccnum)

    # Create the new accession.
    new_acc = BaseObject.objects.create(
        obj_type=obj_parent.obj_type, title=" ".join([newaccnum, newaccnom]),
        owner=request.user, status=obj_parent.status, parent=obj_parent
    )
    new_acc.set_value_for('ACC_NUMERO', newaccnum)
    new_acc.set_value_for('ACC_NOM', newaccnom)

    # Create all attributes for the new accession after the attributes parent.
    for value in obj_parent.get_values():
        if value.attr.name in ('ACC_NUMERO', 'ACC_NOM') or not value.attr.inheritable:
            continue
        value.obj = new_acc
        value.pk = None
        value.save()

    # Create attribute ACC_ACTIF if it doesn't exist in new accession.
    attr_actif = Attribute.objects.get(name='ACC_ACTIF')
    value_voc_yes = VocabValue.objects.get(vocab__name='YesNo', value_code='yes')
    new_obj_value = ObjectValue.objects.get_or_create(
        obj=new_acc, attr=attr_actif, defaults={'value_voc' : value_voc_yes}
    )
    messages.success(request, "L’accession a bien été dupliquée.")

    return JsonResponse({'result': 'OK', 'new_url': new_acc.get_absolute_url()})


def get_list_inventory():
    """ Show all inventory """
    type_obj = ObjectType.objects.get(code='inventory')
    list_inventory = BaseObject.objects.filter(obj_type=type_obj).order_by('title')

    return list_inventory


class SearchView(TemplateView):
    template_name = 'search/search.html'
    output = 'list'
    form_class = forms.SearchForm

    def get(self, request, *args, **kwargs):
        search_form = self.form_class(request.GET or None, request.user)
        results = None
        context = {
            'breadcrumb': [HOME_TUPLE, (_("Search"), "")],
            'form': search_form,
            'qs': request.META['QUERY_STRING'], # TODO remove existing p (.replace('&p=',''),
            'total': -1,  # Mark for 'no search done'
        }
        if request.GET and search_form.is_valid():
            results = search_form.search()
            paginator = Paginator(results, 40)
            no_page = int(request.GET.get('p', '1'))
            
            try:
                page = paginator.page(no_page)
            except (EmptyPage, InvalidPage):
                page = paginator.page(paginator.num_pages)

            # Transform object ids in real objects (only for current page)
            page.object_list = BaseObject.objects.filter(
                pk__in=[o.get('obj_id', o.get('id')) for o in page.object_list]
            )
            # Create a list with all accession attributes
            acc_attrs = ObjectType.objects.get(code='accession').get_attributes()
            acc_attrs = sorted(acc_attrs, key=attrgetter('atitle'))

            context.update({
                'page': page,
                'total': paginator.count,
                'attributs': acc_attrs,
            })

        return self.render_to_response(context)


class ImageSearchView(HaystackSearchView):
    """ Images are indexed with haystack """

    def build_form(self, *args, **kwargs):
        self.searchqueryset = SearchQuerySet().filter(otype='image')
        if not self.request.user.is_authenticated:
            self.searchqueryset = self.searchqueryset.filter(public__exact=True)
        return super().build_form(*args, **kwargs)

    def extra_context(self):
        extra = super().extra_context()
        extra['title']      = _("Search Images")
        extra['breadcrumb'] = [HOME_TUPLE, (_("Search Images"), "")]
        extra['is_search']  = hasattr(self.form, "cleaned_data") and 'q' in self.form.cleaned_data
        return extra


def add_criterion(request, base_num):
    """ AJAX request to add a new criterion line in the search form """
    search_form = forms.SearchForm(
        data={}, user=request.user, base_num=int(base_num))
    t = Template("""<div class="search_crit"><div class="minus_icon" id="minus_icon_%d"><a href='#' title='%s' onclick='return del_criterion($(this));'>
                    <img src='%simg/minus.png' alt='Remove criterion'/></a></div>
                    {{ crit_group.0 }}  {{ crit_group.1 }}  {{ crit_group.2 }} {{ crit_group.3 }}</div>""" % (int(base_num), _("Remove criterion"), settings.STATIC_URL))
    return HttpResponse(t.render(Context({'crit_group': list(search_form.criterion())[0]})))

# Utility functions
def form_errors_to_string(error_list):
    err_string = ", ".join(["%s: %s" % (k, "/".join([str(e) for e in err])) for k, err in error_list.items()])
    return err_string

def http_403(request, reason):
    return HttpResponseForbidden(
        loader.render_to_string('403.html', context={'reason': reason},  request=request)
    )

def tabular_view_allowed(user):
    if settings.ALLOW_TABULAR_VIEW == 'all':
        return True
    elif settings.ALLOW_TABULAR_VIEW == 'auth':
        return user.is_authenticated
    # finally: only admin
    elif user.is_anonymous:
        return False
    else:
        return user.is_staff

def is_ajax(request):
    return request.META.get('HTTP_X_REQUESTED_WITH') == 'XMLHttpRequest'
