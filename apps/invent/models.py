import datetime
import operator
import os 
import re 
import types 
from collections import OrderedDict
from functools import reduce

from django import forms
from django.conf import settings
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.staticfiles import finders
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django.core.serializers import serialize
from django.db import models
from django.db.models import Q
from django.db.models.query import QuerySet
from django.db.models.signals import post_save
from django.dispatch import Signal
from django.template import Template
from django.urls import reverse
from django.utils.functional import cached_property
from django.utils.safestring import mark_safe, SafeData
from django.utils.translation import gettext, gettext_lazy as _

from sorl.thumbnail import ImageField
from mptt.managers import TreeManager
from mptt.models import MPTTModel

from invent.fields import UuidField
from invent.utils import truncate
from permission.models import Permission, PermissionMixin


class DateValidator:
    code = 'invalid'
    valid_eurisco = r'^(?P<year>[12]\d\d\d)(----|(?P<month>[01]\d)(--|(?P<day>[0-3]\d)))?$' # 2004 or 19820529 or 200612-- or 2008----
    valid_iso     = r'^(?P<year>[12]\d\d\d)-(?P<month>[01]\d)-(?P<day>[0-3]\d)$' # 2008-03-12
    valid_ch_fr   = r'^((?P<day>[0-3]?\d)[\./])?(?P<month>[01]?\d)[\./](?P<year>[12]\d\d\d)$' # 21/12/2006 ou 04.07.2005

    def __call__(self, value):
        if isinstance(value, datetime.datetime):
            return value.date()
        if isinstance(value, datetime.date):
            return value
        if not isinstance(value, str):
            value = str(value)
        m = re.match(self.valid_eurisco, value) or re.match(self.valid_iso, value) or re.match(self.valid_ch_fr, value)

        if m is not None:
            year = m.group('year')
            month = m.group('month')
            day = m.group('day')

            year = int(year) if not year is None else datetime.date.today().year
            month = int(month) if not month is None else 1
            day = int(day) if not day is None else 1

            try:
                return datetime.date(year, month, day)
            except ValueError:
                pass

        raise ValidationError("The value '%s' is not valid for data type Date" % (value,), code=self.code)

date_validator = DateValidator()


class ObjectType(models.Model):
    """ Initial object types are filled by fixtures/initial_data.json """
    code        = models.SlugField(max_length=20)
    tname       = models.CharField(max_length=50, unique=True)
    icon        = models.ImageField(upload_to="icons", blank=True)
    searchable  = models.BooleanField(default=False,
        help_text="Available in 'Search in' select on search page")
    can_contain  = models.ManyToManyField('self', symmetrical=False, blank=True)

    class Meta:
        db_table = "object_type"

    def __str__(self):
        return self.tname

    @classmethod
    def as_choices(cls, only_searchable=False):
        """ Return a list of tuples of all object types suitable for a choice form widget """
        if only_searchable:
            return [(o.id, o.tname) for o in cls.objects.filter(searchable=True)]
        else:
            return [(o.id, o.tname) for o in cls.objects.all()]

    @property
    def possible_attributes(self):
        """ Return an attribute structure in the form (OrderedDict):
            {'group_name': [attributes,], ...}
        """
        if not hasattr(self, '_possible_attributes'):
            groups = [afo.attr_group for afo in self.attributeforobject_set.exclude(attr_group__isnull=True).select_related('attr_group')]
            self._possible_attributes =OrderedDict(zip([g.gname for g in groups], [[] for i in range(len(groups))]))
            for aig in AttributeInGroup.objects.filter(group__in=groups).order_by('position'):
                self._possible_attributes[aig.group.gname].append(aig.attribute)
            # Add non-grouped attributes
            for afo in self.attributeforobject_set.filter(attr_group__isnull=True).order_by('position'):
                self._possible_attributes.setdefault(gettext("Other Attributes"), []).append(afo.attr)
        return self._possible_attributes

    def get_groups(self, categ_exclude=[], with_not_grouped=False):
        """ Return a list of all groups attached to this object type
            If with_not_grouped is True, a final fake group is added with all attributes for this ObjectType not in an attribute group
        """
        groups = [afo.attr_group for afo in self.attributeforobject_set.select_related('attr_group').all().order_by('position') if afo.attr_group and afo.attr_group.category not in categ_exclude]
        if with_not_grouped:
            groups.append(AttributeForObject.get_others_grouped(self))
        return groups

    def get_attributes(self):
        """ Returns a list of attributes valid for this object type """
        attr_list = []
        for afo in self.attributeforobject_set.all().order_by('position'):
            attr_list.extend(afo.get_attributes())
        return attr_list


class BaseObjectManager(TreeManager):
    def filter_writable_for_user(self, user):
        if user.is_superuser:
            return self.get_queryset()
        else:
            group_pks = [g.pk for g in user.groups.all()]
            return self.get_queryset().filter(
                Q(permissions__group__pk__in=group_pks, permissions__perm='rw') |
                Q(status="private", owner__pk=user.pk)
            )

STATUS_CHOICES = (
    ('private', _("Private")),
    ('public', _("Public")),
)

class BaseObject(MPTTModel, PermissionMixin):
    obj_type = models.ForeignKey(ObjectType, on_delete=models.CASCADE)
    uuid        = UuidField(unique=True, auto=True)
    title       = models.CharField(max_length=100, verbose_name=_("Title"))
    description = models.TextField(blank=True, null=True, verbose_name=_("Description"))
    parent = models.ForeignKey('self', null=True, blank=True, related_name='children', on_delete=models.CASCADE)
    owner = models.ForeignKey(User, on_delete=models.PROTECT)
    status      = models.CharField(max_length=10, choices=STATUS_CHOICES, default='private')
    created     = models.DateTimeField(auto_now_add=True)
    modified    = models.DateTimeField(auto_now=True)
    weight      = models.SmallIntegerField(
        default=0, help_text="Higher numbers will make the item appear first in some lists")
    files       = GenericRelation('FileAttachment')
    permissions = GenericRelation(Permission)
    
    objects = tree = BaseObjectManager()

    class Meta:
        db_table = "base_object"
        get_latest_by = "modified"

    def __str__(self):
        return self.title or str(self.pk)

    def __len__(self):
        return len(self.children.all())

    def __bool__(self):
        return True

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._cached_values = None

    def get_absolute_url(self):
        return reverse('object_detail', args=[str(self.id)])
    
    # Functions inherited from mptt:
    #get_ancestors, get_children, get_descendants...
    #https://django-mptt.readthedocs.io/

    def _fill_cache(self):
        if self._cached_values is None:
            values = self.get_values()
            self._cached_values = {}
            for val in values:
                if val.attr.name in self._cached_values:
                    if isinstance(self._cached_values[val.attr.name], MetaValue):
                        self._cached_values[val.attr.name].append_value(val)
                    else:
                        meta = MetaValue(val.attr, val.obj, [self._cached_values[val.attr.name], val])
                        self._cached_values[val.attr.name] = meta
                else:
                    self._cached_values[val.attr.name] = val

    @classmethod
    def get_by_attr_value(cls, attr_name, value, limit_to=None):
        attr = Attribute.objects.get(name=attr_name)
        query_dict = {
            'attr__name': attr_name,
            attr.get_value_field(): value,
        }
        if limit_to is not None:
            query_dict['obj__obj_type__code'] = limit_to
        vals = ObjectValue.objects.filter(**query_dict).values_list('obj_id', flat=True).distinct()
        return cls.objects.filter(pk__in=vals)

    def can_edit(self, user):
        """ Return True if user can edit this object """
        return self.has_write_permission(user)

    def can_view(self, user):
        return self.has_read_permission(user)

    def is_containable(self):
        return bool(self.obj_type.can_contain.exclude(code='image').count())

    def is_task_enabled(self):
        return 'task' in settings.INSTALLED_APPS

    def breadcrumb(self, link_self=False):
        """ Produce a tuple list structure to give to template to build the breadcrumb """
        bc_list = [(_("Home"), "/")]
        bc_list.extend([(str(o), o.get_absolute_url()) for o in self.get_ancestors()])
        bc_list.append((self.get_title(), link_self and self.get_absolute_url() or ""))
        return bc_list

    def get_title(self):
        return self.title or str(self.pk)

    def set_title(self, atitle):
        if atitle is not None:
            self.title = atitle
            self.save()
            
    def get_id(self):
        return self.pk

    def get_features(self):
        """ Test if object has images and return a list of images as tuples (image url, title)
            Displayed currently in object_list_block (on object_detail) and search results """
        images = []
        has_images = self._has_images if hasattr(self, '_has_images') else self.get_images().count() > 0
        if has_images:
            images.append((os.path.join(settings.STATIC_URL, 'icons', 'image.png'), gettext("This object has images")))
        return images

    def get_images(self):
        return self.get_children().filter(obj_type__code='image')

    def get_children_by_type(self, type_code=None):
        """ Return a dict of tuples : (count, lists of children), grouped by type """
        grouped_objects = {}
        # get_children returns an empty query self is a leaf node! (and annotate cannot be used then)
        if not self.is_leaf_node():
            obj_types = self.get_children().values('obj_type', 'obj_type__code').annotate(ocount=models.Count('obj_type')).order_by()
            if type_code is not None:
                obj_types = obj_types.filter(obj_type__code=type_code)
            for ot in obj_types:
                # For accessions, we display the whole subtree of accessions.
                if ot['obj_type__code'] == 'accession':
                    children = self.get_accession_descendants()
                    count = children.count()
                else:
                    children = self.get_children().order_by('title')
                    count = ot['ocount']
                top_level = children.filter(parent=self).count()
                children = children.filter(obj_type__pk=ot['obj_type'])
                grouped_objects[ObjectType.objects.get(id=ot['obj_type'])] = {
                    'count': count, 'top_level': top_level, 'children': children,
                }
        return grouped_objects

    def get_accession_descendants(self):
        qs = self.get_descendants().filter(obj_type__code='accession')
        # Exclude accessions from non-accessions children
        filters = []
        for n in self.get_children().exclude(obj_type__code='accession'):
           filters.append(Q(tree_id=n.tree_id, lft__gt=n.lft, rght__lt=n.rght))
        if filters:
            q = reduce(operator.or_, filters)
            qs = qs.exclude(q)
        return qs

    def uuid_url(self, request):
        return request.build_absolute_uri(reverse('object_by_uuid', args=[self.uuid]))

    def get_icon_url(self):
        """ Return an URL suitable for a src HTML attribute, return default one if no icon property """
        if self.obj_type.icon:
            return self.obj_type.icon.url
        return os.path.join(settings.STATIC_URL, 'icons','default.png')

    def get_attr_dict(self, attr_list, empty_value=''):
        """ Return all values of an object in a OrderedDict with each key corresponding in attr_list.name:
            {'DONORDESCR': 'Description', 'DONORNUMB': 135, ...}
        """
        attr_dict = OrderedDict()
        self._fill_cache()
        remaining_values = self._cached_values.copy()
        for attr in attr_list:
            attr_dict[attr.name] = self.get_value_for(attr.name) or empty_value
            if attr.name in remaining_values:
                del remaining_values[attr.name]
        for k, v in remaining_values.items():
            remaining_values[k] = v.get_value() or empty_value
        return attr_dict, remaining_values

    def get_children_existing_attrs(self):
        """ Return all existing attributes for all children, except 'automatic' ones (like taxo, etc.) """
        return list(Attribute.objects.filter(objectvalue__obj__parent=self).distinct()) #.order_by('name'))

    def get_values(self):
        """ Return a list of values for this object (as ObjectValue objects) """
        if not self.pk:
            return []
        values = ObjectValue.objects.filter(obj=self).select_related('obj','attr').as_list()
        if self.obj_type.code == 'accession':
            values.extend([val for val in [
                ObjectValue(obj=self, attr=attr) for attr in Attribute.objects.filter(name__in=[
                    'ACC_MIN_CONSERV_FRIGO', 'ACC_MIN_CONSERV_CONGEL', 'ACC_MIN_CONSERV_JBGENEVE',
                    'ACC_MIN_CONSERV_SVALBARD'
                ])
            ] if val.get_value() is not None])
        return values

    def get_value_for(self, attr_name, as_object=False):
        """ Return the value corresponding to attr_name for this object, either as string or object """
        if self._cached_values is None:
            try:
                val_obj = ObjectValue.objects.get(obj=self, attr__name=attr_name)
            except ObjectValue.DoesNotExist:
                val_obj = None
        else:
            val_obj = self._cached_values.get(attr_name, None)
        if as_object:
            return val_obj
        else:
            return val_obj and val_obj.get_value() or None

    def set_value_for(self, attr_name, value):
        """ Set an attribute value for this object """
        if value is None or value == '':
            # Do not set empty values
            return
        attr = Attribute.objects.get(name=attr_name)
        val_obj, cr = ObjectValue.objects.get_or_create(obj=self, attr=attr)
        val_obj.set_value({'value_%s' % attr_name: value})
        # Accessions trees are ordered by accession number
        if self.obj_type.code == 'accession' and attr_name == 'ACC_NUMERO' and self.parent:
            self.move_to_sorted_position('ACC_NUMERO', value)

    def move_to_sorted_position(self, order_by, value):
        acc_numbs_first_level = list(
            ObjectValue.objects.filter(
                obj__in=self.parent.get_children(), attr__name=order_by
            ).values_list('value_text', 'obj_id')
        )
        try:
            acc_numbs = [(float(txt), pk) for txt, pk in acc_numbs_first_level]
            self_tuple = (float(value), self.pk)
        except ValueError:
            acc_numbs = acc_numbs_first_level
            self_tuple = (value, self.pk)
        acc_numbs.sort()
        right_idx = acc_numbs.index(self_tuple) + 1
        try:
            right_sibling = BaseObject.objects.get(pk=acc_numbs[right_idx][1])
        except IndexError:
            pass
        else:
            # move self wrt accession number
            self.move_to(right_sibling, position="left")

    def get_grouped_values(self, user, with_taxo=True):
        """ Return all values grouped by attribute group:
            {'group_name1': {'attr_title1': (attr1_obj, [Value1]), 'attr_title2': (attr2_obj, [Value2, Value3]), ...},
             'group_name2': {...}, ...}
            If a group has a 'renderer' key, its value is used as a template to render the group values
        """
        self._fill_cache()

        val_groups = OrderedDict(zip(self.obj_type.possible_attributes.keys(),
            [OrderedDict() for i in range(len(self.obj_type.possible_attributes.keys()))]))
        can_view_private = self.has_permission(('r+', 'rw'), user)
        for grp, attrs in self.obj_type.possible_attributes.items():
            for attr in attrs:
                if attr.name in self._cached_values:
                    val = self._cached_values[attr.name]
                    if val.attr.private and not can_view_private:
                        continue

                    #val_groups.setdefault(grp, OrderedDict())
                    if attr.atitle not in val_groups[grp]:
                        val_groups[grp][attr.atitle] = (attr, [])
                    val_groups[grp][attr.atitle][1].append(val)
        for key in list(val_groups.keys()):
            if not val_groups[key]:
                del val_groups[key]
        if len(val_groups) == 1 and list(val_groups.keys())[0] == gettext("Other Attributes"):
            val_groups[gettext("Attributes")] = val_groups[gettext("Other Attributes")]
            del val_groups[gettext("Other Attributes")]
        return val_groups

    def serialize(self):
        """
        Typically used to get back a deleted object from a backup, to be able to
        `loaddata` the content in the production database.
        """
        lots = []
        for lot in self.lot_set.all():
            lots.append(lot)
        tests = []
        for tst in GermTest.objects.filter(lot__accession=self):
            tests.append(tst)
        return serialize(
            'json',
            [self] + list(self.files.all()) + list(self.permissions.all()) +
            list(self.objectvalue_set.all()) + lots + tests
        )

    @cached_property
    def _get_culture(self):
        culture = self.get_ancestors()[0].title
        try:
            sous_culture = self.get_ancestors()[1].title
        except IndexError:
            sous_culture = None
        if culture == "Blackbox":
            culture == "Légumes"
        if culture in [
            "Céréales", "Légumes", "Plantes aromatiques et médicinales",
            "Plantes industrielles", "Plantes fourragères", "Maïs"
        ]:
            return [culture, sous_culture]
        return [None, None]

    def get_nb_min_grains(self):
        # remplace ACC_MIN_CONSERV_CONGEL, ACC_MIN_CONSERV_FRIGO, ACC_MIN_CONSERV_JBGENEVE, ACC_MIN_CONSERV_SVALBARD
        if self.obj_type.code != 'accession':
            raise TypeError("Unable to get nb_min_grains for non-accession objects")
        culture, sous_culture = self._get_culture
        if not culture:
            return {'frigo': None, 'congelateur': None, 'jbg': None, 'svalbard': None}
        if culture == 'Céréales':
            return {'frigo': '250 g', 'congelateur': '180 g', 'jbg': None, 'svalbard': '100 g'}
        if (
            (culture == "Légumes" and (not sous_culture or (
                not 'haricot' in sous_culture.lower() and not 'pois' in sous_culture.lower()
            ))) or
            (culture == "Plantes industrielles"  and sous_culture not in ["Fèves", "Soja"])
        ):
            return {'frigo': 4000, 'congelateur': 6000, 'jbg': None, 'svalbard': 2000}
        if (
            culture == "Légumes" or culture == "Plantes industrielles" or culture == "Maïs"
        ):
            return {'frigo': 2000, 'congelateur': 4000, 'jbg': 1000, 'svalbard': 1500}
        if culture == "Plantes aromatiques et médicinales":
            return {'frigo': None, 'congelateur': 8000, 'jbg': None, 'svalbard': 1000}
        return {}

    def get_acc_min_conserv_congel(self):
        return self.get_nb_min_grains().get('congelateur')

    def get_acc_min_conserv_frigo(self):
        return self.get_nb_min_grains().get('frigo')

    def get_acc_min_conserv_jbg(self):
        return self.get_nb_min_grains().get('jbg')

    def get_acc_min_conserv_svalbard(self):
        return self.get_nb_min_grains().get('svalbard')


class ImageObject(BaseObject):
    class Meta:
        proxy = True

class ImageSubobject(models.Model):
    obj   = models.OneToOneField(BaseObject, on_delete=models.CASCADE)
    image = ImageField(upload_to='photos')
    class Meta:
        db_table = "image_subobject"

    def __str__(self):
        return "Image %s" % self.image.url


# By default, copy permissions of the parent object at creation time
def copy_permissions(sender, instance, created, **kwargs):
    if created and instance.parent:
        for perm in instance.parent.permissions.all():
            p = Permission(content_object=instance, group=perm.group, perm=perm.perm)
            p.save()
post_save.connect(copy_permissions, sender=BaseObject)


class AttributeForObject(models.Model):
    obj_type = models.ForeignKey('ObjectType', on_delete=models.CASCADE)
    attr = models.ForeignKey('Attribute', blank=True, null=True, on_delete=models.CASCADE)
    attr_group = models.ForeignKey('AttributeGroup', blank=True, null=True, on_delete=models.CASCADE)
    # May be used to specify mandatory or optional attributes
    status      = models.CharField(max_length=15, blank=True, null=True)
    position    = models.IntegerField(default=0)
    searchable_in_child = models.BooleanField(default=False)

    class Meta:
        db_table = "attribute_for_object"
        ordering = ['obj_type', 'position']

    def __str__(self):
        if self.attr_id:
            return u"Attribute %s for object type '%s'" % (self.attr.name, self.obj_type.code)
        else:
            return u"Attribute group %s for object type '%s'" % (self.attr_group.gname, self.obj_type.code)

    @classmethod
    def get_others_grouped(cls, obj_type):
        """ Return a fake attribute group with all not grouped attributes """
        class FakeGroup:
            id       = obj_type.id * -1
            gname    = _("Other attributes")
            category = 'other'
            position = 999
            def get_attributes(self):
                """ Return a list of attributes """
                return Attribute.objects.filter(attributeforobject__obj_type=obj_type, active=True).order_by('attributeforobject__position')
        return FakeGroup()

    def get_attributes(self):
        """ Return a list of attributes contained in object """
        attr_list = []
        if self.attr:
            attr_list.append(self.attr)
        elif self.attr_group:
            attr_list.extend(self.attr_group.get_attributes())
        return attr_list


class DatatypeChoices:
    """ search_tp: T: text, N:number, D: date, V: vocab """
    choices = {
        'text'   : {'name': _(u"Text"),    'db_vfield': 'value_text',
                    'ffield_class': forms.CharField, 'ffield_opts': {'max_length': 2000},
                    'search_tp': 'T'},
        'integer': {'name': _(u"Integer"), 'db_vfield': 'value_int',
                    'ffield_class': forms.IntegerField,
                    # MySQL limits on INT
                    'ffield_opts': {'max_value': 2147483647, 'min_value': -2147483647},
                    'search_tp': 'N'},
        'float'  : {'name': _(u"Float"),   'db_vfield': 'value_float',
                    'ffield_class': forms.FloatField, 'search_tp': 'N'},
        'boolean': {'name': _(u"Boolean"), 'db_vfield': 'value_int',
                    'ffield_class': forms.BooleanField, 'search_tp': 'B'},
        'date'   : {'name': _(u"Date"),    'db_vfield': 'value_text',
                    'ffield_class': forms.CharField,
                    'ffield_opts': {'max_length': 10, 'validators': [date_validator]},
                    'search_tp': 'D'},
        'year': {'name': _("Year"), 'db_vfield': 'value_int',
                 'ffield_class': forms.IntegerField,
                 'ffield_opts': {'max_value': 2200, 'min_value': 1900},
                 'search_tp': 'N',
                },
        'speclist': {'name': _(u"List of species"), 'db_vfield': 'value_text',
                     'ffield_class': forms.IntegerField},
        'qset'   : {'name': _(u"Queryset"), 'db_vfield': 'value_int', 'ffield_class': None,
                    'search_tp': 'V'},
    }

    @classmethod
    def as_choices(cls):
        return [(key, ch['name']) for key, ch in cls.choices.items()]

    @classmethod
    def as_searchtp_dict(cls):
        dct = {'vocab': 'V'}
        [dct.setdefault(key, vals['search_tp']) for key, vals in cls.choices.items() if 'search_tp' in vals]
        return dct

dt_choices = DatatypeChoices()

class AttributeManager(models.Manager):
    def used_attributes(self):
        used_ids = Attribute.objects.values_list('id', flat=True).annotate(num_values=models.Count('objectvalue')).filter(num_values__gt=0)
        return self.filter(id__in=list(used_ids))


class Attribute(models.Model):
    name       = models.SlugField(max_length=35, unique=True)
    active     = models.BooleanField(default=True, db_index=True)
    private    = models.BooleanField(default=False,
                                     help_text=u"Only visible by users having write access to the target object")
    atitle     = models.CharField(max_length=50)
    code       = models.CharField(max_length=35, blank=True, null=True)
    ahelp      = models.TextField(blank=True)
    datatype   = models.CharField(max_length=35, choices=dt_choices.as_choices())
    vocab = models.ForeignKey('Vocabulary', blank=True, null=True, on_delete=models.PROTECT)
    # Define if attribute appears in Search form
    searchable = models.BooleanField(default=True)
    inheritable = models.BooleanField(default=True)
    multivalued = models.BooleanField(default=False)
    calculator = models.CharField(max_length=35, blank=True, null=True)
    queryset = models.CharField(max_length=100, blank=True, null=True)
    render_template = models.TextField(blank=True, null=True)
    objects = AttributeManager()

    class Meta:
        db_table = "attribute"
        ordering = ('name',)

    def __str__(self):
        return self.name

    def save(self, **kwargs):
        # Put this in model validation as soon as using django 1.2
        need_val_migration = False
        if self.vocab and self.get_value_count() > 0:
            # Check old values are valid in the new vocabulary
            all_values = ObjectValue.objects.filter(attr=self).select_related('value_voc')
            for val in all_values:
                if val.value_voc.vocab != self.vocab:
                    # Validate current value with new vocab
                    self.vocab.valid_value(val.get_value())
                    need_val_migration = True
        super().save(**kwargs)
        if need_val_migration:
            # Values need to be re-called from DB as old val.attr.vocab seems to be cached
            all_values = ObjectValue.objects.filter(attr=self).select_related('value_voc')
            for val in all_values.as_list():
                val.set_value({'value_%s' % self.name: val.get_value()})

    @classmethod
    def all_attr_names(cls):
        all_names = list(cls.objects.filter(active=True).values_list('name', flat=True))
        # Maybe this should be a real 'special' descriptor ?
        all_names.append('DESCRIPTION')
        return all_names

    def get_datatype(self):
        return self.vocab_id and 'vocab' or self.datatype

    def is_editable(self):
        return not self.calculator

    def get_vocab(self):
        """ Returns the vocabulary as a dictionary list """
        value_list = []
        if self.queryset:
            qs = eval(self.queryset)
            value_list = [{'id': v.id, 'code': None, 'name': str(v)} for v in qs.all()]
        elif self.datatype == 'boolean':
            value_list = [
                {'id':0, 'code':None, 'name': gettext("No")},
                {'id':1, 'code':None, 'name': gettext("Yes")}
            ]
        elif self.vocab:
            qs = self.vocab.vocabvalue_set
            if self.vocab.ordering == 'name':
                qs = qs.order_by('value')
            value_list = [{'id':v.id, 'code':v.value_code, 'name':v.value} for v in qs.all()]
            if self.vocab.name == 'Countries':
                # Add Switzerland as first element in the list
                che_vocab = qs.get(value_code='CHE')
                value_list.insert(0, {'id':che_vocab.id, 'code':che_vocab.value_code, 'name':che_vocab.value})
        return value_list

    def get_value_field(self):
        """ Returns the name of the corresponding object value field """
        if self.vocab_id: return 'value_voc'
        return dt_choices.choices[self.datatype]['db_vfield']

    def get_value_count(self):
        """ Returns the number of values in the database for this attribute """
        return ObjectValue.objects.filter(attr=self).count() if self.pk else 0

    def get_form_field(self, initial=None, with_label=True, required=False):
        """ Return the form field corresponding to this attribute datatype """
        def force_list(val):
            if val is not None and not isinstance(val, types.ListType):
                val = [val]
            return val

        label = with_label and self.atitle or ""
        if self.vocab:
            choices = [(v['code'], truncate(v['name'], 58)) for v in self.get_vocab()]
            if not required:
                choices.insert(0, ('', '------'))
            field = forms.ChoiceField(choices=choices, label=label, initial=initial, help_text=self.ahelp, required=required)
        elif self.datatype == 'qset':
            choices = [(v.id, str(v)) for v in eval(self.queryset)]
            if not required:
                choices.insert(0, ('', '------'))
            if self.multivalued:
                field = forms.MultipleChoiceField(choices=choices, label=label,
                    initial=force_list(initial), help_text=self.ahelp, required=required)
            else:
                field = forms.ChoiceField(choices=choices, label=label,
                    initial=initial, help_text=self.ahelp, required=required)
        else:
            fieldClass = dt_choices.choices[self.datatype]['ffield_class']
            if fieldClass is None:
                return None
            field_opts = {'label': label, 'initial': initial, 'help_text': self.ahelp, 'required': required}
            field_opts.update(dt_choices.choices[self.datatype].get('ffield_opts', {}))
            field = fieldClass(**field_opts)
            if "\n" in str(initial):
                field.widget = forms.Textarea(attrs={'rows':2, 'cols':40})
        return field

CATEGORY_CHOICES = (
    ('general', _("General")), # Category for all sort of objects
    ('a0',      _("Specimens - A0")),
    ('list',    _("Lists/Inventories")),
    #('list:subcat', _("lists of type 'subcat'")),
    ('taxo',    _("Taxonomy")),
    ('statistics', _("Statistics")),
)
class AttributeGroup(models.Model):
    gname       = models.CharField(max_length=50, unique=True)
    category    = models.CharField(max_length=15, choices=CATEGORY_CHOICES, default='general')
    position    = models.IntegerField(default=0)
    attributes  = models.ManyToManyField(Attribute, through='AttributeInGroup')

    class Meta:
        db_table = "attribute_group"

    def __str__(self):
        return self.gname or str(self.id)
    
    @classmethod
    def get_groups(cls, categ_filter=[]):
        groups = cls.objects.all()
        if categ_filter:
            groups = [gr for gr in groups if gr.category in categ_filter]
        return groups

    def get_attributes(self):
        """ Return a list of attributes """
        return [aig.attribute for aig in self.attributeingroup_set.filter(attribute__active=True).order_by('position')]


class AttributeInGroup(models.Model):
    """M2M through table between Attribute and AttributeGroup."""
    attribute = models.ForeignKey(Attribute, on_delete=models.CASCADE)
    group = models.ForeignKey(AttributeGroup, on_delete=models.CASCADE)
    position = models.IntegerField(default=0)

    class Meta:
        db_table = "attribute_in_group"
        unique_together = ("attribute", "group")

    def __str__(self):
        return u"%s in group %s (%d)" % (self.attribute.name, self.group.gname, self.position)


ORDERING_CHOICES = (
    ('code', _(u"By value code")),
    ('name', _(u"By translated name")),
)
class Vocabulary(models.Model):
    name     = models.CharField(max_length=35, unique=True)
    ordering = models.CharField(max_length=10, choices=ORDERING_CHOICES, default='code')

    class Meta:
        db_table = "vocabulary"

    def __str__(self):
        return self.name

    def valid_value(self, value):
        """ Check if value is valid for this vocabulary and return the VocabValue if valid
            Raise ValueError otherwise
        """
        value = str(value)
        try:
            vv = VocabValue.objects.get(vocab=self, value_code__iexact=value)
        except VocabValue.DoesNotExist:
            # Also try to get value through value
            vv = VocabValue.objects.filter(vocab=self, value__iexact=value)
            if len(vv) == 1:
                vv = vv[0]
            elif len(vv) == 0:
                raise ValueError(gettext("The value '%(val)s' has not been found in the vocabulary %(voc)s" % {
                    'val': value, 'voc': self.name}))
            else:
                for vv_sup in vv[1:]:
                    if vv_sup != vv[0]:
                        raise ValueError(gettext("Possible ambiguity with value '%(val)s' in vocabulary %(voc)s" % {
                            'val': value, 'voc': self.name}))
                vv = vv[0]
        return vv


class VocabValue(models.Model):
    vocab = models.ForeignKey('Vocabulary', on_delete=models.CASCADE)
    value_code  = models.CharField(max_length=10)
    value       = models.CharField(max_length=120)

    class Meta:
        db_table = "vocab_value"
        ordering = ('value_code',)

    def __str__(self):
        return "%s: %s" % (self.value_code, self.value)


class ObjectValueQuerySet(QuerySet):
    def as_list(self):
        """ Return a list of queryset objects, of the right type
            Should always be the last method called on a queryset """
        olist = []
        for o in self:
            olist.append(o.get_proxied())
        return olist


class ObjectValueManager(models.Manager):
    def get_queryset(self):
        return ObjectValueQuerySet(self.model)

    def get(self, *args, **kwargs):
        return super().get(*args, **kwargs).get_proxied()

    def get_or_create(self, **kwargs):
        obj, cr = super().get_or_create(**kwargs)
        return obj.get_proxied(), cr

class ObjectValueFactory:
    def __new__(cls, *args, **kwargs):
        ov = ObjectValue(*args, **kwargs)
        return ov.get_proxied()

def int_validator(value):
    # MySQL limits on INT
    if value > 2147483647 or value < -2147483647:
        raise ValidationError("value out of limits")

value_render = Signal()


class ObjectValue(models.Model):
    obj = models.ForeignKey('BaseObject', on_delete=models.CASCADE)
    attr = models.ForeignKey('Attribute', on_delete=models.CASCADE)
    value_text  = models.TextField(blank=True, null=True)
    value_int   = models.IntegerField(blank=True, null=True, validators=[int_validator])
    value_float = models.FloatField(blank=True, null=True)
    value_voc = models.ForeignKey(VocabValue, blank=True, null=True, on_delete=models.CASCADE)
    comments    = models.TextField(blank=True, null=True)
    objects = ObjectValueManager()

    class Meta:
        db_table = "object_value"

    def __str__(self):
        return "%s: %s" % (self.attr.name, self.get_value())

    def _value_field(self):
        return self.attr.get_value_field()

    def get_proxied(self):
        """ Set the appropriate ObjectValue proxy class and return self """
        if self.attr and self.attr.datatype == 'date':
            self.__class__ = ObjectValueDate
        if self.attr and self.attr.datatype == 'boolean':
            self.__class__ = ObjectValueBoolean
        elif self.attr and self.attr.vocab:
            self.__class__ = ObjectValueVocab
        elif self.attr and self.attr.queryset:
            self.__class__ = ObjectValueQueryset
        return self

    def clean(self):
        """ Object validation method to prevent several values for same attribute,
            until a proper multivalue logic is implemented """
        if not self.attr.multivalued:
            exist_count = ObjectValue.objects.filter(obj=self.obj, attr=self.attr).exclude(pk=self.id).count()
            if exist_count > 0:
                raise ValidationError(_("There is already a value for this attribute"))

    def validate(self, value):
        if isinstance(value, str):
            return value.strip()
        return value

    def get_value(self, raw=False):
        if self.attr.calculator:
            # Dynamic value
            method = getattr(self.obj, self.attr.calculator, None)
            if callable(method):
                value = method()
            else:
                raise ValueError("")
            return value
        if self.value_voc:
            if raw:
                value = self.value_voc.value_code
            else:
                value = self.value_voc.value
        elif self.attr.datatype == 'qset':
            if raw:
                value = self.value_int
            else:
                base_qset = eval(self.attr.queryset)
                try:
                    value = base_qset.filter(pk=self.value_int)[0]
                    if hasattr(value, 'get_absolute_url'):
                        value = mark_safe('<a href="%s">%s</a>' % (value.get_absolute_url(), value))
                    else:
                        value = str(value)
                except (ObjectDoesNotExist, IndexError):
                    value = gettext("unvalid object")
        else:
            value = getattr(self, self._value_field())
        return value

    def set_value(self, value_dict, save=True):
        """ value_dict is a dict with the same struct as form.cleaned_data, in the form:
            {'value_DESCRNAME1': value, 'value_DESCRNAME2': value, 'comments': ...} """
        assert isinstance(value_dict, dict)
        if not self.attr.is_editable():
            return None
        if self.attr.vocab:
            value = value_dict.get('value_%s' % self.attr.name)
            vv = self.validate(value)
            self.value_voc = vv
        else:
            value = self.validate(value_dict.get('value_%s' % self.attr.name))
            setattr(self, self._value_field(), value)
        self.comments = value_dict.get('comments', None)
        self.full_clean()
        if save:
            self.save()
        if self.obj._cached_values and self.attr.name not in self.obj._cached_values:
            self.obj._cached_values[self.attr.name] = self

    def render(self, context):
        """ Prepare the value to be HTML ready """
        if self.attr.render_template:
            t = Template(self.attr.render_template)
            context.update({"attribute": self.attr})
            return t.render(context)
        else:
            WORD_MAX_SIZE = 32
            rendered_val = self.get_value()
            if not isinstance(rendered_val, SafeData):
                rendered_val = str(self.get_value())
                for word in rendered_val.split():
                    if len(word) > WORD_MAX_SIZE:
                        # Cut word and replace in val
                        new_word = ""
                        for i in range(0, (len(word) // WORD_MAX_SIZE) + 1):
                            new_word += word[i * WORD_MAX_SIZE: (i+1) * WORD_MAX_SIZE] + " "
                        rendered_val = rendered_val.replace(word, new_word[:-1])
                rendered_val = rendered_val.replace("\n", "<br/>")
            if self.comments:
                rendered_val = u"%s <em>(%s)</em>" % (rendered_val, self.comments)
            result = [rendered_val]
            #value_render.send(sender = self, result = result)
            return ''.join(result)

            
class ObjectValueDate(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        return date_validator(value)


class ObjectValueBoolean(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        if value in ('1', 'on', 'true', 'True'):
            return True
        elif value in ('0', 'off', 'false', 'False'):
            return False
        return value

    def render(self, context):
        rendered_val = self.get_value() and gettext("Yes") or gettext("No")
        if self.comments:
            rendered_val = "%s <em>(%s)</em>" % (rendered_val, self.comments)
        return rendered_val


class ObjectValueVocab(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        """ Get a vocab value object corresponding to value (string) """
        return self.attr.vocab.valid_value(value)


class ObjectValueQueryset(ObjectValue):
    class Meta:
        proxy = True

    def validate(self, value):
        """ Get a vocab value object corresponding to value (string) """
        queryset = eval(self.attr.queryset)

        if isinstance(value, models.Model):
            value_pk = value.pk
            if queryset.filter(pk = value_pk).exists():
                return value_pk

        try:
            value_pk = int(value)
        except (TypeError, ValueError):
            pass
        else:
            if queryset.filter(pk=value_pk).exists():
                return value_pk

        try:
            return eval(self.attr.queryset).get(title__iexact=str(value)).pk
        except BaseObject.DoesNotExist:
            pass

        raise ValidationError("The value '%s' is not valid for attribute %s" % (value, self.attr.name))


class MetaValue:
    """ Abstraction level over ObjectValue ORM model (handle multivalues) """

    EMPTY_VALUE = ['', []]

    def __init__(self, attr, obj, values=None):
        self.id = attr.name
        self.obj = obj
        self.attr = attr
        self.ovalues = []
        if values is not None:
            self.ovalues = values

    @property
    def comments(self):
        return "\n".join(filter(None, [val.comments for val in self.ovalues]))

    def set_value(self, value_dict):
        value = value_dict.get('value_%s' % self.attr.name)
        self.delete()
        if isinstance(value, (list, tuple)):
            value = filter(lambda v: v not in self.EMPTY_VALUE, value)
            for val in value:
                ovalue = ObjectValueFactory(obj=self.obj, attr=self.attr)
                value_dict['value_%s' % self.attr.name] = val
                ovalue.set_value(value_dict)
                self.ovalues.append(ovalue)
        elif value not in self.EMPTY_VALUE:
            ovalue = ObjectValueFactory(obj=self.obj, attr=self.attr)
            ovalue.set_value(value_dict)
            self.ovalues = [ovalue]

    def append_value(self, value):
        self.ovalues.append(value)

    def get_value(self, raw=False):
        return [val.get_value(raw) for val in self.ovalues]

    def render(self, context):
        return u"<br>".join([val.render(context) for val in self.ovalues])

    def delete(self):
        [val.delete() for val in self.ovalues]
        self.ovalues = []


class FileAttachment(models.Model):
    fil      = models.FileField(upload_to='attachments', verbose_name=_("File"))
    title    = models.TextField(blank=True, verbose_name=_("Title"))
    mimetype = models.CharField(max_length=100, blank=True, editable=False)
    # Generic relation fields
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    class Meta:
        db_table = "fileattachment"
    
    def __str__(self):
        return os.path.basename(self.fil.name)

    def get_icon(self):
        """ Return an icon from the file type, if available in STATIC_URL/img/mimetypes """
        img_abspath = finders.find(os.path.join('img','mimetypes', "%s.png" % self.mimetype.replace("/","-")))
        if img_abspath:
            return os.path.join(settings.STATIC_URL, 'img','mimetypes', "%s.png" % self.mimetype.replace("/","-"))
        return os.path.join(settings.STATIC_URL, 'img','mimetypes', 'unknown.png')


class InstituteQuerySet(models.QuerySet):
    def get(self, **kwargs):
        # queryset attributes are querying models by 'title'
        if 'title__iexact' in kwargs:
            kwargs['code__iexact'] = kwargs['title__iexact']
            del kwargs['title__iexact']
        return super().get(**kwargs)


class Institute(models.Model):
    name = models.CharField(max_length=250)
    code = models.CharField(max_length=10, unique=True)

    objects = InstituteQuerySet.as_manager()

    def __str__(self):
        return f'{self.code} - {self.name}'


class Lot(models.Model):
    PHYS_LOC_CHOICES = (
        ('col_active', 'collection active (frigo)'),
        ('col_base', 'collection de base (congélateur)'),
        ('col_cons', 'collection de conservation (externe)'),
        ('commande', 'commande (externe)'),
        ('multiplic', 'en multiplication'),
    )
    AFFECTATION_CHOICES = (
        ('commande', 'Commande'),
        ('conserv', 'Conservation'),
        ('multiplic', 'Multiplication'),
        ('retrait', 'Retrait'),
        ('temoin', 'Lot témoin'),
        ('test', 'Test de germination'),
        ('transfert', 'Transfert'),
    )
    accession = models.ForeignKey(BaseObject, on_delete=models.CASCADE)
    designation = models.CharField(_("Designation"), blank=True, max_length=60) # LOT_NUMERO, si None, LOT_NUMERO_SEQ
    creation = models.DateField(_("Creation date"), blank=True, null=True) # LOT_DATE_CREATION
    annee_recolte = models.PositiveSmallIntegerField("Année de récolte", null=True, blank=True) # LOT_ANNEE_RECOLTE
    transfert = models.DateField("Date de transfert", blank=True, null=True)  # LOT_DATE_TRANSFERT
    #lot_parent = models.ForeignKey("self", blank=True, null=True, on_delete=models.SET_NULL)  # LOT_PARENT
    lot_parent = models.CharField("Lot parent", max_length=50, blank=True)  # LOT_PARENT
    retire = models.BooleanField("Lot retiré", default=False)  # LOT_RETIRE
    germ_rate_initial = models.SmallIntegerField(_("Initial germination rate"), null=True, blank=True) # LOT_TAUX_GERM_INITIAL
    pmg = models.DecimalField("Poids mille grains [g]", max_digits=8, decimal_places=2, null=True, blank=True) # LOT_PMG
    weight = models.DecimalField(_("Weight [g]"), max_digits=8, decimal_places=2, null=True, blank=True) # LOT_POIDS
    total_weight = models.DecimalField(_("Total weight [g]"), max_digits=8, decimal_places=2, null=True, blank=True) # LOT_POIDS_TOT
    nb_sachet = models.PositiveSmallIntegerField("Nb sachets", null=True, blank=True) # LOT_NB_SACHETS
    nb_plants = models.IntegerField("Nb de plants", null=True, blank=True)
    phys_location = models.CharField(_("Location (physical genebank)"), max_length=12, choices=PHYS_LOC_CHOICES, blank=True) # LOT_EMPLACEMENT
    instcode = models.ForeignKey(Institute, null=True, blank=True, on_delete=models.SET_NULL)
    parcelle = models.CharField("Parcelle", max_length=30, blank=True)  # LOT_PARCELLE
    box_id = models.CharField(_("Box ID"), max_length=50, blank=True) # LOT_CAISSE
    loc_x = models.CharField("Bloc (coordonnée X)", max_length=20, blank=True) # LOT_BLOC
    loc_y = models.CharField("Rangée (coordonnée Y)", max_length=20, blank=True) # LOT_RANGEE
    loc_z = models.CharField("Étagère (coordonnée Z)", max_length=20, blank=True) # LOT_ETAGERE
    affectation = models.CharField("Affectation", max_length=10, choices=AFFECTATION_CHOICES, blank=True) # LOT_AFFECTATION
    comments = models.TextField(_("Comments"), blank=True) # LOT_COMMENTAIRE
    # LOT_DATE_TEST_DERNIER / LOT_TAUX_GERM_DERNIER calculés auto à partir des tests du lot
    lot_code1 = models.CharField("Code lot 1", max_length=60, blank=True)  # LOT_CODE_1
    lot_code2 = models.CharField("Code lot 2", max_length=60, blank=True)  # LOT_CODE_2
    lot_code3 = models.CharField("Code lot 3", max_length=60, blank=True)  # LOT_CODE_3

    header_mapping = {
        'LOT_NUMERO': 'designation',
        'LOT_ANNEE_RECOLTE': 'annee_recolte',
        'LOT_PARENT': 'lot_parent',
        'LOT_DATE_CREATION': 'creation',
        'LOT_DATE_TRANSFERT': 'transfert',
        'LOT_RETIRE': 'retire',
        'LOT_TAUX_GERM_INITIAL': 'germ_rate_initial',
        'LOT_PMG': 'pmg',
        'LOT_POIDS': 'weight',
        'LOT_POIDS_TOT': 'total_weight',
        'LOT_NB_SACHETS': 'nb_sachet',
        'LOT_EMPLACEMENT': 'phys_location',
        'LOT_INSTCODE': 'instcode',
        'LOT_PARCELLE': 'parcelle',
        'LOT_CAISSE': 'box_id',
        'LOT_BLOC': 'loc_x',
        'LOT_RANGEE': 'loc_y',
        'LOT_ETAGERE': 'loc_z',
        'LOT_AFFECTATION': 'affectation',
        'LOT_CODE_1': 'lot_code1',
        'LOT_CODE_2': 'lot_code2',
        'LOT_CODE_3': 'lot_code3',
        'LOT_COMMENTAIRE': 'comments',
    }

    def __str__(self):
        return f'Lot {self.designation} pour l’accession {self.accession}'

    @classmethod
    def attribute_fields(cls):
        fields = []
        header_mapping_inv = {v: k for k, v in cls.header_mapping.items()}
        for field in cls._meta.get_fields():
            if field.one_to_many or field.name in ('id', 'accession'):
                continue
            field.header_key = header_mapping_inv.get(field.name, field.name)
            fields.append(field)
        return fields

    def attributes(self, empty=False):
        for field in self.attribute_fields():
            if hasattr(self, f'get_{field.name}_display'):
                value = getattr(self, f'get_{field.name}_display')()
            else:
                value = getattr(self, field.name)
            if value not in (None, '', False):
                yield field, value
            elif empty:
                yield field, ''


class GermTest(models.Model):
    NOMBRE_SEM_CHOICES = (
        (1, '25'),
        (2, '50'),
        (3, '100'),
        (4, '150'),
        (5, '200'),
    )
    lot = models.ForeignKey(Lot, on_delete=models.CASCADE)
    date_test = models.DateField("Date du test", null=True, blank=True)  # TEST_DATE
    numero = models.CharField("Numéro du test", max_length=30, blank=True) # TEST_NUMERO
    termine = models.BooleanField("Test terminé", default=False) # TEST_TERMINE
    # Should be automatic from lot.accession
    acc_num = models.CharField(max_length=50, blank=True)  # TEST_ACC_NUMERO
    nombre = models.PositiveSmallIntegerField("Nombre de semences testées", choices=NOMBRE_SEM_CHOICES, null=True, blank=True) # TEST_nombre
    taux1 = models.PositiveSmallIntegerField("Taux 1er comptage des plantules normales", null=True, blank=True) # TEST_TAUX_1
    taux2 = models.PositiveSmallIntegerField("Taux 2ème comptage des plantules normales", null=True, blank=True) # TEST_TAUX_2
    taux3 = models.PositiveSmallIntegerField("Taux 3ème comptage des plantules normales", null=True, blank=True) # TEST_TAUX_3
    taux_norm = models.PositiveSmallIntegerField("Résultat final plantules normales en %", null=True, blank=True) # TEST_normales
    taux_anorm = models.PositiveSmallIntegerField("Taux de plantules anormales", null=True, blank=True) # TEST_anormales
    taux_durs = models.PositiveSmallIntegerField("Taux dures/dormantes", null=True, blank=True) # TEST_durs
    taux_morts = models.PositiveSmallIntegerField("Taux de plantules mortes", null=True, blank=True) # TEST_mortes
    comments = models.TextField(_("Comments"), blank=True) # TEST_COMMENTAIRE

    attribute_field_names = [
        'nombre', 'taux1', 'taux2', 'taux3', 'taux_norm', 'taux_anorm',
        'taux_durs', 'taux_morts', 'comments',
    ]

    header_mapping = {
        'TEST_DATE': 'date_test',
        'TEST_NUMERO': 'numero',
        'TEST_TERMINE': 'termine',
        'TEST_nombre': 'nombre',
        'TEST_TAUX_1': 'taux1',
        'TEST_TAUX_2': 'taux2',
        'TEST_TAUX_3': 'taux3',
        'TEST_normales': 'taux_norm',
        'TEST_anormales': 'taux_anorm',
        'TEST_durs': 'taux_durs',
        'TEST_mortes': 'taux_morts',
        'TEST_COMMENTAIRE': 'comments',
    }

    def __str__(self):
        return f'Test de germination ({self.date_test})'

    @classmethod
    def attribute_fields(cls):
        fields = []
        header_mapping_inv = {v: k for k, v in cls.header_mapping.items()}
        for name in cls.attribute_field_names:
            field = cls._meta.get_field(name)
            field.header_key = header_mapping_inv.get(field.name, field.name)
            fields.append(field)
        return fields

    def attributes(self, empty=False):
        for field in self.attribute_fields():
            if hasattr(self, f'get_{field.name}_display'):
                value = getattr(self, f'get_{field.name}_display')()
            else:
                value = getattr(self, field.name)
            if value not in (None, '', False):
                yield field.name, field.verbose_name, value
            elif empty:
                yield field.name, field.verbose_name, ''


class Command(models.Model):
    lot = models.ForeignKey(Lot, on_delete=models.PROTECT)
    designation = models.CharField(_("Designation"), blank=True, max_length=200)
    date_cmd = models.DateField(_("Command date"), blank=True, null=True)
    weight = models.DecimalField(_("Weight [g]"), max_digits=8, decimal_places=2, null=True, blank=True)
    comment = models.TextField(blank=True)

    def __str__(self):
        return self.designation

    def attributes(self):
        for field_name in ["date_cmd", "weight", "comment"]:
            value = getattr(self, field_name)
            if value not in (None, '', False):
                field = self._meta.get_field(field_name)
                yield field_name, field.verbose_name, value
