from django.core.paginator import Paginator, InvalidPage, EmptyPage


class BGNPaginator(Paginator):
    """ Extend Django paginator """

    def __init__(self, object_list, per_page, **kwargs):        
        if per_page == 'All':
            per_page = 2000
        else:
            per_page = int(per_page)

        super().__init__(object_list, per_page, **kwargs)

    def page(self, number):
        """
        Copy of Paginator.page
        Return a Page object for the given 1-based page number."""
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        # Our customization: append/prepend accessions if they are children
        # to avoid displaying part trees (parent/children on different pages)
        # in paginated accessions.
        obj_list = self.object_list[bottom:top]
        if len(obj_list) and obj_list[0].obj_type.code == 'accession':
            obj_list = list(obj_list)
            min_level = min(obj.level for obj in obj_list)
            # Append next accessions while they are children
            while True:
                try:
                    next_candidate = self.object_list[top]
                except IndexError:
                    break
                is_child = next_candidate.level - min_level > 0
                if is_child:
                    obj_list.append(next_candidate)
                    top += 1
                else:
                    break
            # Prepend previous accessions while top one is children
            while obj_list[0].level - min_level > 0 and bottom >= 1:
                obj_list.insert(0, self.object_list[bottom - 1])
                bottom -= 1
        # End of customization
        return self._get_page(obj_list, number, self)

    def get_context(self, request):
        """ Return a dictionary containing various useful context variables """
        no_page = int(request.GET.get('p', '1'))

        try:
            self.current_page = self.page(no_page)
        except (EmptyPage, InvalidPage):
            self.current_page = self.page(self.num_pages)

        next_number = self.count - self.current_page.end_index()
        if next_number > self.per_page:
            next_number = self.per_page

        # Compute page range
        page_range = []

        if no_page != 1:
            page_range.append('1')
        if no_page > 7:
            page_range.append('...')

        for p in range(no_page - 5, no_page):
            if p > 1:
                page_range.append(str(p))

        page_range.append('[%d]' % no_page)

        for p in range(no_page+1, no_page+6):
            if p < self.num_pages: page_range.append(str(p))

        if self.num_pages - no_page > 6:
            page_range.append('...')

        if no_page != self.num_pages:
            page_range.append(str(self.num_pages))

        query_string = "&".join(["%s=%s" % (k,v) for k,v in request.GET.items() if k != "page"])

        return {
            'page': self.current_page,
            'page_range': page_range,
            'next_number': next_number,
        }
