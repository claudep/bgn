# -*- coding: utf-8 -*-

hortus_attributes = (
    {'name':"OTHERNUMB_BDN", 'title':{'en':"BDN number", 'de':u"Identifikationsnummer BDN", 'fr':u"Numéro d'identification BDN"},
     'code':None, 'help':"Specimen number in the bdn (www.bdn.ch)", 'datatype':"text", 'vocab':None},
    {'name':"COLLDATEHIST", 'title':{'en':"Collect date of base material", 'de':u"Erntedatum des Fundmaterials", 'fr':u"Date de collecte du matériel original"},
     'code':None, 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"COLLSITE_GEM", 'title':{'en':"Collecting site locality", 'de':u"Fundort Gemeinde", 'fr':u"Commune de la collecte"},
     'help':{'en':"Locality name where the breed has been found", 'de':u"Name der Gemeinde in der das Saatgut gefunden wurde", 'fr':u"Nom de la commune où la semence a été découverte"},
     'code':None, 'help':"", 'datatype':"text", 'vocab':None},
    {'name':"NUMB_EAR", 'title':{'en':"Ear number", 'de':u"Anzahl Kolben", 'fr':u"Nombre d'épis"},
     'code':None, 'help':"", 'datatype':"float", 'vocab':None},
    {'name':"ACCCONTENTS_UNITY", 'title':u"Zahlenwert und Einheit Akzession", 'code':None, 'help':u"Beispiel: 100g", 'datatype':"text"},
    {'name':"VARCONSERVSTAT_CH", 'title':u"Steht auf der Positivliste der Schweiz", 'code':None, 'help':u"Gibt Auskunft, ob die Sorte in der Schweiz als erhaltungswürdig angesehen wird oder nicht.", 'datatype':"text", 'vocab': 'YesNo'},
    {'name':"IDENTIFIED", 'title':u"Die Akzession wurde identifiziert", 'code':None, 'help':u"", 'datatype':"text", 'vocab': 'YesNoUncertain'},
    {'name':"IDENTIFICATION_PRC", 'title':u"Bestimmung Sicherheit in %", 'code':None, 'help':u"Sicherheit in %, mit welcher eine Akzession bestummen wurde.",
     'datatype':"integer"},
    {'name':"IDENTIFICATION_PERSON", 'title':u"Name des Bestimmer", 'code':None, 'datatype':"text"},
    {'name':"IDENTIFICATION_DATE", 'title':u"Datum der Sortenidentifikation", 'code':None, 'help':u"Datum (Jahr) der Sortenidentifikation rsp. In welchem das Fruchtmuster genommen wurde", 'datatype':"text"},
    {'name':"IDENTIFICATION_REMARKS", 'title':u"Bemerkungen zur Identifikation", 'code':None, 'datatype':"text"},
    {'name':"CATEGORY", 'title':u"Kategorien von Kulturpflanzen", 'code':None, 'datatype':"text", 'vocab':"Categories"},
    {'name':"ACCMULT_REMARK", 'title':u"Bemerkungen zur Vermehrung", 'code':None, 'datatype':"text"},
)
