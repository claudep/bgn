from django.contrib import admin
from django.conf import settings

from invent import models


@admin.register(models.BaseObject)
class BaseObjectAdmin(admin.ModelAdmin):
    list_filter = ('obj_type',)
    list_display = ('title_not_empty', 'obj_type', 'weight')
    search_fields = ['title', 'description']
    raw_id_fields = ['parent']

    def title_not_empty(self, obj):
        return obj.title or '<no title>'
    title_not_empty.short_description = "Title"


@admin.register(models.Attribute)
class AttributeAdmin(admin.ModelAdmin):
    search_fields = ['name', 'ahelp']
    list_display = ('name', 'atitle', 'active', 'searchable', 'datatype', 'vocab')
    list_filter = ('datatype',)
#    raw_id_fields = ('parent',)


class AttributeForObjectInline(admin.TabularInline):
    model = models.AttributeForObject


@admin.register(models.ObjectType)
class ObjectTypeAdmin(admin.ModelAdmin):
    list_display = ['tname', 'code', 'searchable']
    inlines = [ AttributeForObjectInline ]


@admin.register(models.AttributeForObject)
class AttributeForObjectAdmin(admin.ModelAdmin):
    list_display = ('obj_type', 'attr_group', 'attr', 'position')


class AttributeInGroupInline(admin.TabularInline):
    model = models.AttributeInGroup
    ordering = ('position',)


@admin.register(models.AttributeGroup)
class AttributeGroupAdmin(admin.ModelAdmin):
    list_display = ('gname', 'category', 'position')
    inlines = [AttributeInGroupInline]


@admin.register(models.VocabValue)
class VocabValueAdmin(admin.ModelAdmin):
    search_fields = ('value_code', 'value')


class VocabValueInline(admin.TabularInline):
    model = models.VocabValue


@admin.register(models.Vocabulary)
class VocabularyAdmin(admin.ModelAdmin):
    inlines = [VocabValueInline]


@admin.register(models.ObjectValue)
class ObjectValueAdmin(admin.ModelAdmin):
    search_fields = ['attr__name', 'value_text', 'value_int', 'value_float']
    raw_id_fields = ['obj']


@admin.register(models.Institute)
class InstituteAdmin(admin.ModelAdmin):
    ordering = ['code']
    search_fields = ['code', 'name']


@admin.register(models.Lot)
class LotAdmin(admin.ModelAdmin):
    raw_id_fields = ['accession']


admin.site.register(models.FileAttachment)
admin.site.register(models.GermTest)
