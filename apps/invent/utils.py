# -*- coding: utf-8 -*-
from django.conf import settings

def default_language():
    """ Returns the locale code of the default language ('en', ...) """
    return settings.LANGUAGES[settings.DEFAULT_LANGUAGE-1][0]

def truncate(text, length):
    if len(text) < length:
        return text
    return u"%s…" % text[:length]
