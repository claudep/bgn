from django.core.management.base import BaseCommand

from invent.data.data import load_vocs, load_attrs, load_object_types
from invent.data import eurisco_attributes, bdn_attributes, various_attributes

class Command(BaseCommand):
    """ Import the Attributes defined in the invent/data folder """
    def handle(self, *args, **options):
        load_vocs()
        load_attrs(eurisco_attributes, bdn_attributes, various_attributes)
        load_object_types()
