from django.core.management.base import BaseCommand

from invent import data
from invent.data.data import load_vocs

class Command(BaseCommand):
    """ Create all vocabularies from data/vocabuleries.py """
    def handle(self, *args, **options):
        load_vocs()
        print "vocabularies loaded."
