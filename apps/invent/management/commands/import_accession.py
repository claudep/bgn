from django.core.management.commands.loaddata import Command as BaseCommand


class Command(BaseCommand):
    """Mirror command of export_accession, based on loaddata but adapted for MPTT models"""
    def __init__(self, *args, **kwargs):
        self.first_obj = None
        super().__init__(*args, **kwargs)

    def save_obj(self, obj):
        if hasattr(obj.object, 'lft'):
            # Delete MPTT fields, willl be recreated
            obj.object.level = None
            obj.object.lft = None
            obj.object.rght = None
            obj.object.tree_id = None
            obj.object.save()
            if not self.first_obj:
                self.first_obj = obj.object
            return True
        else:
            return super().save_obj(obj)

    def loaddata(self, fixture_labels):
        super().loaddata(fixture_labels)
        self.first_obj.move_to_sorted_position('ACC_NUMERO', self.first_obj.get_value_for('ACC_NUMERO'))
        
