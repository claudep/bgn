from io import StringIO
from itertools import chain      

from django.contrib.admin.utils import NestedObjects
from django.core import serializers
from django.core.management.base import BaseCommand

from invent.models import BaseObject


class Command(BaseCommand):
    """ Export/dump a base object with all related attributes """

    def add_arguments(self, parser):
        parser.add_argument('--object-id', required=True)

    def handle(self, *args, **options):
        
        collector = NestedObjects(using="default")
        collector.collect([BaseObject.objects.get(pk=options['object_id'])])

        objects = list(chain.from_iterable(collector.data.values()))
        out = StringIO()
        # Unfortunately, the order is not always suitable to be safely reimported
        # May need manual reordering
        out.write(serializers.serialize("json", objects, indent=1))
        return out.getvalue()
