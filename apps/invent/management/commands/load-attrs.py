from django.core.management.base import BaseCommand

from invent import data
from invent.data.data import load_attrs, load_groups, load_vocs, load_object_types
#from invent.data import eurisco_attributes, bdn_attributes, various_attributes

class Command(BaseCommand):
    """ Import the Attribute lists specified on command line:
        python manage.py load-attrs path.file.containing.attributes [nextpath ...] """
    def handle(self, *args, **options):
        load_vocs() # standard vocabularies
        for arg in args:
            imp_from, imp_name = arg.rsplit('.', 1)
            try:
                mod = __import__(arg, globals(), locals(), [imp_name], -1)
            except ImportError:
                print "Sorry, unable to import the module '%s'" % arg
                continue
            if hasattr(mod, 'vocs'):
                load_vocs(mod.vocs)
            if hasattr(mod, 'attributes'):
                load_attrs(mod.attributes)
                print "'%s' attributes successfully loaded" % arg
            else:
                print "Sorry, there are no '%s' attributes" % arg
            if hasattr(mod, 'attribute_groups'):
                load_groups(mod.attribute_groups)
            if hasattr(mod, 'object_types'):
                load_object_types(mod.object_types)
