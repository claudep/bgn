/* obj_id is a global variable defined in the template */
function show_groups() {
    if ($("#attr_groups").is (':visible')) {
        $("#attr_groups").hide();
        $("#groups_expander").attr("src", static_url+"img/closed.png");
    } else {
        $("#attr_groups").show();
        $("#groups_expander").attr("src", static_url+"img/open.png");
    }
    return false;
}

function show_attrs(group_id) {
    /* Toggle function to show/hide attribute groups
       First call to show attributes load them through an ajax call */
    var attr_list = $("#attr_list_"+group_id);
    if (attr_list.is (':visible')) {
        attr_list.hide();
        $("#expander_"+group_id).attr("src", static_url+"img/closed.png");
    } else {
        if (attr_list.html() == "") {
            // find content through ajax call
            $.get("/attributes_in_group/"+group_id+"/", function(data){
                var descriptors = eval(data);
                var content = "";
                for(var i=0; i<descriptors.length; i++) {
                    content += "<a href='.' onclick='return add_descriptor("+obj_id+","+descriptors[i]['id']+","+descriptors[i]['dyn']+")' title='"+descriptors[i]['title']+"'>"+descriptors[i]['name']+"</a><br />";
                }
                attr_list.html(content);
            });
        }
        $("#expander_"+group_id).attr("src", static_url+"img/open.png");
        attr_list.show();
    }
    return false;
}

function add_descriptor(object_id, descr_id, is_dynamic, refresh_func) {
    /* Add a new object value edit form */
    if (is_dynamic) {
        // Add value in page
        var url = "/"+object_id+"/value/new/"+descr_id+"/";
        $.ajax({
            type: "POST",
            url: url,
            data: "value=0",
            success: function(data){
                $("#new_descriptor").after(data);
            },
            error: function (xhr, ajaxOptions, thrownError){
                $("#attr_edit_errors-0").html(xhr.responseText);
            }
        });
    } else {
        // Get widget through Ajax and display on the page
        $.get("/"+object_id+"/value/new/"+descr_id+"/", function(data) {
            $("#new_descriptor").html(data);
            $("#new_descriptor").show();
            if (refresh_func == null) {
                $("form#descr_edit_form-0").data('refresh_func', function(d) {
                    // TODO: add a waiting icon
                    window.location.reload();
                });
            } else {
                $("form#descr_edit_form-0").data('refresh_func', refresh_func);
            }
        });
    }
    return false;
}

function edit_value(value_id) {
    $.get("/"+obj_id+"/value/"+value_id+"/edit/", function(formdata) {
        // Hide displayed value and show the form obtained by 'get'
        $("div#value-"+value_id).hide();
        $("div#value-"+value_id).after(formdata);
        $("form#descr_edit_form-"+value_id).data('refresh_func', function(d) {
            $("div#value-"+value_id+" > .value").html(d);
            $("div#value-"+value_id).show();
        });
        var i = 1;
    });
    return false;
}

function save_descriptor(form, value_id) {
    /* Save an attribute value for an object, either a new value (value_id=0) or
       an existing value (value_id is id or attr name) */
    var object_id = $(form).find("input#id_object").val();
    if (value_id == 0) {
        var url = "/"+object_id+"/value/new/"+$(form).find("input#id_descriptor").val()+"/";
    } else {
        var url = "/"+object_id+"/value/"+value_id+"/edit/";
    }
    $.ajax({
        type: "POST",
        url: url,
        data: $(form).serialize(),
        success: function(data){
            $(form).data('refresh_func')(data);
            $(form).remove();
        },
        error: function (xhr, ajaxOptions, thrownError){
            $("#attr_edit_errors-"+value_id).html(xhr.responseText);
        }
    });
    return false;
}

function cancel_descriptor(form, value_id) {
    $(form).remove();
    if (value_id != 0) {
        $("div#value-"+value_id).show();
    }
    return false;
}

function delete_value(value_id) {
    // confirms array is defined at template level to be translatable
    if (confirm(gettext("Do you really want to delete this value?"))) {
        $.ajax({
            type: "POST",
            url: "/"+obj_id+"/value/del/",
            data: "id_to_delete="+value_id,
            success: function(data){
                $("div#value-"+value_id).hide();
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}

function delete_photo(photo_id) {
    if (confirm(gettext("Do you really want to delete this photo?"))) {
        $.ajax({
            type: "POST",
            url: "/"+obj_id+"/photo/del/",
            data: "id_to_delete="+photo_id,
            success: function(data){
                $("div#photo-"+photo_id).hide();
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}

function delete_file(file_id) {
    if (confirm(gettext("Do you really want to delete this file?"))) {
        $.ajax({
            type: "POST",
            url: "/"+obj_id+"/file/del/",
            data: "id_to_delete="+file_id,
            success: function(data){
                $("div#file-"+file_id).hide();
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert(xhr.responseText);
            }
        });
    }
    return false;
}

function set_taxon(id, name) {
    // This function receive the taxon id and name when selected by the taxon select widget
    form = $("#taxon_selector").parents("form");
    form.find(".form_value").val(id);
    save_descriptor(form, 0);
};

function duplicate_input_num_accession(obj_parent_id) {
    var num_new_acc = $("#num_acce").val()
    var nom_new_acc = $("#name_acce").val()
   
    if (num_new_acc != "") {
        $.ajax({
            type: "POST",
            url: $('#acc_dup_form').attr('action'),
            data: { num_new_acc : num_new_acc,  nom_new_acc: nom_new_acc},
            success: function(data){
                if (data.result == 'OK') {
                    window.location.href = data.new_url;
                }
            },
            error: function (xhr, ajaxOptions, thrownError){
                alert("Error: " + xhr.responseText);
            }
        });
    }
    else {
        alert("It's not possible to duplicate without accession number !");
    }
    return false;
}
