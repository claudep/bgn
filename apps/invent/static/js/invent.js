function open_taxo(url) {
    window.open(url, "taxonomy", "width=600,height=440,resizable=1,scrollbars=1");
    return false;
}

function get_page(navigBlock, page_num) {
    /* Get a sublist of content items, paginated */
    const url = navigBlock.dataset.url;
    const parent = navigBlock.closest('.object_block_content').parentNode;
    $.ajax({
        type: "GET",
        url: url,
        data: "p="+page_num,
        success: (data) => {
            createCookie(url, page_num);
            const block = navigBlock.closest('.object_block_content');
            const div = document.createElement('div');
            div.innerHTML = data;
            block.replaceWith(div);
            setDisplayByInitial(parent);
            setPaginationClicks(parent);
        },
        error: (xhr, ajaxOptions, thrownError) => {
            alert(xhr.responseText);
        }
    });
    return false;
}

/* Function from object_edit to toggle display of field groups */
function toggle_group(group_name) {
    tbody = $("#"+group_name+"-tbody")
    if (tbody.is (':visible')) {
        tbody.hide();
        $("#"+group_name+"-expander").attr("src", static_url+"img/menu_right.gif");
    } else {
        tbody.show();
        $("#"+group_name+"-expander").attr("src", static_url+"img/menu_down.gif");
    }
    return false;
}

function zoom_image(img_url, width, title) {
    $("#col_func").hide();
    $("#object_content").hide();
    img_html = "\
    <div class='photo' style='width:"+ width +"px;'>\
      <div><img src='" + img_url + "' onclick='return unzoom();'/></div>\
      <div style='float: right;'><a href='#' class='icon' onclick='return unzoom();'><img src='" + static_url + "img/unzoom_icon.png' height='22'/></a></div>\
      <div class='photo_title' style='width:" + width + "px'>" + title + "</div>\
    </div>";
    $("#alt_container").html(img_html);
    $("#alt_container").show();
    return false;
}

function unzoom(img) {
    $("#alt_container").hide();
    $("#object_content").show();
    $("#col_func").show();
    return false;
}

function zoom_table(tbl_url) {
    $.get(tbl_url, function(data) {
        $("#col_func").hide();
        $("#object_content").hide();
        data = "<div style=''><a href='#' class='icon' onclick='return unzoom();'><img src='" + static_url + "img/unzoom_icon.png' height='22'/></a></div>" + data;
        $("#alt_container").html(data);
        $("#alt_container").show();
        $(".tabular").tablesorter();
    });
    return false;
}

/* Cookie functions, copied from CMFPlone */
function createCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    } else {
        expires = "";
    }
    document.cookie = name+"="+escape(value)+expires+"; SameSite=Lax; path=/;";
};

function readCookie(name, default_val) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') {
            c = c.substring(1,c.length);
        }
        if (c.indexOf(nameEQ) == 0) {
            return unescape(c.substring(nameEQ.length,c.length));
        }
    }
    return default_val;
};

function setDisplayByInitial(node) {
    node.querySelectorAll('.display-by').forEach(select => {
        const pageSize = readCookie('BGNPageSize', 30);
        select.value = pageSize;
        select.addEventListener('change', (ev) => {
            createCookie('BGNPageSize', select.options[select.selectedIndex].value, 1);
            document.location.reload()
        });
    });
}

function setPaginationClicks(node) {
    node.querySelectorAll('a.paginate').forEach(link => {
        link.addEventListener('click', (ev) => {
            ev.preventDefault();
            const a = ev.currentTarget;
            get_page(a.closest('.object_block_navigation'), a.dataset.page);
        });
    });
}

document.addEventListener('DOMContentLoaded', (event) => {
    setDisplayByInitial(document);
    setPaginationClicks(document);
});
