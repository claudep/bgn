/* var T_options, N_options, etc. are defined in search.html template */
function onAttrChange() {
    var filter_num = this.id.split('_',3)[2]; // this.id = id_choice_1
    if (this.value == "") {
        document.getElementById("id_oper_"+filter_num).options.length = 0;
        return;
    }
    var attr_type = this.value.split('_')[0];
    var attr_name = this.value.substring(this.value.indexOf('_')+1);
    populateOper(document.getElementById("id_oper_"+filter_num), eval(attr_type+'_options'));
    if (attr_type == 'V') {
        // Populate Vocab select menu
        $.getJSON(getvocab_url, {'attr_name': attr_name}, function(data) {
            vel = document.getElementById("id_vvalue_"+filter_num);
            vel.options.length = 0;
            $.each(data, function() {vel.options[vel.options.length] = new Option(this.name, this.id);});
        });
        $("#id_value_"+filter_num).hide();
        $("#id_value_"+filter_num).val('');
        $("#id_vvalue_"+filter_num).show();
    } else if (attr_type == 'B') {
        vel = document.getElementById("id_vvalue_"+filter_num);
        vel.options.length = 0;
        vel.options[vel.options.length] = new Option("Vrai", "1");
        vel.options[vel.options.length] = new Option("Faux", "0");
        $("#id_value_"+filter_num).hide();
        $("#id_value_"+filter_num).val('');
        $("#id_vvalue_"+filter_num).show();
    } else {
        $("#id_vvalue_"+filter_num).hide();
        $("#id_value_"+filter_num).show();
    }
}

function populateOper(el, items) {
    el.options.length = 0;
    $.each(items, function () {
        el.options[el.options.length] = new Option(this.name, this.value);
    });
}

function on_objtype_changed() {
    // get new attribute list
    $.get("/search/add_criterion/"+$("#id_object_type").val()+"/999/", function(data) {
        var cont = "?";
        // First pass to detect unvalid attributes and ask for confirm or revert
        $('.attr_select').each(function(i, el) {
            if (cont == "?" && el.value != "" && data.indexOf('value="'+el.value+'"') == -1) {
                rep = confirm('Some chosen attributes in the form are not valid for the new object type. ' +
                     'Do you want to continue (unvalid criteria will be removed)?');
                if (!rep) cont = "no";
                else cont = "yes";
            }
        });
        if (cont == "no") {
            // Revert select change
            $("#id_object_type").val($("#id_object_type").data('current'));
            return false;
        }
        // Second pass to replace criteria by new widgets 
        $('.attr_select').each(function(i, el) {
            if (data.indexOf('value="'+el.value+'"') == -1) {
                // Replace or remove
                if ($(".search_crit").length < 2) {
                    var parent_div = $(el).parent();
                    parent_div.replaceWith(data);
                    $(".attr_select").change(onAttrChange);
                } else $(el).parent().remove();
            } else {
                // Replace element and restore previous values if any
                var parent_div = $(el).parent();
                var saved = [];
                $(el).parent().children().filter(":input").each(function(idx, el) {saved[idx]=el.value});
                var new_div = $(data);
                parent_div.replaceWith(new_div);
                $(".attr_select").change(onAttrChange);
                if (saved[0] != "") {
                    new_div.children().filter(":input").each(function(idx, el) {
                        $(el).val(saved[idx]);
                        if (idx == 0) $(el).change();
                    });
                }
            }
        });
        $("#id_object_type").data('current', $("#id_object_type").val());
    });
}

/* Add a new search criterion line */
function add_criterion(lk) {
    // criterion_numb is defined in search.html
    $.ajax({
        type: "GET",
        url: "/search/add_criterion/"+criterion_numb+"/",
        data: "",
        success: function(data) {
            lk.parent().before(data);
            criterion_numb += 1;
            $(".attr_select").change(onAttrChange);
        },
        error: function (xhr, ajaxOptions, thrownError){
            alert(xhr.responseText);
        }
    });
    return false;
}

function del_criterion(lk) {
    lk.parent().parent().remove();
    return false;
}

/* Selected and Unselected all checkbox */
function selectedAll(ref, name) {
    var form = ref;

    while (form.parentNode && form.nodeName.toLowerCase() != 'form') {
        form = form.parentNode;
    }

    var elements = form.getElementsByTagName('input');

    for (var i=0; i < elements.length; i++) {
        if (elements[i].type == 'checkbox' && elements[i].name == name) {
            elements[i].checked = ref.checked;
        }
    }
}
    
/* Show link for export data */
function showLinkExport() {
   divInfo = document.getElementById('object_block_link_data');
   
   if (divInfo.style.display == 'none') {
       divInfo.style.display = 'block';
   }
   else {
       divInfo.style.display = 'none';
   }
}

$(document).ready(function() {
  $(".attr_select").change(onAttrChange);
  const objtypeInput = $("#id_object_type");
  objtypeInput.change(on_objtype_changed);
  objtypeInput.data('current', objtypeInput.val());
  if (objtypeInput.length && objtypeInput[0].length < 2) $("#otype_select").hide();
});

