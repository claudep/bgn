var export_window;

function open_window_export() {

    var top = (screen.height-400)/2;
    var left = (screen.width-700)/2;
    var size = "top=" + top + ", left=" + left + ",width=" + 700 + ", height=" + 400 +",";
    var opts = "menubar=no, scrollbars=no, statusbar=no";
    var chemin = "choix_attrs_export.html";

    export_window = window.open(chemin, "", size + opts);
}

function attributExport(form) {

    var attrsExp = new Array(0);
    var attrChoix = form['option'];
 
    for (var nItem = 0; nItem < attrChoix.length; nItem++) {

        if (attrChoix[nItem].checked){
            attrsExp.push(attrChoix[nItem].value);
        }
    }

    if (attrsExp.length == 0) {
        alert("Désole, vous n'avez pas choisi des attributs pour réaliser l'exportation!");
    } else {
        views.export_data(attrsExp);
        alert("Data exportation ... !");
    }
    
}

function close_window_export() {
    export_window = window.close();
}

function read_attributs_export() {
	$.ajax({
		type:"POST",
		url: "choix_attributs",
		dataType: "json",
		traditional: true,
		data : {'list_attributs': JSON.stringify(attributs)},
		success: function(dataO["HTTPRESPONSE"]);
		}
	});
}

