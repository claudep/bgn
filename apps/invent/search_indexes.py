from django.contrib.auth.models import AnonymousUser
from django.db.models import signals

from haystack import indexes

from invent.models import BaseObject, ObjectValue


class MyRealTimeSearchIndex(indexes.SearchIndex):
    """ Custom Search index so as when a Value change, the BaseObject is indexed """
    def update_object(self, instance, **kwargs):
        if isinstance(instance, ObjectValue):
            self.update_object(instance.obj, **kwargs)
        else:
            super(MyRealTimeSearchIndex, self).update_object(instance, **kwargs)

    def _setup_save(self, model):
        signals.post_save.connect(self.update_object, sender=model)
        if model == BaseObject:
            signals.post_save.connect(self.update_object, sender=ObjectValue)


class ObjectIndex(MyRealTimeSearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True)
    public = indexes.BooleanField()
    otype  = indexes.CharField(model_attr="obj_type__code")

    def get_model(self):
        return BaseObject

    def prepare_text(self, obj):
        # limit words as too long words (like dna sequences) will make the indexing crash.
        values = " ".join([
            ' '.join([t[:230] for t in str(v.get_value()).split()])
            for v in obj.get_values()
        ])
        return "%s %s %s" % (obj.title, obj.description, values)

    def prepare_public(self, obj):
        return obj.can_view(AnonymousUser())
