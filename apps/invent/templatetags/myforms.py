from django import template
from django.utils.safestring import mark_safe

register = template.Library()

@register.filter
def get_label(value):
    """ This filter returns the label tag and handle the required attribute """
    attrs = " class='optional'"
    content = ""
    if hasattr(value, 'field'):
        if value.field.required:
            attrs = " class='required'"
        content = u'<label for="%s"%s>%s</label>' % (value.auto_id, attrs, value.label)
    return mark_safe(content)

@register.filter
def perm_delete(form, field_id):
    return form['perm_del_%d' % field_id]
