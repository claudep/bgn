from django import template
from django.template.defaultfilters import truncatewords as truncate_words
from django.utils.html import strip_tags
from django.utils.safestring import mark_safe
from django.utils.translation import gettext as _

from invent.models import BaseObject

register = template.Library()

@register.filter
def resume(obj, style):
    """ Short description of object, e.g. used in search results
        style: short  = <a href="link">title</a>
               middle = <a href="link">title</a> in <a href="link">parent</a>
               long   = <a href="link">title</a> description 15 words... in <a href="link">parent</a>
    """
    res = u"""<a href="%s">%s</a>""" % (obj.get_absolute_url(), obj.get_title())
    if style == 'long' and obj.description:
        res += " " + truncate_words(strip_tags(obj.description), 15)
    if style != 'short' and obj.parent:
        res += _(', <i>in <a href="%(url)s">%(txt)s</a></i>') % {
            'url': obj.parent.get_absolute_url(), 'txt': obj.parent.get_title()}
    return mark_safe(res)

@register.inclusion_tag('invent/home_block.html')
def block_by_id(obj_id):
    try:
        bloc = BaseObject.objects.get(pk = int(obj_id))
    except BaseObject.DoesNotExist:
        return {'bloc': "Bloc not found"}
    return {'bloc': bloc}

@register.filter
def can_contain(obj, objtype_code):
    return obj.obj_type.can_contain.filter(code=objtype_code).exists()


@register.filter
def subtract(value, arg):
    return value - arg
