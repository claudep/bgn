from pathlib import Path

from django.conf import settings
from django.contrib.auth.models import User
from django.utils.translation import gettext as _

from invent.models import ObjectType, BaseObject, ObjectValue
from invent.tests.base import BaseTestCase

from imports.models import Importer, SavedFile

file_base = Path(__file__).parent


class ImportTestCase(BaseTestCase):
    fixtures = ['basedata.json']
    # To produce a fresh basedata.json:
    # In local_settings.py, uncomment test DATABASE settings
    # Create the test database (psql -U bcis) : CREATE DATABASE bcis_test OWNER=bcis;
    # Run python manage.py syncdb (do not create superuser) followed by python manage.py load-data
    # Run python manage.py dumpdata invent --indent 2 > apps/invent/fixtures/basedata.json

    def test_import_ods(self):
        before_len = len(self.inventory)
        res = Importer().import_file(file_base / "odsfile.ods", self.user, self.inventory, imp_type='create_acc')
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 2, 'num2': 2})
        self.assertEqual(before_len + 2, len(self.inventory))
        # Test the INSTCODE_REMARK pseudo attribute is imported as comments
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        obj2 = ObjectValue.objects.get(value_text='124.35').obj
        self.assertEqual(obj2.get_value_for('INSTCODE', as_object=True).comments, "Équivalent à AUS006")
        # Test that the imported file has been archived
        self.assertEqual(SavedFile.objects.filter(user=self.user).count(), 1)
        # https://github.com/zbohm/python-ooolib/issues/2
        #self.assertEqual(obj1.get_value_for('REMARKS'), "Contenu\nsur deux lignes")

    def test_update_ods(self):
        # Import first
        res = Importer().import_file(file_base / "odsfile.ods", self.user, self.inventory, imp_type='create_acc')
        # Then update
        res = Importer().import_file(file_base / "odsfile-update.ods", self.user, self.inventory, imp_type='update_acc')
        # Assertions
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        self.assertEqual(obj1.get_value_for('ELEVATION'), 755.5)
        self.assertEqual(obj1.get_value_for('REMARKS'), None)

    def test_import_xls(self):
        before_len = len(self.inventory)
        res = Importer().import_file(file_base / "xlsfile.xlsx", self.user, self.inventory, imp_type='create_acc')
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 3, 'num2': 3})
        self.assertEqual(before_len + 2, len(self.inventory))
        obj1 = ObjectValue.objects.get(value_text='34-cx').obj
        self.assertEqual(obj1.get_value_for('REMARKS'), "Contenu\nsur deux lignes")
        self.assertEqual(obj1.get_value_for('ATTR_DATE'), '2014-12-23')
        self.assertEqual(obj1.title, '34-cx - Nom')
        obj2 = ObjectValue.objects.get(value_text='124.35').obj
        self.assertEqual(obj2.get_value_for('ELEVATION'), 600)
        self.assertEqual(obj2.title, '124.35')
        # Lot import
        self.assertEqual(obj2.lot_set.count(), 1)
        lot = obj2.lot_set.first()
        self.assertEqual(lot.designation, 'Frigo')
        self.assertEqual(lot.weight, 175)

    def test_blank_lines(self):
        """ Test file where header and data lines are not respectively 1 and 2 """
        before_len = len(self.inventory)
        res = Importer().import_file(file_base / "xlsfile_blanks.xlsx", self.user, self.inventory, imp_type='create_acc', header_line=3, data_first_line=7)
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 2, 'num2': 2})
        self.assertEqual(before_len + 2, len(self.inventory))

    def test_import_create_children(self):
        # Create parent accession
        parent_obj = self._create_object(self.spec_type, title="ZM000011 - test maïs", values={
            'ACC_NUMERO': 'ZM000011',
            'ACC_NOM': 'test maïs',
            'ELEVATION': 445,
        })
        parent_obj.parent = self.inventory
        parent_obj.save()
        res = Importer().import_file(file_base / "child_accession.xlsx", self.user, self.inventory, imp_type='create_acc')
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 1, 'num2': 1})
        parent_obj.refresh_from_db()
        self.assertEqual(len(parent_obj.get_children()), 1)
        child_obj = parent_obj.get_children()[0]
        # assert inherited attrs
        self.assertEqual(child_obj.get_value_for('ACC_NUMERO'), 'ZM000011_01')
        # inherit attrs from parent
        self.assertEqual(child_obj.get_value_for('ELEVATION'), 445)
        # but attrs specified in import file takes priority
        self.assertEqual(child_obj.get_value_for('ACC_NOM'), 'test maïs enfant')

        # Adding it a second time will increase the child suffix
        res = Importer().import_file(file_base / "child_accession.xlsx", self.user, self.inventory, imp_type='create_acc')
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 1, 'num2': 1})
        parent_obj.refresh_from_db()
        self.assertEqual(len(parent_obj.get_children()), 2)
        child_obj = parent_obj.get_children()[1]
        self.assertEqual(child_obj.get_value_for('ACC_NUMERO'), 'ZM000011_02')
        self.assertEqual(child_obj.title, 'ZM000011_02 - test maïs enfant')

        # Now adding a child of a child
        res = Importer().import_file(file_base / "child_accession2.xlsx", self.user, self.inventory, imp_type='create_acc')
        self.assertEqual(res, _("%(num1)d objects imported on %(num2)d total rows") % {'num1': 1, 'num2': 1})
        parent_obj.refresh_from_db()
        self.assertEqual(len(parent_obj.get_children()), 2)
        self.assertEqual(len(parent_obj.get_descendants()), 3)
        subchild = parent_obj.get_children()[0].get_children()[0]
        self.assertEqual(subchild.get_value_for('ACC_NUMERO'), 'ZM000011_03')
