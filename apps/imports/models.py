import datetime
import logging
import re
from collections import OrderedDict
from contextlib import nullcontext
from pathlib import Path
from time import time

from openpyxl import load_workbook
import ooolib  # package ooolib-python

from django.db import models, transaction
from django.core.files import File
from django.utils.dateparse import parse_date
from django.utils.translation import gettext as _
from django.contrib.auth.models import User

from invent.models import (
    Attribute, BaseObject, Institute, Lot, ObjectType, ObjectValue, ObjectValueFactory
)


class UnsupportedFileFormat(Exception):
    pass

class SavedFile(models.Model):
    """ This model class is used to archive imported files """
    ifile = models.FileField(upload_to='imported', verbose_name=_("File"))
    stamp = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    description = models.TextField()

    def __str__(self):
        return "%s uploaded by %s the %s" % (self.ifile, self.user, self.stamp)

    def save(self, tempfile, **kwargs):
        """ Save tempfile (an ImportedFile instance) """
        with open(tempfile.file_path, mode='rb') as fh:
            self.ifile.save(tempfile.orig_name, File(fh), False)
        super().save(**kwargs)


class ImportedFile:
    """ Class to abstract an imported file, support xls and ods files """

    def __init__(self, datafile, sheet_index=0, sheet_name=None, header_line=1, data_first_line=2):
        """ datafile may be a file path or a django (Uploaded)File object """
        
        self.header_line = header_line
        self.data_first_line = data_first_line
        if isinstance(datafile, (str, Path)):
            self.file_path = datafile
            self.orig_name = Path(datafile).name
        else:
            self.file_path = datafile.temporary_file_path()
            self.orig_name = datafile.name
        self.format = self._sniff_format(datafile)
        if self.format in ("xls", "xlsx"):
            self.book = load_workbook(self.file_path, read_only=True)
            if sheet_name:
                self.data_sheet = self.book[sheet_name]
            else:
                self.data_sheet = self.book.active
            self.nrows = self.data_sheet.max_row
            self.ncols = self.data_sheet.max_column

        elif self.format == "ods":
            self.book = ooolib.Calc(opendoc=self.file_path) 
            self.book.set_sheet_index(sheet_index)
            self.data_sheet = self.book
            
            (self.ncols, self.nrows) = self.data_sheet.get_sheet_dimensions()
            
        elif self.format == "csv": 
            sourceFile = open(self.file_path, "r")
            self.book = sourceFile
            #self.book.set_sheet_index(sheet_index)
            
            # Calcul lines numbers in csv file
            numLin = 0
            for line in sourceFile:
                if numLin == 0:
                    numCol = len(line.rstrip('\n\r').split(";"))                    
                numLin += 1
            sourceFile.close()
                
            self.nrows = numLin
            self.ncols = numCol 

        else:
            raise UnsupportedFileFormat("Unknown file extension '%s'" % self.format)        

        self._headers = []
        
    @classmethod
    def _sniff_format(cls, dfile):
        """ dfile may be a file path or a django (Uploaded)File object """
        if isinstance(dfile, str):
            fmt = dfile.rsplit(".", 1)[1]
        elif isinstance(dfile, Path):
            fmt = dfile.suffix.strip('.')
        else:
            if "opendocument.spreadsheet" in dfile.content_type:
                fmt = "ods"
            elif "excel" in dfile.content_type or 'spreadsheetml' in dfile.content_type:
                fmt = "xls"
            elif  "text/csv" in dfile.content_type:  
                fmt = "csv"
            else:
                raise UnsupportedFileFormat("Unknown file type '%s'" % dfile.content_type)
        return fmt

    def get_headers(self):
        """ Extract headers for the file (according to self.header_line) """
        
        if not self._headers:
            if self.format in ("xls", "xlsx"):
                row = self.data_sheet[self.header_line]
                for i, cell in enumerate(row):
                    h_value = cell.value.strip() if cell.value else cell.value
                    if h_value:
                        self._headers.append(h_value)
                    else:
                        logging.warning("Empty header in %s" % self.file_path)
                        self._headers.append("--empty--")

            if self.format == "ods":
                for i in range(self.ncols):
                    cell_value = self.data_sheet.get_cell_value(i+1, self.header_line)
                    if cell_value:
                        self._headers.append(self.data_sheet.get_cell_value(i+1, self.header_line)[1].strip())
                    else:
                        logging.warning("Empty header in %s" % self.file_path)
                        self._headers.append("--empty--")
                        
            if self.format == "csv":
                sourceFile = open(self.file_path, "r")
                allRows = sourceFile.readlines()
                row =  allRows[self.header_line-1].split(";")
                
                for i in range(self.ncols):
                    h_value = row[i].strip()  
                    
                    if h_value:
                        self._headers.append(h_value)
                    else:
                        logging.warn("Empty header in %s" % self.file_path)
                        self._headers.append("--empty--")

                sourceFile.close()

        return self._headers

    @property
    def num_data_lines(self):
        return self.nrows - (self.data_first_line - 1)

    def iterate(self):
        empty_lines = 0
        for rx in range(self.data_first_line - 1, self.nrows):
            data_dict = self.get_row_dict(rx)
            if all(v in (None, '') for v in data_dict.values()):
                # Ignoring completely empty lines
                empty_lines += 1
                if empty_lines > 10:
                    return
                continue
            else:
                empty_lines = 0
            yield rx, data_dict

    def get_row_dict(self, idx):
        row_dict = OrderedDict()
        if not self._headers:
            self._headers = self.get_headers()
        
        if self.format in ("xls", "xlsx"):
            row = self.data_sheet[idx + 1]
            for i, cell in enumerate(row):
                if type(cell.value) == float and str(cell.value).endswith('.0'):
                    row_dict[self._headers[i]] = int(cell.value)
                else:
                    row_dict[self._headers[i]] = cell.value

        if self.format == "ods":
            for i in range(self.ncols):
                cell_value = self.data_sheet.get_cell_value(i+1, idx+1)
                
                if cell_value and cell_value[0] == 'formula' and cell_value[1]:
                    raise ValueError(_("The ODS file contains formula. Please convert them to raw values before importing the file."), cell_value)
                row_dict[self._headers[i]] = cell_value and cell_value[1].replace("<text:line-break/>", "\n")
                
        if self.format == "csv":
            sourceFile = open(self.file_path, "r")
            allRows = sourceFile.readlines()
            row =  allRows[idx].split(";")
            
            for ind in range(self.ncols):
                if (row[ind].isdigit()):
                    row_dict[self._headers[ind]] = row[ind]
                else:
                    row_dict[self._headers[ind]] = row[ind].decode('utf8') 
    
            sourceFile.close()
            
        return row_dict


def check_value(attr, value):
    """ Check if value is valid for attr, and check if multiple values
        Alway return a list """
      
    if attr.datatype == "integer":
        try:
            v = int(value)
            return [v]
        except:
            if isinstance(value, (datetime.date, datetime.datetime)):
                raise ValueError("L’attribut %s s’attend à recevoir un nombre entier" % attr.code)
            # Try to separate value in multiple integer values
            for sep in [',', '/', ';', '|', u'¦']:
                if sep in value:
                    vals = value.split(sep)
                    try:
                        # Transform "2006, 2007" to [2006, 2007]
                        vals = [int(v.strip()) for v in value.split(sep)]
                        return vals
                    except:
                        raise ValueError("Unable to transform '%s' to a list of integers")
    elif attr.datatype == "qset" and 'Institute' in attr.queryset:
        # Hardcoded for institute attributes
        try:
            value = Institute.objects.get(code__iexact=value[:6])
        except Institute.DoesNotExist:
            raise ValueError(f"Le code institut «{value[:6]}» est introuvable.")
    return [value]


def set_attributes_from_line(new_obj, data_dict, field_mapping, title_fields):
    """ Utility function to populate attribute of new_obj with content from data_dict
         - data_dict is a result from get_row_dict
         - field_mapping is a mapping between imported file column names and attribute names
         - title_fields is a list of column names used to form the object title
    """
            
    for key, value in data_dict.items():
        if not value:
            continue
        attr_name = field_mapping.get(key, None)
        if attr_name is None:
            continue

        if isinstance(value, float) and value == int(value):
            value = int(value)
        try:
            new_obj.set_value_for(attr_name, value)
        except Exception as e:
            print("Set value '%s' for attribute %s: %s" % (value, attr_name, str(e)))

        # Try to establish a title for the object
        def _get_value(col_name):
            attr_name = field_mapping.get(col_name, None)
 
            if attr_name:
                # Get prioritarily from object value (already sanitized)
                val = new_obj.get_value_for(attr_name)
            else:
                if isinstance(data_dict[col_name], float) and str(data_dict[col_name]).endswith('.0'):
                    val = str(int(data_dict[col_name]))
                else:
                    val = str(data_dict[col_name])
            return val

        title_str = u" - ".join(filter(None,
            [_get_value(col_name) for col_name in title_fields])
        )
        if title_str:
            new_obj.title = title_str
            new_obj.save()


class Importer:
    def is_remark_field(self, attr_name):
        return attr_name.endswith("_REMARK") and attr_name not in self.valid_attrs and attr_name.rsplit('_', 1)[0] in self.valid_attrs

    def import_file(self, datafile, user, parent_obj, strict_attr=False, imp_type='', header_line=1, data_first_line=2):
        self.imp_file = ImportedFile(datafile, header_line=header_line, data_first_line=data_first_line)
        self.user = user
        self.parent_obj = parent_obj
        self.update = 'update' in imp_type
        self.errors = []
        if imp_type in ['create_acc', 'update_acc']:
            return self.import_accs(strict_attr=strict_attr)
        elif imp_type == 'update_lot':
            return self.update_lots()
        else:
            raise Exception(f'{imp_type} not yet supported')

    def find_accession(self, acc_numero):
        found_accs = BaseObject.get_by_attr_value('ACC_NUMERO', acc_numero , limit_to='accession')
        if len(found_accs) > 1:
            found_accs = self.parent_obj.get_descendants().filter(obj_type__code='accession'
                ).filter(pk__in=[o.pk for o in found_accs])
        if len(found_accs) == 0:
            self.errors.append(f"Impossible de trouver une accession avec le numéro '{acc_numero}'")
            return None
        if len(found_accs) > 1:
            self.errors.append(f"Plusieurs accessions trouvées avec le numéro '{acc_numero}'")
            return None
        return found_accs[0]

    def update_lots(self):
        headers = self.imp_file.get_headers()
        create_if_not_exists = True
        current_acc = None
        update_mapping = {
            'LOT_ANNEE_RECOLTE': 'annee_recolte',
            'LOT_POIDS': 'weight',
            'LOT_POIDS_TOT': 'total_weight',
            'LOT_NB_SACHETS': 'nb_sachet',
            'LOT_EMPLACEMENT': 'phys_location',
            'LOT_CAISSE': 'box_id',
            'LOT_BLOC': 'loc_x',
            'LOT_RANGEE': 'loc_y',
            'LOT_ETAGERE': 'loc_z',
            'LOT_AFFECTATION': 'affectation',
        }
        empl_mapping = {tpl[1]: tpl[0] for tpl in Lot.PHYS_LOC_CHOICES}
        affect_mapping = {tpl[1]: tpl[0] for tpl in Lot.AFFECTATION_CHOICES}
        updated = 0
        with transaction.atomic():
            batch_update = []
            batch_create = []
            for num_line, data_dict in self.imp_file.iterate():
                if data_dict['OBJ_TYPE'] == 'accession':
                    current_acc = self.find_accession(data_dict.get('ACC_NUMERO', ""))
                if data_dict['OBJ_TYPE'] != 'lot' or current_acc is None:
                    continue
                # Update lot
                try:
                    lot = Lot.objects.get(accession=current_acc, designation=data_dict['ACC_NUMERO'])
                except Lot.DoesNotExist:
                    try:
                        lot = Lot.objects.get(accession=current_acc, designation__icontains=data_dict['ACC_NUMERO'])
                    except Lot.DoesNotExist:
                        if create_if_not_exists:
                            lot = Lot(accession=current_acc, designation=data_dict['ACC_NUMERO'])
                        else:
                            self.errors.append(
                                f"Impossible de trouver le lot {data_dict['ACC_NUMERO']} pour l’accession '{current_acc}'"
                            )
                            continue
                except Lot.MultipleObjectsReturned:
                    self.errors.append(f"Plusieurs lots correspondent à {data_dict['ACC_NUMERO']} pour l’accession '{current_acc}'")
                    continue
                for col_name, field_name in update_mapping.items():
                    if col_name == 'LOT_EMPLACEMENT':
                        setattr(lot, field_name, empl_mapping.get(data_dict[col_name], data_dict[col_name]))
                    elif col_name == 'LOT_AFFECTATION':
                        setattr(lot, field_name, affect_mapping.get(data_dict[col_name], data_dict[col_name]))
                    elif col_name in headers:
                        setattr(lot, field_name, data_dict[col_name])
                lot.full_clean()
                if lot.pk:
                    batch_update.append(lot)
                else:
                    batch_create.append(lot)
                print('.', end='', flush=True)
                updated += 1
                if updated % 100 == 0:
                    Lot.objects.bulk_update(batch_update, update_mapping.values())
                    batch_update = []
                    if batch_create:
                        Lot.objects.bulk_create(batch_create)
                        batch_create = []
            Lot.objects.bulk_update(batch_update, update_mapping.values())
            if batch_create:
                Lot.objects.bulk_create(batch_create)
            self.check_errors() 

        return _("%(num1)d objects imported on %(num2)d total rows") % {
            'num1': updated, 'num2': self.imp_file.num_data_lines
        }

    def import_accs(self, strict_attr=False):
        """ Create objects from the lines of a data file """

        headers = self.imp_file.get_headers()
        acc_content_type = ObjectType.objects.get(code='accession')
        
        if strict_attr:
            self.valid_attrs = [attr.name for attr in acc_content_type.get_attributes()]
        else:
            # Accept all attributes listed in the database
            self.valid_attrs = Attribute.all_attr_names()
            
        special_attrs = ['OBJ_TYPE', 'OBJ_HIERARCHY']
        self.valid_attrs.extend(special_attrs)
        imported = 0

        # Check header validity
        for d in headers:
            if d.upper() not in self.valid_attrs and d != "--empty--" and not d.startswith('LOT_'):
                # Accept "<attr>_remark" as a comment for the corresponding attr value
                if self.is_remark_field(d):
                    continue
                self.errors.append(_("Attribute '%s' is not valid") % d)

        if self.errors:
            raise ValueError(", ".join(self.errors))
            
        parents = { "": self.parent_obj }

        tempsTotal = 0
        context = nullcontext if (self.update or self.imp_file.nrows < 40) else BaseObject.objects.delay_mptt_updates

        # atomic slow down imports, especially for >500 lines
        with transaction.atomic(), context():
            for num_line, data_dict in self.imp_file.iterate():
                if len(self.errors) > 20:
                    self.errors.append("Too much errors, aborting")
                    break
                start = time()
                parent = self.parent_obj

                # Create object line by line
                taxo_data = {}
                acc_numero = str(data_dict.get('ACC_NUMERO', "") or "").strip()
                title = (" - ".join([str(val) for val in [acc_numero, data_dict.get('ACC_NOM', "")] if val])).strip()

                obj_type__code = (data_dict.get('OBJ_TYPE') or '').lower().strip()
                hierarchy = str(data_dict.get('OBJ_HIERARCHY', "")).strip()
                acc_parent = str(data_dict.get('ACC_PARENT') or "").strip()

                if acc_parent:
                    try:
                        parent = ObjectValue.objects.get(
                            attr__name='ACC_NUMERO', value_text=acc_parent,
                            obj__in=self.parent_obj.get_descendants().filter(obj_type__code='accession')
                        ).obj
                    except ObjectValue.DoesNotExist:
                        self.errors.append(f"Impossible de trouver l'accession parente «{acc_parent}»")
                    except ObjectValue.MultipleObjectsReturned:
                        self.errors.append(
                            f"Il existe plusieurs accessions ayant le numéro «{acc_parent}» dans cette collection."
                        )
                elif hierarchy:
                    while hierarchy.endswith('.0'):
                        hierarchy = hierarchy[0 : len(hierarchy) - len('.0')]

                    hierarchy_dot = hierarchy.rfind('.')

                    if hierarchy_dot != -1:
                        hierarchy_parent = hierarchy[0 : hierarchy_dot]
                        parent = parents.get(hierarchy_parent, parent)

                if obj_type__code in ('', 'accession') and acc_numero:
                    obj_type = acc_content_type
                    # Verify if object exists in database
                    try:
                        newacc = ObjectValue.objects.get(
                            attr__name='ACC_NUMERO', value_text=acc_numero,
                            obj__in=self.parent_obj.get_descendants().filter(obj_type__code='accession')
                        ).obj
                    except ObjectValue.DoesNotExist:
                        newacc = BaseObject(obj_type=obj_type, title=title, owner=self.user, status=parent.status, parent=parent)
                        newacc.save()
                    else:
                        if not self.update:
                            # If an accession with same acc_numero exists in same parent inventory, create a child (#5)
                            parent_acc = newacc
                            newacc = BaseObject(obj_type=obj_type, title=title, owner=self.user, status=parent.status, parent=parent_acc)
                            newacc.save()
                            self.parent_obj.refresh_from_db()
                            # ...and make it inherit all attributes from parent
                            for value in parent_acc.get_values():
                                if value.attr.name == 'ACC_NUMERO':
                                    # Ensure ACC_NUMERO is unique in collection and suffixed with _nn
                                    m = re.match(r'(.+)_(\d\d)', value.value_text)
                                    if m:
                                        base, suffix = m.groups()
                                        suffix = int(suffix)
                                    else:
                                        base, suffix = value.value_text, None
                                    is_unique = False
                                    while not is_unique:
                                        if suffix:
                                            suffix += 1
                                            value.value_text = f'{base}_{suffix:02}'
                                        else:
                                            value.value_text = f'{value.value_text}_01'
                                            suffix = 1
                                        is_unique = not ObjectValue.objects.filter(
                                            attr__name='ACC_NUMERO', value_text=value.value_text,
                                            obj__in=self.parent_obj.get_descendants().filter(obj_type__code='accession')
                                        ).exists()
                                    del data_dict['ACC_NUMERO']
                                    newacc.title = (" - ".join([val for val in [value.value_text, data_dict.get('ACC_NOM', "")] if val])).strip()
                                    newacc.save()

                                elif value.attr.name in data_dict or not value.attr.inheritable:
                                    # Will be set by value in file, no need to inherit it.
                                    continue
                                value.obj = newacc
                                value.pk = None
                                value.save()

                elif obj_type__code == 'lot':
                    try:
                        lot = Lot.objects.get(accession=parent, designation=title)
                    except Lot.DoesNotExist:
                        lot = Lot.objects.create(accession=parent, designation=title)
                    self.set_lot_from_line(lot, data_dict)
                    imported += 1
                    continue
                elif obj_type__code == 'test':
                    raise NotImplementedError("Still TODO")

                if hierarchy:
                    parents[hierarchy] = newacc

                # Add attributes
                wait_values = []
                for key, value in data_dict.items():
                    if value == "" or value is None:
                        if self.update:
                            try:
                                existing_val = ObjectValue.objects.get(obj=newacc, attr__name=key)
                                existing_val.delete()
                            except ObjectValue.DoesNotExist:
                                pass
                        continue

                    if key == 'DESCRIPTION':
                        newacc.description = value
                        newacc.save()
                        continue

                    if self.is_remark_field(key):
                        attr = Attribute.objects.get(name=key.rsplit('_', 1)[0])
                        try:
                            oval = [oval for oval in wait_values if oval.attr == attr][0]
                        except IndexError:
                            self.errors.append(
                                f"Ligne {num_line}: le champ {attr}_REMARK possède une valeur mais il n'y a pas de valeur {attr} correspondante"
                            )
                            continue
                        else:
                            oval.comments = value
                        continue

                    try:
                        attr = Attribute.objects.get(name=key)
                    except Attribute.DoesNotExist:
                        # key in special_attrs
                        continue
                    if attr.name.startswith('ACC_') and newacc.obj_type.code != 'accession':
                        continue

                    try:
                        values = check_value(attr, value)
                    except ValueError as err:
                        self.errors.append("Line %(num)d: %(err)s" % {'num': num_line, 'err': err})
                        continue

                    for v in values:
                        newval = None
                        if self.update:
                            try:
                                newval = ObjectValue.objects.get(obj=newacc, attr=attr)
                            except ObjectValue.DoesNotExist:
                                pass

                        if not newval:
                            newval = ObjectValueFactory(obj=newacc, attr=attr)
                        try:
                            if (v != '\n') :
                                newval.set_value({'value_%s' % key: v}, save=False)
                                wait_values.append(newval)
                        except Exception as e:
                            self.errors.append(
                                "Line %(num)d: unable to set value '%(value)s' for field '%(attrib)s' (error was: '%(error)s')" % {
                                    'num': num_line, 'value': v, 'attrib': key, 'error': str(e)
                                }
                            )
                            continue
                ObjectValue.objects.bulk_update(
                    [v for v in wait_values if v.pk is not None],
                    ['value_text', 'value_int', 'value_float', 'value_voc', 'comments']
                )
                ObjectValue.objects.bulk_create([v for v in wait_values if v.pk is None])

                elapsed = time() - start
                imported += 1
                logging.warning("Imported object %d/%d (%f)" % (imported, self.imp_file.nrows-1, elapsed))

            # Archive imported file
            saved = SavedFile(user=self.user, description="user-imported file containing objects (attached to BaseObject %s)" % self.parent_obj.uuid)
            saved.save(self.imp_file)
            self.check_errors()  # Has to happen inside atomic to rollback the transaction

        return _("%(num1)d objects imported on %(num2)d total rows") % {
            'num1': imported, 'num2': self.imp_file.num_data_lines
        }

    def check_errors(self):
        if self.errors:
            msg = "\n".join(["Des erreurs sont survenues durant l’importation:"] + self.errors)
            raise Exception(msg)

    def set_lot_from_line(self, lot, data_dict):
        errors = []
        for key, value in data_dict.items():
            if not key.startswith('LOT_'):
                continue
            val_empty = value == "" or value is None
            if key not in Lot.header_mapping:
                raise ValueError(f"La colonne {key} n'est pas prise en charge")
            field_name = Lot.header_mapping[key]
            field = Lot._meta.get_field(field_name)
            if field.choices and not val_empty and value not in dict(field.choices):
                # Try to match the value by its verbose name
                new_value = {v: k for k, v in field.choices}.get(value)
                if new_value:
                    value = new_value
                else:
                    errors.append(f"La valeur «{value}» ne semble pas être acceptée pour la colonne {key}")
                    continue
            if field_name == 'instcode' and not val_empty:
                try:
                    value = Institute.objects.get(code=value[:6])
                except Institute.DoesNotExist:
                    errors.append(f"Le code institut «{value}» n'est pas connu du système")
            elif field.get_internal_type() == "BooleanField":
                value = True if value in ('ja', 'oui', 'yes', 'vrai', 'true', '1', 1) else False
            elif value is None and field.get_internal_type() in {"CharField", "TextField"}:
                value = ""
            elif not val_empty and field.get_internal_type() == "DateField":
                try:
                    value = parse_date(value)
                except (ValueError, TypeError):
                    errors.append(f"La valeur «{value}» ne semble pas être une date valide")
                    continue
            setattr(lot, field_name, value)
        if errors:
            self.errors.extend(errors)
        else:
            lot.save()
