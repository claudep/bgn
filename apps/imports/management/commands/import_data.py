from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from invent.models import BaseObject
from imports.models import Importer


class Command(BaseCommand):
    """Equivalent to import form."""
    def add_arguments(self, parser):
        parser.add_argument('file')
        parser.add_argument('--type', required=True)
        parser.add_argument('--user')
        parser.add_argument('--invent-id', required=True)

    def handle(self, *args, **options):
        imp_file = options['file']
        imp_type = options['type']
        if options['user']:
            imp_user = User.objects.get(username=options['user'])
        else:
            imp_user = None
        parent_obj = BaseObject.objects.get(pk=options['invent_id'])
        return Importer().import_file(imp_file, imp_user, parent_obj, imp_type=imp_type)
