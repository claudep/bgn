# Django settings for bcis project.
from pathlib import Path
from django.urls import reverse_lazy

DEBUG = False
BASE_DIR = Path(__file__).resolve().parent.parent

ADMINS = (
    ('Claude Paroz', 'claude@2xlibre.net'),
)

MANAGERS = ADMINS
SERVER_EMAIL = "server@bcis.ch"

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME':   'bgn',
        'USER':   'bgn',
        'PASSWORD': '123456',
    }
}
DEFAULT_AUTO_FIELD = 'django.db.models.AutoField'

TIME_ZONE = 'Europe/Zurich'
USE_TZ = True

LANGUAGE_CODE = 'fr'
LANGUAGES = (
    ('fr', 'French'),
)
DEFAULT_LANGUAGE = 1 # English

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = True

# Absolute path to the directory that holds media.
MEDIA_ROOT = BASE_DIR / 'media'

# URL that handles the media served from MEDIA_ROOT.
MEDIA_URL = '/media/'

STATIC_ROOT = BASE_DIR / 'static'
STATIC_URL = '/static/'

MIDDLEWARE = [
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'common.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.static',
                'django.contrib.messages.context_processors.messages',
                'django.template.context_processors.request',
            ],
            'builtins': [
                'django.templatetags.static',
            ],
        },
    },
]

INSTALLED_APPS = [
    'django.contrib.auth',
    'django.contrib.admin',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    # External apps
    'mptt',
    'sorl.thumbnail',
    'haystack',
    'tinymce',
    # BCIS apps
    'proj',
    'permission',
    'invent',
    'imports',
    'taxo',
    'task',
]

LOGIN_URL = "/login/"
LOGIN_REDIRECT_URL = "/"

LOCALE_PATHS = [BASE_DIR / 'locale']

STORAGES = {
    "default": {
        "BACKEND": "django.core.files.storage.FileSystemStorage",
    },
    "staticfiles": {
        "BACKEND": "django.contrib.staticfiles.storage.ManifestStaticFilesStorage",
    },
}

MESSAGE_STORAGE = 'django.contrib.messages.storage.fallback.FallbackStorage'

ABSOLUTE_URL_OVERRIDES = {
    'auth.user': lambda o: reverse_lazy('profile', args=[o.username]),
}

# Force all uploaded files to be written on disk, so as they can be opened by xlrd module
FILE_UPLOAD_HANDLERS = ("django.core.files.uploadhandler.TemporaryFileUploadHandler",)

CREATE_TAXONOMY_ON_IMPORT = False
ALLOW_TABULAR_VIEW = 'admin' # all, auth, admin
WEB_STATS_URL = "/awstats/awstats.pl?config=bgn"

TINYMCE_DEFAULT_CONFIG = {
    'theme': 'silver',
    'menubar': False,
    'plugins': 'link code',
    'toolbar': 'bold italic | link unlink | undo redo | code',
    'toolbar_location': 'bottom',
    'statusbar': False,
    #'icons': 'small',
}
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'xapian_backend.XapianEngine',
        'PATH': '/var/cache/xapian/bgn',
        'INCLUDE_SPELLING': True,
        'BATCH_SIZE': 100,
    }
}
HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'
