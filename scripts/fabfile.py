# Examples:
#   $ fab [-H www.bcis.ch] clone-remote-db hortus

import getpass
from fabric import task
from invoke import Context, Exit

MAIN_HOST = 'agroscope'
VIRTUALENV_DIR = '/var/virtualenvs/bgn/bin/activate'


@task(hosts=[MAIN_HOST])
def clone_remote_db(conn, dbname='bgn'):
    """ Dump a remote database and load it locally """
    local = Context()

    def exist_local_db(db):
        cmd = local.run('psql --list', hide=True)
        db_list = [line.split()[0] for line in cmd.stdout.split('\n')[3:-3]]
        return db in db_list

    def exist_username(db):
        cmd = local.run('psql -d postgres -c "select usename from pg_user;"', hide=True)
        user_list = [line.strip() for line in cmd.stdout.split('\n')][2:-3]
        return db in user_list

    pdict = {'db': dbname}
    conn.config['sudo']['password'] = getpass.getpass("Enter the sudo password (on the server):")
    conn.run('touch %(db)s.backup && chmod o+rw %(db)s.backup' % pdict)
    conn.sudo('pg_dump --no-owner -Fc -b -f "%(db)s.backup" %(db)s' % pdict, user='postgres')
    conn.get('%(db)s.backup' % pdict)

    if exist_local_db(dbname):
        rep = input('A local database named "%s" already exists. Overwrite? (y/n)' % dbname)
        if rep == 'y':
            local.run('psql -d postgres -c "DROP DATABASE %(db)s;"'  % {'db': dbname})
        else:
            raise Exit("Database not copied")

    if exist_username(dbname):
        pdict['owner'] = dbname
    else:
        pdict['owner'] = "claude"
    local.run('psql -d postgres -c "CREATE DATABASE %(db)s OWNER=%(owner)s;"' % pdict)
    local.run('pg_restore -U %(owner)s -d %(db)s --no-owner "%(db)s.backup"' % pdict)


# ex: fab [-H www.bcis.ch] set-server-maintenance on --project=proj_name
@task(hosts=[MAIN_HOST])
def set_server_maintenance(conn, switch):
    """ Pass the server into or out of maintenance mode
        switch is "on" or "off" """
    with conn.cd('/var/www/bgn'):
        if switch == "on":
            conn.run('sed -i -e "s/UPGRADING = False/UPGRADING = True/" common/wsgi.py')
        elif switch == "off":
            conn.run('sed -i -e "s/UPGRADING = True/UPGRADING = False/" common/wsgi.py')
        else:
            raise Exit("The parameter should be either on or off")
    

# ex: fab deploy --project=proj_name
@task(hosts=[MAIN_HOST])
def deploy(conn):
    """ Apply code change """
    set_server_maintenance(conn, 'on')
    proj_name = 'bgn'
    with conn.cd('/var/www/%s' % proj_name):
        conn.run('git stash && git pull && git stash pop')
        with conn.prefix('source %s' % VIRTUALENV_DIR):
            conn.run('python manage.py migrate')
            conn.run('python manage.py collectstatic --noinput')
            conn.run('python manage.py compilemessages')
    set_server_maintenance(conn, 'off')


def _check_folders_exist(proj_name):
    """ Check that folders where the webserver might be writing files is writable """
    media_folders = ['attachments', 'icons', 'photos', 'cache', 'blasts']
    for folder in media_folders:
        path = '/var/www/%s/media/%s' % (proj_name, folder)
        if not files.exists(path):
            conn.run('mkdir -p %s' % path)
            conn.sudo('chown www-data %s' % path)
            conn.sudo('chmod g+w %s' % path)

