# Script to apply multiple command to a Postgres database
# Here : change ownership of all tables of a database
psql -c "\dt" -A -t -F " " mycoscope | awk '{ print $2; }' | while read ma_table
do
  echo "ALTER TABLE $ma_table OWNER TO bcis;"
done | psql mycoscope
