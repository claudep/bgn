# Import an XLS file and create objects
from imports.models import ImportedFile
from invent.models import BaseObject, ObjectType
from django.contrib.auth.models import User
from django.db import transaction
from django.contrib.gis.geos import Point

@transaction.commit_on_success
def import_file(file_path):
    imp_file = ImportedFile(file_path, sheet_name="Tabelle1")
    user = User.objects.get(username='claudep')
    new_inv = BaseObject.objects.create(
                obj_type=ObjectType.objects.get(code="inventory"),
                status="public", owner=user, title=u"Importation observations")
    obj_type = ObjectType.objects.get(code="observation")
    
    for rx in range(1, imp_file.nrows):
        data_dict = imp_file.get_row_dict(rx)
        new_obj = BaseObject.objects.create(obj_type=obj_type, status="public", parent=new_inv, owner=new_inv.owner)
        new_obj.set_value_for('SPECIES_CNAME', data_dict['NameFz'])
        new_obj.set_value_for('OBSERV_QUANT', data_dict['Anzahl'] or 1)
        new_obj.set_value_for('OBSERV_DATE', int(data_dict['Jahr']))
        if data_dict['RoteListe']:
            new_obj.set_value_for('STATUT_LR', data_dict['RoteListe'])
        new_obj.point = Point(int(data_dict['KoordX'])*1000, int(data_dict['KoordY'])*1000, srid=21781)
        new_obj.title = u"%s - %s" % (data_dict['NameFz'], int(data_dict['Jahr']))
        new_obj.save()

import_file('<path>')


# Mass import of images
import re
from pathlib import Path
from invent.forms import AddPhotoForm
from invent.models import BaseObject
from django.core.files import File

photo_dir = Path('/home/claude/haricotsrame')
ancestor_invent_id = 5094 # This allows disambiguation of duplicate acc nos, if needed (can be None)
files = sorted(photo_dir.glob('*.jpg'))
imported = 0
unique_images = True # If accessions should only have one image

for file_path in files:
    #acc_no = file_path.stem  # .lstrip('0')
    acc_no = file_path.stem.split()[0]  # .lstrip('0')
    #acc_no = re.split('[_\s]', acc_no)[0]
    #acc_no = acc_no.replace('-', '')
    acc_list = BaseObject.get_by_attr_value('ACC_NUMERO', acc_no, limit_to='accession')
    if len(acc_list) > 1 and ancestor_invent_id:
        acc_list = [acc for acc in acc_list if ancestor_invent_id in [anc.pk for anc in acc.get_ancestors()]]
    if len(acc_list) != 1:
        print(f"Unable to find a unique accession with ACC_NUMERO={acc_no}")
        continue
    acc = acc_list[0]
    if unique_images and acc.get_images().count() > 0:
        print(f"Acc {acc_no} has already some images, ignoring.")
        continue
    with file_path.open(mode='rb') as fh:
        #title = file_path.stem
        title = 'Photo de %s' % acc_no
        form = AddPhotoForm(data={'title': title}, files={'image': File(fh, name=file_path.name)})
        if form.is_valid():
            form.save(parent=acc)
            # surrogate escape is due to accents from Win systems
            print(f"{file_path.name.encode('utf-8', 'surrogateescape')} saved")
            imported += 1
            file_path.unlink()
        else:
            print("Error saving file file_path: %s" % form.errors)
print(f"{imported} images imported")


# Check duplicates ACC_NUMERO in inventories
from collections import Counter
from invent.models import BaseObject, ObjectValue

def check_duplicates(inv):
    acc_numbs = list(
        ObjectValue.objects.filter(
            obj__in=inv.get_descendants().filter(obj_type__code='accession'), attr__name='ACC_NUMERO'
        ).values_list('value_text', flat=True)
    )
    count = Counter(acc_numbs)
    dups = [key for key, val in count.items() if val > 1]
    return dups

for obj in BaseObject.objects.filter(obj_type__code='inventory'):
    print(f"Checking duplicates for '{obj}':")
    dups = check_duplicates(obj)
    print(dups)


# Move donorname values to equivalent donorcodes
from invent.models import Attribute, Institute, ObjectValue
for val in ObjectValue.objects.filter(attr__name='ACC_DONORNAME', value_text__startswith='Biosem (CHE'):
    val.obj.set_value_for('ACC_DONORCODE', Institute.objects.get(code='CHE098').pk)
    val.delete()
