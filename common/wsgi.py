import os
import sys

UPGRADING = False

project_dir = "/".join(os.path.abspath(os.path.dirname(__file__)).split('/')[:-1])
parent_dir = "/".join(project_dir.split('/')[:-1])
app_dir = os.path.join(project_dir, 'apps')

sys.path.append(project_dir)
sys.path.append(parent_dir)
sys.path.append(app_dir)

def upgrade_in_progress(environ, start_response):
    upgrade_file = os.path.join(project_dir, 'templates', '503.html')
    if os.path.exists(upgrade_file):
        response_headers = [('Content-type','text/html')]
        response = open(upgrade_file, 'rb').read()
    else:
        response_headers = [('Content-type','text/plain')]
        response = b"This site is in maintenance mode, please come back in some minutes."

    if environ['REQUEST_METHOD'] == 'GET':
        status = '200 OK'
    else:
        status = '403 Forbidden'
    start_response(status, response_headers)
    return [response]

if UPGRADING:
    application = upgrade_in_progress
else:
    os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
    from django.core.wsgi import get_wsgi_application
    application = get_wsgi_application()

