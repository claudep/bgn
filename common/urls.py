from django.conf import settings
from django.contrib import admin
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path
from django.utils import timezone
from django.views.decorators.http import last_modified
from django.views.generic import TemplateView
from django.views.i18n import JavaScriptCatalog
from django.views.static import serve

from haystack.views import search_view_factory
from invent.forms import ImageSearchForm
from invent import views
from proj import views as proj_views

handler500 = 'proj.views.server_error'
last_modified_date = timezone.now()

urlpatterns = [
    path('admin/', admin.site.urls),
    path('taxo/',  include('apps.taxo.urls')),
    path('task/', include('task.urls')),
    path('tinymce/', include('tinymce.urls')),
    path('jsi18n/', last_modified(lambda req, **kw: last_modified_date)(JavaScriptCatalog.as_view()),
        name='javascript_catalog'),

    path('login/', auth_views.LoginView.as_view(template_name='proj/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='/'), name='logout'),
    re_path(r'^users/(?P<username>[\w@\.\-]+)/$', proj_views.profile, name='profile'),
    re_path(r'^setlang/(?P<lang>\w+)/$', proj_views.set_language, name='setlang'),
    path('site_admin/', proj_views.site_admin, name='site_admin'),
    path('help/', proj_views.help_index, name='help_index'),
    path('help/<subject>/', proj_views.help, name='help'),

    path('', views.IndexView.as_view(), name='home'),

    path('search/', views.SearchView.as_view(), name='search'),
    path('search/export', views.search_export, name='search-export'),
    path('search/add_criterion/<int:base_num>/', views.add_criterion, name='add_criterion'),
    path('search_images/', search_view_factory(
        view_class=views.ImageSearchView,
        form_class=ImageSearchForm,
        template='search/search_images.html'), name='search_img'),

    #Duplicate accession after the germination.
    path('invent/<int:object_id>/duplicate/', views.accession_duplicate, name='accession_duplicate'),

    path('invent/', views.invent_list, name='inventories'),
    path('invent/autocomplete/', views.object_autocomplete, name='baseobject_autocomplete'),
    path('invent/<int:parent_id>/new/<obj_code>/', views.ObjectCreateView.as_view(), name='object_create'),
    path('invent/<int:obj_id>/', views.ObjectDetailView.as_view(), name='object_detail'),
    path('invent/<int:obj_id>/as_image/', views.object_image, name='object_image'),
    path('invent/<int:obj_id>/edit/', views.ObjectEditView.as_view(), name='object_edit'),
    path('invent/<int:obj_id>/editperms/', views.object_edit_perms, name='object_edit_perms'),
    path('invent/<int:obj_id>/delete/', views.object_delete, name='object_delete'),
    re_path(r'^invent/(?P<obj_id>\d+)/contents/(?P<otype>[-\w]+)?/?$',
        views.PaginatedObjectsView.as_view(), name='object_list'),
    re_path(r'^invent/(?P<obj_id>\d+)/contents/(?P<otype>[-\w]+)/(?P<format>(table|geojson))/$',
        views.object_list_table, name='object_list_table'),
    re_path(r'^invent/by_uuid/(?P<uuid>[-\w]+)/(?P<format>(html|json|xml))?/?$',
        views.object_list_by_uuid, name='object_by_uuid'),
    path('invent/<int:obj_id>/export/lots/', views.ObjectExportLots.as_view(), name='object_export_lots'),
    path('invent/<int:obj_id>/import/', views.object_import, name='object_import'),
    path('attributes/', views.attributes, name='attributes'),
    # attribute name passed in GET
    path('attributes/vocab/', views.vocab_for_attribute, name='vocab_for_attribute'),

    re_path(r'^attributes_in_group/(?P<group_id>-?\d+)/$', views.attributes_in_group),

    # CRUD for Lots and GermTests
    path('accession/<int:pk>/new_lot/', views.LotEditView.as_view(create=True), name='lot_create'),
    path('lot/<int:pk>/edit/', views.LotEditView.as_view(), name='lot_edit'),
    path('lot/<int:pk>/delete/', views.LotDeleteView.as_view(), name='lot_delete'),
    path('lot/<int:pk>/new_test/', views.GermTestEditView.as_view(create=True), name='test_create'),
    path('germtest/<int:pk>/edit/', views.GermTestEditView.as_view(), name='test_edit'),
    path('germtest/<int:pk>/delete/', views.GermTestDeleteView.as_view(), name='test_delete'),
    path('command/<int:lot_pk>/create/', views.CommandEditView.as_view(create=True), name='command_create'),
    path('command/<int:pk>/edit/', views.CommandEditView.as_view(), name='command_edit'),
    path('command/<int:pk>/delete/', views.CommandDeleteView.as_view(), name='command_delete'),

    path('<int:object_id>/value/new/<int:attr_id>/', views.add_value, name='add_value'),
    re_path(r'^(?P<object_id>\d+)/value/(?P<value_id>[\w\-]+)/edit/$', views.edit_value),
    path('<int:object_id>/value/del/', views.del_value, name='del_value'), # value id in POST
    # Image and file attachment editing
    path('<int:object_id>/photo/add/', views.add_photo, name="add_photo"),
    path('<int:object_id>/file/<int:file_id>/edit/', views.edit_file, name='edit_file'),
    path('<int:object_id>/file/del/', views.del_file), # file id in POST
]

if settings.DEBUG:
    urlpatterns += [
        path('media/<path:path>', serve, {'document_root': settings.MEDIA_ROOT}),
    ]
