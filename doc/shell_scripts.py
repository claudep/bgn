# ***** Rename specimens based on some attribute ******
from invent.models import BaseObject

bos = BaseObject.objects.filter(obj_type__tname='Specimen', taxon__isnull=False)

for bo in bos:
    try:
        old_title = int(bo.title)
    except:
        continue
    
    if bo.taxon:
        new_title = bo.taxon.name
    else:
        new_title = bo.get_value_for('REMARKS_TAXO')
        if new_title and new_title.startswith("SPECIES: "):
            new_title = new_title[9:]
    if new_title:
        new_title = "%s (%d)" % (new_title, old_title)
        bo.title = new_title
        bo.save()
        print "Specimen renamed from '%s' to '%s'" % (old_title, new_title)

# ***** Change lang of attribute titles *****
import itertools
from invent.models import Attribute

new_lang = 'de'
# FIXME: add project attributes
for d in in itertools.chain([eurisco_attributes, bdn_attributes, various_attributes]):
    try:
        attr = Attribute.objects.get(name=d['name'])
    except Attribute.DoesNotExist:
        print "Attribute %s does not exist" % d['name']
        continue
    new_title = type(d['title']) is dict and d['title'][new_lang] or d['title']
    if new_title != attr.atitle:
        attr.atitle = new_title
        attr.save()
        print "Changed %s title from '%s' to '%s'" % (attr.name, attr.atitle, new_title)

# ***** Add a group permission to an object
from invent.models import BaseObject
from django.contrib.auth.models import Group

bo = BaseObject.objects.get(id=4062)
grp = Group.objects.get(name='myco_admin')

bo.add_write_permission(grp, apply_to_children=True)
